class Delay<T>{

  Future<T> invokeWithArgs({Function function,args})async{
    return Future.delayed(Duration(milliseconds: 200),(){
      return function(args);
    });
  }

  Future<T> invoke({Function function})async{
    return Future.delayed(Duration(milliseconds: 200),(){
      return function();
    });
  }

}