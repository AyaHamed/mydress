import 'dart:convert';
import 'dart:io';
import 'dart:ui';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:location/location.dart';
import 'package:my_dress/general/Bloc/UserProvider.dart';
import 'package:my_dress/general/constants/GlobalState.dart';
import 'package:my_dress/general/constants/LoadingDialog.dart';
import 'package:my_dress/general/models/CustomerModel.dart';
import 'package:my_dress/general/models/DelegateModel.dart';
import 'package:my_dress/general/models/SavedDataModel.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

class Utils {


  static Future<void> manipulateSplashData(
      Function func, GlobalKey<ScaffoldState> scaffold) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var strUser = prefs.get("user");
    if (strUser != null) {
      SavedDataModel data = SavedDataModel().savedDataModelFromJson(strUser);
      GlobalState.instance.set("token", data.token);
      print(json.decode(strUser));
      setCurrentUserData(data,scaffold.currentContext);
      changeLanguage(data.lang,func);
    } else {
      changeLanguage("ar",func);
      //MyRoute().navigate(context: scaffold.currentContext,route: SelectUser(),withReplace: true);
    }
  }

  static void  setCurrentUserData(SavedDataModel model,BuildContext context){
    var provider = Provider.of<UserProvider>(context,listen: false);
    provider.setModel(model);
    if(model.type==1){
      //MyRoute().navigate(context: context,route: Home(),withReplace: true);
    }else{
      //MyRoute().navigate(context: context,route: DelegateHome(),withReplace: true);
    }
  }

  static void saveUserData(SavedDataModel model)async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("user", SavedDataModel().savedDataModelToJson(model));
  }

  static void  changeLanguage(String lang,Function func){
    if (lang == "en") {
      func(Locale('en', 'US'));
    } else {
      func(Locale('ar', 'EG'));
    }
  }

  static SavedDataModel getSavedData({@required BuildContext context}){
    var provider = Provider.of<UserProvider>(context,listen: false);
    return provider.model;
  }

  static void clearSavedData()async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.clear();
  }


  static CustomerModel getCustomerData({@required BuildContext context}){
    var provider = Provider.of<UserProvider>(context,listen: false);
    return provider.model.customer;
  }

  static DelegateModel getDelegateData({@required BuildContext context}){
    var provider = Provider.of<UserProvider>(context,listen: false);
    return provider.model.delegate;
  }

  static int getCurrentUserType({@required BuildContext context}){
    var provider = Provider.of<UserProvider>(context,listen: false);
    return provider.model.type;
  }

  static String getCurrentUserLang({@required BuildContext context}){
    var provider = Provider.of<UserProvider>(context,listen: false);
    return provider.model.lang;
  }

  static String getCurrentUserToken({@required BuildContext context}){
    var provider = Provider.of<UserProvider>(context,listen: false);
    return provider.model.token;
  }

  static void setCurrentUserType({@required BuildContext context,@required int type}){
    var provider = Provider.of<UserProvider>(context,listen: false);
    SavedDataModel model=provider.model;
    model.type=type;
    provider.setModel(model);
  }

  static void setCurrentUserLang({@required BuildContext context,@required Function function,@required String lang}){
    var provider = Provider.of<UserProvider>(context,listen: false);
    SavedDataModel model=provider.model;
    model.lang=lang;
    provider.setModel(model);
    changeLanguage(lang,function);
  }

  static String getCurrentUserId({@required BuildContext context}){
    var provider = Provider.of<UserProvider>(context,listen: false);
    return getCurrentUserType(context: context)==1?provider.model.customer.id:provider.model.delegate.id;
  }

  static void getCurrentLocation() {
    var location = Location();
    location.getLocation().then((loc) {
      GlobalState.instance.set("currentLat", "${loc.latitude}");
      GlobalState.instance.set("currentLng", "${loc.longitude}");
    });
  }


  static void setSelectUser({@required int type, @required BuildContext context}) async {
    setCurrentUserType(context: context,type: type);
    //MyRoute().navigate(context: context, route: Login());
  }

  static void launchURL({String url, scaffold}) async {
    if (!url.toString().startsWith("https")) {
      url = "https://" + url;
    }
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      LoadingDialog(scaffold).showNotification("${tr("checkLink")}");
    }
  }

  static void launchWhatsApp(phone) async {
    String message = 'مرحبا بك';
    if (phone.startsWith("00966")) {
      phone = phone.substring(5);
    }
    var _whatsAppUrl = "whatsapp://send?phone=+966$phone&text=$message";
    print(_whatsAppUrl);
    if (await canLaunch(_whatsAppUrl)) {
      await launch(_whatsAppUrl);
    } else {
      throw 'حدث خطأ ما';
    }
  }

  static void launchYoutube({@required String url}) async {
    if (Platform.isIOS) {
      if (await canLaunch('$url')) {
        await launch('$url', forceSafariVC: false);
      } else {
        if (await canLaunch('$url')) {
          await launch('$url');
        } else {
          throw 'Could not launch $url';
        }
      }
    } else {
      if (await canLaunch(url)) {
        await launch(url);
      } else {
        throw 'Could not launch $url';
      }
    }
  }

  static void callPhone({phone}) async {
    await launch("tel:$phone");
  }

  static void sendMail(mail) async {
    await launch("mailto:$mail");
  }

  static Future<File> getImage() async {
    PickedFile image =
    await ImagePicker().getImage(source: ImageSource.gallery);
    if (image != null) {
      return File(image.path);
    } else {
      return null;
    }
  }

  static Future<File> getVideo() async {
    PickedFile image =
    await ImagePicker().getVideo(source: ImageSource.gallery);
    if (image != null) {
      return File(image.path);
    } else {
      return null;
    }
  }

  static void copToClipboard({String text, GlobalKey<ScaffoldState> scaffold}){
    if(text.trim().isEmpty){
      LoadingDialog(scaffold).showNotification(tr("couponValidation"));
      return;
    }else{
      Clipboard.setData(ClipboardData(text: "$text")).then((value) {
        LoadingDialog(scaffold).showNotification(tr("couponSuccess"));
      });
    }
  }



}
