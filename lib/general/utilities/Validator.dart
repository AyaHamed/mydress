import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

class Validator{

  String noValidate({@required String value}){
    return null;
  }

  String validateEmpty({@required String value,String message}){
    if(value.trim().isEmpty){
      return message??tr("fillField");
    }
    return null;
  }

  String validatePassword({@required String value,String message}){
    if(value.trim().isEmpty){
      return message??tr("fillField");
    }else if(value.length<6){
      return message??tr("passValidation");
    }
    return null;
  }

  String validateEmail({@required String value,String message}){
    if(value.trim().isEmpty){
      return message??tr("fillField");
    }else if(!RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(value)){
      return message??tr("mailValidation");
    }
    return null;
  }

  String validatePhone({@required String value,String message}){
    if(value.trim().isEmpty){
      return message??tr("fillField");
    }else if(!RegExp(r'(^\+[0-9]{2}|^\+[0-9]{2}\(0\)|^\(\+[0-9]{2}\)\(0\)|^00[0-9]{2}|^0)([0-9]{9}$|[0-9\-\s]{10}$)').hasMatch(value)||value.length<10){
      return message??tr("phoneValidation");
    }
    return null;
  }

  String validatePasswordConfirm({@required String confirm,@required String pass,String message}){
    if(confirm.trim().isEmpty){
      return message??tr("fillField");
    }else if(confirm!=pass){
      return message??tr("confirmValidation");
    }
    return null;
  }

}