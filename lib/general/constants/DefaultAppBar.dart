import 'package:flutter/material.dart';
import 'MyColors.dart';
import 'MyText.dart';

// ignore: must_be_immutable
class DefaultAppBar extends PreferredSize{

  String title;
  Widget leading;
  BuildContext con;
  List<Widget> actions=[];
  final Size preferredSize = const Size.fromHeight(kToolbarHeight+5);

  DefaultAppBar({@required this.title,@required this.con,this.actions, this.leading});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return AppBar(
      backgroundColor: MyColors.white,
      title: MyText(title: "$title",size: 17,color: MyColors.primary, fontWeight: FontWeight.bold,),
      automaticallyImplyLeading: true,
      centerTitle: true,
      leading: leading?? IconButton(
          onPressed: () => Navigator.of(con).pop(),
          icon: Icon(
            Icons.arrow_back_ios,
            color: MyColors.primary,
            size: 20,
          )),
      actions: actions,
    );
  }

}