import 'package:flutter/material.dart';
import 'MyColors.dart';
import 'MyText.dart';

// ignore: must_be_immutable
class DefaultButton extends StatelessWidget{

  String title;
  Function onTap;
  Color textColor;
  Color color;

  DefaultButton({@required this.title,@required this.onTap,this.color,this.textColor});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 40),
      child: InkWell(
        onTap: onTap,
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: 45,
          margin: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
          decoration: BoxDecoration(
            color: color??MyColors.primary,
            borderRadius: BorderRadius.circular(25),
          ),
          alignment: Alignment.center,
          child: MyText(
            title: "$title",
            size: 12,
            color: textColor??MyColors.white,
          ),
        ),
      ),
    );
  }

}