import 'package:flutter/material.dart';
import 'package:my_dress/general/constants/MyColors.dart';

class LabelTextField extends StatelessWidget{

  final TextEditingController controller;
  final String label;
  final EdgeInsets margin;
  final bool isPassword;
  final TextInputType type;
  final Function(String value) validate;

  LabelTextField({this.label,this.controller,
    this.margin=const EdgeInsets.all(0),this.isPassword=false,
    this.type=TextInputType.text,this.validate});


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 30),
      child: Container(
        height: 75,
        margin: margin,
        child: TextFormField(
          controller: controller,
          keyboardType: type,
          obscureText: isPassword,
          style: TextStyle(fontSize: 14,fontFamily: "cairo",color: MyColors.grey),
          validator: (value)=> validate(value),
          decoration: InputDecoration(
              enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: MyColors.greyWhite,width: 1),
                  borderRadius: BorderRadius.circular(10)
              ),
              focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                  borderSide: BorderSide(color: MyColors.primary,width: 2)
              ),
              errorBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.grey,width: 1),
                  borderRadius: BorderRadius.circular(10)
              ),
              focusedErrorBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                  borderSide: BorderSide(color: Colors.red,width: 2)
              ),
              errorStyle: TextStyle(fontFamily: "cairo",fontSize: 10),
              labelText: "  $label  ",
              labelStyle: TextStyle(fontFamily: "cairo",fontSize: 14),
              contentPadding: EdgeInsets.symmetric(horizontal: 10,vertical: 10),
              filled: true,
              fillColor: Colors.white
          ),
        ),
      ),
    );
  }


}