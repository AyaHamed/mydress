import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

import 'MyColors.dart';

// ignore: must_be_immutable
class CachedImage extends StatefulWidget{

  String url;
  BoxFit fit;
  double height,width;
  BorderRadius borderRadius;
  ColorFilter colorFilter;
  Alignment alignment;
  Widget child;
  BoxShape boxShape;
  CachedImage({
    @required this.url,
    this.fit=BoxFit.fill,
    @required this.width,
    @required this.height,
    this.borderRadius,
    this.colorFilter,
    this.alignment= Alignment.center,
    this.child,
    this.boxShape=BoxShape.rectangle
  });

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _CachedImageState();
  }

}
class _CachedImageState extends State<CachedImage>{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return CachedNetworkImage(
      imageUrl: "${widget.url}",
      width: widget.width,
      height: widget.height,
      imageBuilder: (context, imageProvider) => Container(
        width: widget.width,
        height: widget.height,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: imageProvider,
            fit: widget.fit,
            colorFilter: widget.colorFilter
          ),
          borderRadius: widget.borderRadius,
          shape: widget.boxShape
        ),
        alignment: widget.alignment,
        child: widget.child,
      ),
      placeholder: (context, url) => Container(
        width: widget.width,height: widget.height,
        alignment: Alignment.center,
        child: SpinKitFadingCircle(
          color: MyColors.primary,
          size: 30.0,
        ),
      ),
      errorWidget: (context, url, error) => Container(
        width: widget.width,height: widget.height,
        alignment: Alignment.center,
        child: Icon(Icons.error),
      ),
    );
  }

}