import 'dart:convert';
import 'dart:io';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart'as http;
import 'package:http_parser/http_parser.dart';
import 'package:my_dress/general/constants/GlobalState.dart';
import 'package:my_dress/general/utilities/ConstData.dart';
import 'package:my_dress/general/utilities/utils.dart';
import 'LoadingDialog.dart';


class Http{

  String baseUrl="https://fstany2020.com/";

  GlobalKey<ScaffoldState> _scaffold;

  Http(this._scaffold);



  post(String url,Map<String,String>  body)async{
    LoadingDialog(_scaffold).showDilaog();

    BuildContext context=_scaffold.currentContext;

    var response = await http.post("$baseUrl$url",body: body);
    if(response.statusCode==200){
      var data=json.decode(response.body);
      print(data);
      if(data["key"]==1){
        Navigator.of(context).pop();
        LoadingDialog(_scaffold).showNotification(data["msg"].toString());
        return data;
      }else{
        Navigator.of(context).pop();
        LoadingDialog(_scaffold).showNotification("${data["msg"].toString()}");
      }
    }else{
      Navigator.of(context).pop();
      LoadingDialog(_scaffold).showNotification("${tr("chickNet")}");
    }

    return null;

  }

  get(String url,Map<String,String> body,)async{

    print("scaffold ${_scaffold==null}");

    print("url $baseUrl$url");
    var response = await http.post("$baseUrl$url",body: body,);
    print(response.statusCode);
    print(response.statusCode);
    if(response.statusCode==200){
      var data=json.decode(response.body);
      print(data);
      if(data["key"]==1){
        return data;
      }else{
        LoadingDialog(_scaffold).showNotification(data["msg"].toString());
      }
    }else{
      LoadingDialog(_scaffold).showNotification("${tr("chickNet")}");
    }
    return null;
  }

  uploadFile(String url,Map<String,dynamic> body,{close = false,token}) async{
    LoadingDialog(_scaffold).showDilaog();

    print(body);
    BuildContext context=_scaffold.currentContext;
    //create multipart request for POST or PATCH method
    var request = http.MultipartRequest("POST", Uri.parse("$baseUrl$url"));
    //add text fields
    body.forEach((key, value) async {
      if((value) is File){
        //create multipart using filepath, string or bytes
        var pic = await http.MultipartFile.fromPath("$key", value.path,contentType: MediaType('image', 'jpg'));
        //add multipart to request
        request.files.add(pic);
      }else if((value) is List<File>){
        value.forEach((element) async {
          var pic = await http.MultipartFile.fromPath("$key", element.path,contentType: MediaType('image', 'jpg'));
          //add multipart to request
          request.files.add(pic);
        });
      }else{
        request.fields["$key"] = "$value";
      }
    });


    var response = await request.send();

    if (close) {
      Navigator.of(context).pop();
    }

    //Get the response from the server
    var responseData = await response.stream.toBytes();
    var responseString = utf8.decode(responseData);
    print(responseString);
    if(response.statusCode==200){
      var data=json.decode(responseString);
      if(data["key"]==1){
        Navigator.of(context).pop();
        LoadingDialog(_scaffold).showNotification(data["msg"]);
        return data;
      }else{
        Navigator.of(context).pop();
        LoadingDialog(_scaffold).showNotification(data["msg"]);
      }
    }else{
      Navigator.of(context).pop();
      LoadingDialog(_scaffold).showNotification("${tr("chickNet")}");
    }
    return null;
  }


}