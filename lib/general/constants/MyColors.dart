import 'dart:ui';

import 'package:flutter/material.dart';

class MyColors{
  static Color primary =  Colors.blue[900];
  static Color secondary =  Color(0xff303030);
  static Color black =  Colors.black;
  static Color gold = Color(0xffD4A726);
  static Color grey = Colors.grey;
  static Color greyWhite = Colors.grey[300];
  static Color blackOpacity = Color(0xff313135);
  static Color white = Colors.white;
  static Color brown = Colors.brown;
  static Color pink = Colors.pinkAccent;


  static setColors({Color primaryColor,Color secondaryColor}){
    primary=primaryColor;
    secondary=secondaryColor;
   }

}