import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MyText extends StatelessWidget {
  String title;
  Color color = Colors.black;
  double size = 8;
  TextAlign align = TextAlign.start;
  TextDecoration decoration= TextDecoration.none;
  TextOverflow overflow;
  bool isNumber;
  FontWeight fontWeight;

  MyText({this.title, this.color, this.size, this.align,this.decoration,this.overflow,this.isNumber=false,this.fontWeight=FontWeight.normal});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return Text(
      "$title",
      textAlign: align,
      textScaleFactor: 1.2,
      style: TextStyle(
          color: color,
          fontSize: size?? 10.5,
          decoration: decoration,
          fontWeight: fontWeight,
        fontFamily: "cairo"
      ),
      overflow: overflow,
    );
  }
}