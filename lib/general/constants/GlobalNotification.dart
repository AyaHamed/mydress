import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:my_dress/general/utilities/utils.dart';


class GlobalNotification {
  static StreamController<Map<String, dynamic>> _onMessageStreamController = StreamController.broadcast();

  final FirebaseMessaging _firebaseMessaging = new FirebaseMessaging();
  FlutterLocalNotificationsPlugin _flutterLocalNotificationsPlugin;
  int _id = 0;
  bool show=true;
  static GlobalKey<NavigatorState> navigatorKey;
  Function reset=(){};
  static GlobalNotification instance = new GlobalNotification._();
  GlobalNotification._();
  GlobalNotification();

  setupNotification({GlobalKey<NavigatorState> navKey,Function func}) {
    navigatorKey=navKey;
    reset=func;
    _flutterLocalNotificationsPlugin=new FlutterLocalNotificationsPlugin();
    var android=new AndroidInitializationSettings("@mipmap/launcher_icon");
    var ios=new IOSInitializationSettings();
    var initSettings=new InitializationSettings(android: android,iOS: ios);
    _flutterLocalNotificationsPlugin.initialize(
      initSettings,
      onSelectNotification: onSelectNotification,
    );
    _firebaseMessaging.configure(
      onMessage: (Map<String,dynamic>message){
        _id++;
        _onMessageStreamController.add(message);
        String _key=Platform.isIOS?"notification":"data";
        if(int.parse(message[_key]["type"])==-1){
          Utils.clearSavedData();
          // navigatorKey.currentState.pushAndRemoveUntil(
          //     CupertinoPageRoute(builder: (cxt)=>SelectUser()), (route) => false);
        }
        _showLocalNotification(message,_id);

        return;
      },
      onResume: (Map<String,dynamic>message){
        print("resssssssssssssssssume $message");
        // onSelectNotification(message);
        return;
      },
      onLaunch: (Map<String,dynamic>message){
        print("lunchiiiiiiiiiiiing $message");
        // onSelectNotification(message);
        return;
      },
    );
    _firebaseMessaging.getToken().then((token){
      print(token);
    });


  }

  StreamController<Map<String, dynamic>> get notificationSubject {
    return _onMessageStreamController;
  }

  _showLocalNotification(message, id) async  {
    // var provider= navigatorKey.currentContext.read<NotifyProvider>();
    // provider.setNotifyCount(provider.count+1);
    var _notify = message["notification"];
    if (Platform.isAndroid) {
      _notify = message["data"];
    }
    var android = AndroidNotificationDetails(
      "${DateTime.now()}",
      "${_notify["title"]}",
      "${_notify["body"]}",
      priority: Priority.high,
      importance: Importance.max,
      playSound: true,
      shortcutId: "$_id",
    );
    var ios = IOSNotificationDetails();
    var _platform = NotificationDetails(android: android,iOS: ios);
    _flutterLocalNotificationsPlugin.show(
        id, "انعامي", "${_notify["body"]}", _platform,
        payload: json.encode(message));
  }

  Future onSelectNotification(payload) async {
    var obj = payload;
    if (payload is String) {
      obj = json.decode(payload);
    }

    var _data;
    if (Platform.isIOS) {
      _data = obj["notification"];
    } else {
      _data = obj["data"];
    }

    int _type=int.parse(_data["type"]);
    print("ooooooo $obj");
    int type= Utils.getCurrentUserType(context: navigatorKey.currentContext);
    if(type==1){
      if(_type==0){
        // navigatorKey.currentState.push(
        //     MaterialPageRoute(builder: (context) => Notifications()));
      }else if(_type>0){
        // navigatorKey.currentState.push(
        //     MaterialPageRoute(builder: (context) =>
        //         OrderDetails(id: _data["order_id"])));
      }

    }else{
      if(_type==0){
        // navigatorKey.currentState.push(
        //     MaterialPageRoute(builder: (context) => DelegateNotifications()));
      }else if(_type>0){
        // navigatorKey.currentState.push(
        //     MaterialPageRoute(builder: (context) =>
        //         DelegateOrderDetails(id: _data["order_id"])));
      }
    }

  }
}
