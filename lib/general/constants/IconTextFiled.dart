import 'package:flutter/material.dart';
import 'package:my_dress/general/constants/MyColors.dart';

class IconTextFiled extends StatelessWidget{

  TextEditingController controller;
  String label;
  EdgeInsets margin=EdgeInsets.all(0);
  TextInputType type=TextInputType.text;
  Widget icon;
  bool isPassword;
  Icon prefix;
  Color borderColor;
  Function(String value) validate;
  IconTextFiled({this.label,this.controller,this.margin,this.type,
    this.icon,this.isPassword=false,this.prefix, this.borderColor, this.validate});


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
      child: Container(
        height: 45,
        margin: margin,
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(25),
            border: borderColor == null? Border.all(color: MyColors.greyWhite) : Border.all(color: borderColor)
        ),
        child: TextFormField(
          controller: controller,
          keyboardType: type,
          enabled: true,
          obscureText: isPassword,
          textAlign: TextAlign.start,
          style: TextStyle(fontSize: 16,fontFamily: "cairo",color: Colors.black.withOpacity(.7)),
          validator: (value)=> validate(value),
          decoration: InputDecoration(
            enabledBorder: InputBorder.none,
            focusedBorder: InputBorder.none,
            hintText: label != null ? "$label" : "",
            hintStyle: TextStyle(fontFamily: "cairo",fontSize: 14,color: Colors.grey),
            contentPadding: EdgeInsets.symmetric(horizontal: 10,vertical: 14),
            suffixIcon: icon,
            prefixIcon: prefix,

          ),
        ),
      ),
    );
  }


}