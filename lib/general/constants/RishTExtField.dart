import 'package:flutter/material.dart';
import 'package:my_dress/general/constants/MyColors.dart';

class RishTextField extends StatelessWidget{

  TextEditingController controller;
  String label;
  EdgeInsets margin=EdgeInsets.all(0);
  bool isPassword=false;
  TextInputType type=TextInputType.text;

  RishTextField({this.label,this.controller,this.margin,this.isPassword,this.type});


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
//      height: 55,
      margin: margin,
      child: TextFormField(
        maxLines: 16,
        minLines: 16,
        controller: controller,
        keyboardType: type,
        obscureText: isPassword,
        style: TextStyle(fontSize: 14,fontFamily: "cairo",color: Colors.black.withOpacity(.7)),
        decoration: InputDecoration(
            enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.grey,width: 1),
                borderRadius: BorderRadius.circular(10)
            ),
            focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
                borderSide: BorderSide(color: MyColors.primary.withOpacity(.5),width: 1)
            ),
            hintText: "  $label  ",
            hintStyle: TextStyle(fontFamily: "cairo",fontSize: 14),
            contentPadding: EdgeInsets.symmetric(horizontal: 10,vertical: 10)
        ),
      ),
    );
  }


}