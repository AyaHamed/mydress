import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:my_dress/general/constants/MyColors.dart';

import 'MyText.dart';

class LoadingDialog{

   var _scaffold;

   LoadingDialog(this._scaffold);

   showDilaog(){
    showDialog(
        barrierDismissible: false,
        context: _scaffold.currentContext,
        builder: (BuildContext context){
          return AlertDialog(
            contentPadding: EdgeInsets.all(0),
            elevation: 0,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(10))
            ),
            backgroundColor: Colors.transparent,
            content: SpinKitDualRing(
              color: MyColors.primary,
              size: 30.0,
            ),
          );
        }
    );
  }

   showLoadingView(){
     return SpinKitCubeGrid(
       color: MyColors.primary,
       size: 40.0,
     );
  }

   showMapLoadingView(){
     return CupertinoActivityIndicator(
       animating: true,
       radius: 20,
     );
   }

   showNotification(msg){
    _scaffold.currentState.showSnackBar(
      SnackBar(
        content: Container(
          height: 25,
          alignment: Alignment.center,
          child: MyText(title: msg,color: MyColors.white,size: 10,),
        ),
        backgroundColor: MyColors.blackOpacity,
        duration: Duration(seconds: 2),

      ),
    );
  }

   showBlackNotification(msg){
     _scaffold.currentState.showSnackBar(
       SnackBar(
         content: Container(
           height: 30,
           alignment: Alignment.center,
           child: MyText(title: msg,color: MyColors.white,size: 16,),
         ),
         backgroundColor: MyColors.primary,
         duration: Duration(seconds: 2),
       ),
     );
   }


}