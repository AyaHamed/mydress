import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_dress/customer/screens/FAQ.dart';
import 'package:my_dress/customer/screens/UserPolicy.dart';
import 'package:my_dress/general/constants/DefaultAppBar.dart';
import 'package:my_dress/general/constants/MyColors.dart';

import 'Terms.dart';

class Policies extends StatefulWidget {
  @override
  _PoliciesState createState() => _PoliciesState();
}

class _PoliciesState extends State<Policies> {

  Widget policyItem(String title, Widget page){
    return InkWell(
      onTap: () {Navigator.of(context).push(CupertinoPageRoute(builder: (_) => page));},

      child: Container(
        width: MediaQuery.of(context).size.width,
        height: 50,

        decoration: BoxDecoration(
            color: MyColors.white,
            border: Border.all(color: MyColors.greyWhite)
        ),

        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,

            children: [

              Icon(Icons.help_rounded, color: MyColors.primary, size: 30,),
              SizedBox(width: 10,),
              Text(title, style: TextStyle(
                color: MyColors.primary,
                fontSize: 20,
              ),),
              Expanded(child: SizedBox()),
              Icon(Icons.arrow_left_sharp, color: MyColors.primary, size: 30,),
            ],
          ),
        ),

      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(

      appBar: DefaultAppBar(con: context, title: "الساسية العامة",),

      body: Container(
        padding: EdgeInsets.all(10),
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
          border: Border.all(color: MyColors.greyWhite),
          borderRadius: BorderRadius.circular(5)
        ),
        
        child: Column(
          children: [
            policyItem("سياسة الاستخدام", UserPolicy()),
            policyItem("الشروط والأحكام", Terms()),
            policyItem("الاسئلة الشائعة", FAQ()),
          ],
        ),
      ),

    );
  }
}
