import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_dress/general/constants/DefaultButton.dart';
import 'package:my_dress/general/constants/IconTextFiled.dart';
import 'package:my_dress/general/constants/MyColors.dart';
import 'package:my_dress/general/constants/MyText.dart';
import 'package:my_dress/general/screens/NewPassword.dart';

class ForgetPassword extends StatefulWidget {
  @override
  _ForgetPasswordState createState() => _ForgetPasswordState();
}

class _ForgetPasswordState extends State<ForgetPassword> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(

        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,

        decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("images/splash@2x.png"),
              fit: BoxFit.cover,
            )
        ),

        child: ListView(
          children: [

            SizedBox(height: MediaQuery.of(context).size.height * 0.13,),

            Image.asset("images/logo.png",
              height: MediaQuery.of(context).size.height * 0.16 ,),

            SizedBox(height: 40,),

            MyText(
              title: "رقم الهاتف",
              size: 13,
              align: TextAlign.center,
              color: MyColors.gold,
            ),

            SizedBox(height: 10,),

            IconTextFiled(),


            SizedBox(height: 20,),

            MyText(
              title: "يرجى إدخال رقم الهاتف ليتم إرسال كود التحقق",
              size: 13,
              align: TextAlign.center,
              color: MyColors.gold,
            ),

            SizedBox(height: 20,),

            DefaultButton(
              title: "إرسال",
              onTap: () {Navigator.of(context)
                  .pushReplacement(CupertinoPageRoute(builder: (_) => NewPassword()));},
            ),

          ],
        ),

      ),
    );
  }
}
