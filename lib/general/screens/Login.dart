import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:my_dress/customer/resources/customerMethods.dart';
import 'package:my_dress/customer/resources/customerRepository.dart';
import 'package:my_dress/general/constants/DefaultButton.dart';
import 'package:my_dress/general/constants/IconTextFiled.dart';
import 'package:my_dress/general/constants/MyColors.dart';
import 'package:my_dress/general/constants/MyText.dart';
import 'package:my_dress/general/screens/ForgetPassword.dart';
import 'package:my_dress/general/utilities/Validator.dart';

import 'Register.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  GlobalKey<ScaffoldState> _scaffold = new GlobalKey<ScaffoldState>();
  GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  CustomerRepository _repository;

  CustomerMethods _customerAuthMethods;

  TextEditingController _phone = new TextEditingController();
  TextEditingController _password = new TextEditingController();
  bool _clicked = false;

  @override
  initState() {
    super.initState();
    setState(() {
      _repository = CustomerRepository(scaffold: _scaffold);
    });
  }

  _setUserLogin() async {
    if (_formKey.currentState.validate()) {
      bool result = await _repository.setUserLogin(_phone.text, _password.text);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffold,
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
            image: DecorationImage(
          image: AssetImage("images/splash@2x.png"),
          fit: BoxFit.cover,
        )),
        child: ListView(
          children: [
            SizedBox(
              height: 150,
            ),
            Image.asset(
              "images/logo.png",
              height: MediaQuery.of(context).size.height * 0.16,
            ),
            SizedBox(
              height: 20,
            ),
            Form(
                key: _formKey,
                child: Column(
                  children: [
                    IconTextFiled(
                      label: "البريد الالكتروني",
                      prefix: Icon(
                        Icons.mail_outline_sharp,
                        size: 25,
                        color: MyColors.grey,
                      ),
                      controller: _phone,
                      validate: (value) =>
                          Validator().validateEmpty(value: value),
                    ),

                    // LabelTextField(
                    //   label: "رقم الجوال",
                    //   controller: _phone,
                    //   validate: (value) => Validator().validateEmpty(value: value),
                    // ),
                    //
                    // LabelTextField(
                    //   label: "كلمة المرور",
                    //   isPassword: true,
                    //   controller: _password,
                    //   validate: (value) => Validator().validateEmpty(value: value),
                    // ),

                    IconTextFiled(
                      label: "كلمة المرور",
                      prefix: Icon(
                        Icons.lock_outline,
                        size: 25,
                        color: MyColors.grey,
                      ),
                      isPassword: true,
                      controller: _password,
                      validate: (value) =>
                          Validator().validateEmpty(value: value),
                    ),
                  ],
                )),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 40, vertical: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  InkWell(
                    onTap: () {
                      Navigator.of(context).pushReplacement(
                          CupertinoPageRoute(builder: (_) => ForgetPassword()));
                    },
                    child: MyText(
                      title: "نسيت كلمة المرور ؟ ",
                      color: MyColors.gold,
                      size: 11,
                    ),
                  ),
                ],
              ),
            ),
            Visibility(
              visible: !_clicked,
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 10),
                child: DefaultButton(
                  title: "دخول",
                  onTap: () => _setUserLogin(),
                  // onTap: () {Navigator.of(context)
                  //     .pushAndRemoveUntil(CupertinoPageRoute(builder: (_) => ProHome(0)), (route) => false);},
                ),
              ),
              replacement: SpinKitDualRing(
                color: MyColors.primary,
                size: 30.0,
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                MyText(
                  title: "ليس لديك حساب ؟ ",
                  color: MyColors.gold,
                  size: 10,
                ),
                InkWell(
                  onTap: () {
                    Navigator.of(context)
                        .push(CupertinoPageRoute(builder: (_) => Register()));
                  },
                  child: MyText(
                    title: "تسجيل جديد",
                    color: MyColors.gold,
                    size: 11,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
