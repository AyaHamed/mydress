import 'dart:async';
import 'package:flutter/material.dart';
import 'package:my_dress/general/screens/SelectType.dart';

class Splash extends StatefulWidget {

  final Function function;

  Splash({@required this.function});

  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> {

  void initState() {
    Timer(Duration(seconds: 5), (){
      widget.function(Locale('ar', 'EG'));
      Navigator.pushReplacement(context,
          MaterialPageRoute(builder: (context)=> SelectType()));

    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(

      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,

      decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("images/splash@2x.png"),
            fit: BoxFit.cover,
          )
      ),

      child: Center(
        child: Image.asset("images/logo@2x.png",
          height: MediaQuery.of(context).size.height *0.3,),
      ),

    );
  }
}

