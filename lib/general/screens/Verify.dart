import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_dress/customer/screens/Home.dart';
import 'package:my_dress/general/constants/DefaultButton.dart';
import 'package:my_dress/general/constants/IconTextFiled.dart';
import 'package:my_dress/general/constants/MyColors.dart';
import 'package:my_dress/general/constants/MyText.dart';

class Verify extends StatefulWidget {
  @override
  _VerifyState createState() => _VerifyState();
}

class _VerifyState extends State<Verify> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(

        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,

        decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("images/splash@2x.png"),
              fit: BoxFit.cover,
            )
        ),

        child: ListView(
          children: [
            SizedBox(height: MediaQuery.of(context).size.height * 0.13,),

            Image.asset("images/logo.png",
              height: MediaQuery.of(context).size.height * 0.16 ,),

            SizedBox(height: 30,),

            MyText(
              title: "كود التحقق",
              color: MyColors.gold,
              size: 13,
              align: TextAlign.center,
            ),

            IconTextFiled(),
            SizedBox(height: MediaQuery.of(context).size.height * 0.05,),

            DefaultButton(
              title: "تأكيد",
              onTap: () {Navigator.of(context)
                  .pushAndRemoveUntil(CupertinoPageRoute(builder: (_) => Home(0)), (route) => false);},
            ),

            DefaultButton(
                title: "ارسال مرة اخرى",
            ),
          ],
        ),

      ),
    );
  }
}
