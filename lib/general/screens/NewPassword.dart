import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_dress/general/constants/DefaultButton.dart';
import 'package:my_dress/general/constants/IconTextFiled.dart';

import 'Login.dart';

class NewPassword extends StatefulWidget {
  @override
  _NewPasswordState createState() => _NewPasswordState();
}

class _NewPasswordState extends State<NewPassword> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(

        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,

        decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("images/splash@2x.png"),
              fit: BoxFit.cover,
            )
        ),

        child: ListView(
          children: [

            SizedBox(height: MediaQuery.of(context).size.height * 0.1,),

            Image.asset("images/logo.png",
              height: MediaQuery.of(context).size.height * 0.16 ,),

            SizedBox(height: 40,),

            IconTextFiled(
              label: "كود التحقق",
              prefix: Icon(Icons.phone_android_rounded,size: 25,),
            ),

            IconTextFiled(
              label: "كلمة المرور الجديدة",
              prefix: Icon(Icons.lock_outline, size: 25,),
              isPassword: true,
            ),

            IconTextFiled(
              label: "تأكيد كلمة المرور الجديدة",
              prefix: Icon(Icons.lock_outline, size: 25,),
              isPassword: true,
            ),

            SizedBox(height: 30,),

            DefaultButton(
              title: "تأكيد",
              onTap: () {Navigator.of(context)
                  .pushReplacement(CupertinoPageRoute(builder: (_) => Login()));},
            ),

          ],
        ),

      ),
    );
  }
}
