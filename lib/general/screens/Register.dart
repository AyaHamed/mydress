import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_dress/general/constants/DefaultButton.dart';
import 'package:my_dress/general/constants/IconTextFiled.dart';
import 'package:my_dress/general/constants/MyColors.dart';
import 'package:my_dress/general/constants/MyText.dart';
import 'package:my_dress/general/screens/Login.dart';
import 'package:my_dress/general/screens/Terms.dart';
import 'package:my_dress/general/screens/Verify.dart';

class Register extends StatefulWidget {
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,

        decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("images/splash@2x.png"),
              fit: BoxFit.cover,
            )
        ),

        child: Padding(
          padding: const EdgeInsets.symmetric( vertical: 40 ),

          child: ListView(
            children: [

              SizedBox(height: MediaQuery.of(context).size.height * 0.1),

              Image.asset("images/logo.png",
                height: MediaQuery.of(context).size.height * 0.16 ,),

              SizedBox(height: 20,),

              IconTextFiled(
                label: "الاسم",
                prefix: Icon(Icons.person,size: 25, color: MyColors.grey,),
              ),
              IconTextFiled(
                label: "رقم الهالتف",
                prefix: Icon(Icons.phone_android,size: 25, color: MyColors.grey,),
              ),
              IconTextFiled(
                label: "البريد الالكتروني",
                prefix: Icon(Icons.mail_outline_sharp,size: 25, color: MyColors.grey,),
              ),
              IconTextFiled(
                label: "كلمة المرور",
                prefix: Icon(Icons.lock_outline,size: 25, color: MyColors.grey,),
                isPassword: true,
              ),
              IconTextFiled(
                label: "تأكيد كلمة المرور",
                prefix: Icon(Icons.lock_outline,size: 25, color: MyColors.grey,),
                isPassword: true,
              ),

              SizedBox(height: 10,),

              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  MyText(
                  title: "بالتسجيل انت توافق على ",
                  color: MyColors.gold,
                  size: 10,
                ),
                  InkWell(
                    onTap: () {Navigator.of(context)
                        .push(CupertinoPageRoute(builder: (_) => Terms()));},
                    child: MyText(
                      title: "الشروط والأحكام",
                      color: MyColors.gold,
                      size: 10,
                    ),
                  ),
                ],
              ),

              Padding(
                padding: const EdgeInsets.symmetric(vertical: 10),
                child: DefaultButton(
                  title: "تسجيل",
                  onTap: () {Navigator.of(context)
                      .pushReplacement(CupertinoPageRoute(builder: (_) => Verify()));},
                ),
              ),

              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  MyText(
                    title: "لدي حساب بالفعل ؟ ",
                    color: MyColors.gold,
                    size: 10,
                  ),
                  InkWell(
                    onTap: () {Navigator.of(context)
                        .pushReplacement(CupertinoPageRoute(builder: (_) => Login()));},
                    child: MyText(
                      title: "تسجيل الدخول",
                      color: MyColors.gold,
                      size: 10,
                    ),
                  ),
                ],
              ),

            ],
          ),
        ),

      ),
    );
  }
}
