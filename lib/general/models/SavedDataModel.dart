import 'dart:convert';

import 'package:my_dress/general/models/CustomerModel.dart';
import 'package:my_dress/general/models/DelegateModel.dart';


class SavedDataModel {
  SavedDataModel({
    this.lang="ar",
    this.customer,
    this.delegate,
    this.type,
    this.token="",
    this.deviceId=""
  });

  String lang;
  String token;
  String deviceId;
  int type;
  CustomerModel customer;
  DelegateModel delegate;

  factory SavedDataModel.fromJson(Map<String, dynamic> json) => SavedDataModel(
    lang: json["lang"],
    token: json["token"],
    type: json["type"],
    customer: CustomerModel.fromJson(json["customer"]),
    delegate: DelegateModel.fromJson(json["delegate"]),
  );

  Map<String, dynamic> toJson() => {
    "lang": lang,
    "token": token,
    "type": type,
    "customer": customer.toJson(),
    "delegate": delegate.toJson(),
  };

  SavedDataModel savedDataModelFromJson(String str) => SavedDataModel.fromJson(json.decode(str));

  String savedDataModelToJson(SavedDataModel data) => json.encode(data.toJson());


}

