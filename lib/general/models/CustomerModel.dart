// To parse this JSON data, do
//
//     final customerModel = customerModelFromJson(jsonString);

import 'dart:convert';

import 'dart:io';

CustomerModel customerModelFromJson(String str) => CustomerModel.fromJson(json.decode(str));

String customerModelToJson(CustomerModel data) => json.encode(data.toJson());

class CustomerModel {
  CustomerModel({
    this.typeUser,
    this.id,
    this.userName,
    this.email,
    this.img,
    this.phone,
    this.notify,
    this.lat,
    this.lng,
    this.address,
    this.lang,
    this.image
  });

  int typeUser;
  String id;
  String userName;
  String email;
  String img;
  String phone;
  bool notify;
  String lat;
  String lng;
  String address;
  String lang;
  File image;

  factory CustomerModel.fromJson(Map<String, dynamic> json) => CustomerModel(
    typeUser: json["type_user"],
    id: json["id"],
    userName: json["user_name"],
    email: json["email"],
    img: json["img"],
    phone: json["phone"],
    notify: json["notify"],
    lat: json["lat"],
    lng: json["lng"],
    address: json["address"],
    lang: json["lang"],
  );

  Map<String, dynamic> toJson() => {
    "type_user": typeUser,
    "id": id,
    "user_name": userName,
    "email": email,
    "img": img,
    "phone": phone,
    "notify": notify,
    "lat": lat,
    "lng": lng,
    "address": address,
    "lang": lang,
  };
}
