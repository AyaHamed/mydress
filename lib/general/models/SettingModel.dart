// To parse this JSON data, do
//
//     final settingModel = settingModelFromJson(jsonString);

import 'dart:convert';

SettingModel settingModelFromJson(String str) => SettingModel.fromJson(json.decode(str));

String settingModelToJson(SettingModel data) => json.encode(data.toJson());

class SettingModel {
  SettingModel({
    this.aboutUsClient,
    this.aboutUsDelegt,
    this.condtionsClient,
    this.condtionsDelegt,
    this.text1Client,
    this.text2Client,
    this.text3Client,
    this.text1Delegt,
    this.text2Delegt,
    this.text3Delegt,
    this.twitter,
    this.phone,
    this.location,
    this.keyMap,
    this.instgram,
    this.facebook,
    this.bankAccount,
    this.bankAccount2,
    this.bankAccountName,
    this.bankAccountName2,
    this.bgColor,
    this.splash,
    this.logo,
    this.textColor,
    this.secondary,
    this.primary
  });

  String aboutUsClient;
  String aboutUsDelegt;
  String condtionsClient;
  String condtionsDelegt;
  String text1Client;
  String text2Client;
  String text3Client;
  String text1Delegt;
  String text2Delegt;
  String text3Delegt;
  String twitter;
  String phone;
  String location;
  dynamic keyMap;
  String instgram;
  String facebook;
  String bankAccount;
  String bankAccount2;
  String bankAccountName;
  String bankAccountName2;
  String primary;
  String secondary;
  String textColor;
  String bgColor;
  String logo;
  String splash;

  factory SettingModel.fromJson(Map<String, dynamic> json) => SettingModel(
    aboutUsClient: json["aboutUs_client"],
    aboutUsDelegt: json["aboutUs_delegt"],
    condtionsClient: json["condtions_client"],
    condtionsDelegt: json["condtions_delegt"],
    text1Client: json["text1_client"],
    text2Client: json["text2_client"],
    text3Client: json["text3_client"],
    text1Delegt: json["text1_delegt"],
    text2Delegt: json["text2_delegt"],
    text3Delegt: json["text3_delegt"],
    twitter: json["twitter"],
    phone: json["phone"],
    location: json["location"],
    keyMap: json["key_map"],
    instgram: json["instgram"],
    facebook: json["facebook"],
    bankAccount: json["bank_account"],
    bankAccount2: json["bank_account2"],
    bankAccountName: json["bank_account_name"],
    bankAccountName2: json["bank_account_name2"],
    primary: json["color_primry"],
    secondary: json["color_scoundry"],
    textColor: json["color_text"],
    bgColor: json["color_background"],
    logo: json["img_logo"],
    splash: json["img_splash"],
  );

  Map<String, dynamic> toJson() => {
    "aboutUs_client": aboutUsClient,
    "aboutUs_delegt": aboutUsDelegt,
    "condtions_client": condtionsClient,
    "condtions_delegt": condtionsDelegt,
    "text1_client": text1Client,
    "text2_client": text2Client,
    "text3_client": text3Client,
    "text1_delegt": text1Delegt,
    "text2_delegt": text2Delegt,
    "text3_delegt": text3Delegt,
    "twitter": twitter,
    "phone": phone,
    "location": location,
    "key_map": keyMap,
    "instgram": instgram,
    "facebook": facebook,
    "bank_account": bankAccount,
    "bank_account2": bankAccount2,
    "bank_account_name": bankAccountName,
    "bank_account_name2": bankAccountName2,
  };
}
