// To parse this JSON data, do
//
//     final delegateModel = delegateModelFromJson(jsonString);

import 'dart:convert';

import 'dart:io';

DelegateModel delegateModelFromJson(String str) => DelegateModel.fromJson(json.decode(str));

String delegateModelToJson(DelegateModel data) => json.encode(data.toJson());

class DelegateModel {
  DelegateModel({
    this.typeUser,
    this.point,
    this.id,
    this.userName,
    this.email,
    this.img,
    this.phone,
    this.notify,
    this.lat,
    this.lng,
    this.address,
    this.lang,
    this.image
  });

  int typeUser;
  int point;
  String id;
  String userName;
  String email;
  String img;
  String phone;
  bool notify;
  dynamic lat;
  dynamic lng;
  String address;
  String lang;
  File image;

  factory DelegateModel.fromJson(Map<String, dynamic> json) => DelegateModel(
    typeUser: json["type_user"],
    point: json["point"],
    id: json["id"],
    userName: json["user_name"],
    email: json["email"],
    img: json["img"],
    phone: json["phone"],
    notify: json["notify"],
    lat: json["lat"],
    lng: json["lng"],
    address: json["address"],
    lang: json["lang"],
  );

  Map<String, dynamic> toJson() => {
    "type_user": typeUser,
    "point": point,
    "id": id,
    "user_name": userName,
    "email": email,
    "img": img,
    "phone": phone,
    "notify": notify,
    "lat": lat,
    "lng": lng,
    "address": address,
    "lang": lang,
  };
}
