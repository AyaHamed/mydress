import 'package:flutter/foundation.dart';
import 'package:my_dress/general/models/SavedDataModel.dart';

class UserProvider with ChangeNotifier {

  SavedDataModel _dataModel;

  SavedDataModel get model => _dataModel?? new SavedDataModel(lang: "ar");

  void setModel(SavedDataModel model) {
    print("data updated successfully");
    _dataModel=model;
    notifyListeners();
  }

}