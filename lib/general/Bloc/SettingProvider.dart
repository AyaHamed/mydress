import 'package:flutter/foundation.dart';
import 'package:my_dress/general/models/SettingModel.dart';

class SettingProvider with ChangeNotifier {

  SettingModel _dataModel;

  SettingModel get model => _dataModel?? new SettingModel();

  void setModel(SettingModel model) {
    print("data updated successfully");
    _dataModel=model;
    notifyListeners();
  }

}