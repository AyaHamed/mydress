import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_dress/customer/widgets/dressColor.dart';
import 'package:my_dress/customer/widgets/title.dart';
import 'package:my_dress/general/constants/DefaultAppBar.dart';
import 'package:my_dress/general/constants/DefaultButton.dart';
import 'package:my_dress/general/constants/IconTextFiled.dart';
import 'package:my_dress/general/constants/MyColors.dart';
import 'package:my_dress/general/constants/MyText.dart';
import 'package:my_dress/provider/screens/Edit3.dart';
import 'package:my_dress/provider/widgets/dressSize.dart';
import 'package:my_dress/provider/widgets/newSteps.dart';

class Edit2 extends StatefulWidget {
  @override
  _Edit2State createState() => _Edit2State();
}

class _Edit2State extends State<Edit2> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar(
        con: context,
        title: "إضافة منتج",
      ),

      body: ListView(
        physics: NeverScrollableScrollPhysics(),

        children: [

          steps(
            context: context,
            border1: MyColors.primary,
            fill1: MyColors.gold,
            border2: MyColors.primary,
            fill2: MyColors.gold,
          ),

          Padding(
            padding: const EdgeInsets.only(top: 30, right: 20, left: 20),
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height - 300,
              padding: EdgeInsets.symmetric(vertical: 20),
              decoration: BoxDecoration(
                  color: MyColors.white,
                  borderRadius: BorderRadius.circular(5),
                  border: Border.all(color: MyColors.greyWhite)
              ),

              child: ListView(
                children: [

                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        MyText(
                          title: "تفاصيل المنتج",
                          color: MyColors.primary,
                          size: 17,
                          fontWeight: FontWeight.bold,
                          align: TextAlign.start,
                        ),
                        Icon(Icons.edit, size: 15, color: MyColors.primary),
                      ],
                    ),
                  ),

                  IconTextFiled(
                    label: "العلامة  التجارية",
                    borderColor: MyColors.greyWhite,
                  ),
                  IconTextFiled(
                    label: "نوع القماش",
                    borderColor: MyColors.greyWhite,
                  ),
                  IconTextFiled(
                    label: "نمط الفستان",
                    borderColor: MyColors.greyWhite,
                  ),
                  IconTextFiled(
                    label: "شكل الفستان",
                    borderColor: MyColors.greyWhite,
                  ),

                  title(
                      text: "ألوان المنتج",
                      fontColor: MyColors.primary,
                      iconColor: MyColors.primary,
                      fontSize: 15
                  ),

                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      dressColor(MyColors.primary),
                      dressColor(MyColors.pink),
                      dressColor(MyColors.black),
                      dressColor(MyColors.white),
                      dressColor(MyColors.greyWhite),
                      dressColor(MyColors.grey),
                      dressColor(MyColors.blackOpacity),
                      dressColor(MyColors.gold),
                      dressColor(MyColors.brown),
                    ],
                  ),

                  title(
                      text: "مقاسات المنتج",
                      fontColor: MyColors.primary,
                      iconColor: MyColors.primary,
                      fontSize: 15
                  ),

                  Row(
                    children: [
                      SizedBox(width: 10,),
                      dressSize(
                        size1: "Small",
                        size2: "Large",
                        size3: "XXX Large",
                      ),
                      Spacer(),
                      dressSize(
                        size1: "Medium",
                        size2: "X Large",
                        size3: "XX Large",
                      ),

                    ],
                  ),



                ],
              ),

            ),
          ),
        ],
      ),bottomNavigationBar: Padding(
      padding: const EdgeInsets.symmetric(vertical: 25),
      child: DefaultButton(
        title: "التالي",
        onTap: () {Navigator.of(context)
            .pushReplacement(CupertinoPageRoute(builder: (_) => Edit3()));},
      ),
    ),
    );
  }
}
