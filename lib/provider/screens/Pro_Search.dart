import 'package:my_dress/customer/widgets/ItemDress.dart';
import 'package:my_dress/general/constants/DefaultAppBar.dart';
import 'package:my_dress/general/constants/MyColors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'Pro_DressDetails.dart';

class ProSearch extends StatefulWidget {
  @override
  _ProSearchState createState() => _ProSearchState();
}

class _ProSearchState extends State<ProSearch> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: DefaultAppBar(
          con: context, title: "البحث",
          leading: Container(),
        ),

        body: ListView(
          children: [

            Padding(
              padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 20),
              child: Container(
                height: 45,
                width: MediaQuery.of(context).size.width,

                decoration: BoxDecoration(
                    color: MyColors.white,
                    border: Border.all(color: MyColors.greyWhite),
                    borderRadius: BorderRadius.circular(30)
                ),

                padding: EdgeInsets.symmetric(horizontal: 20),

                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,

                  children: [

                    Container(
                      height: 35, width: 35,

                      decoration: BoxDecoration(
                        color: MyColors.primary,
                        border: Border.all(color: MyColors.greyWhite),
                        borderRadius: BorderRadius.circular(30),
                      ),

                      child: Icon(Icons.search, color: MyColors.white, size: 20,),

                    ),

                    Container(
                      width: MediaQuery.of(context).size.width - 120,
                      child: TextFormField(
                        decoration: InputDecoration(
                            focusedBorder: InputBorder.none,
                            enabledBorder: InputBorder.none,
                            hintText: "البحث"
                        ),
                      ),
                    ),

                  ],
                ),

              ),
            ),

            SingleChildScrollView(
              child: Wrap(
                  spacing: 15,
                  alignment: WrapAlignment.center,
                  children: List.generate(20, (index) => itemDress(context: context, page: Pro_DressDetails()))
              ),
            ),

          ],
        )
    );
  }
}
