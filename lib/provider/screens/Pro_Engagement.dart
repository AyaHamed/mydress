import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:my_dress/customer/widgets/ItemDress.dart';
import 'package:my_dress/general/constants/MyColors.dart';
import 'package:my_dress/general/constants/MyText.dart';

import 'ProHome.dart';
import 'Pro_DressDetails.dart';

class ProEngagement extends StatefulWidget {
  @override
  _ProEngagementState createState() => _ProEngagementState();
}

class _ProEngagementState extends State<ProEngagement> {

  void _onPressed(){
    showModalBottomSheet(context: context, builder: (context) {

      return Container(
        height: MediaQuery.of(context).size.height * 0.3,
        child: Column(children: [
          bottomSheetItem("ترتيب حسب", color: MyColors.primary),
          bottomSheetItem("ترتيب حسب"),
          bottomSheetItem("ترتيب حسب"),
          bottomSheetItem("ترتيب حسب"),
          bottomSheetItem("ترتيب حسب"),
          bottomSheetItem("ترتيب حسب"),
        ],),
      );
    });
  }

  Widget bottomSheetItem(String title, {Color color}){
    return Container(
      height: 40,
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
          color: MyColors.white,
          border: Border.all(color: MyColors.greyWhite)
      ),
      child: Center(
        child: MyText(
          title: title,
          color: color?? MyColors.black,
          size: 15,
          align: TextAlign.center,
        ),
      ),
    );
  }

  Widget filter(String title, IconData icon){
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 30),
      child: Container(
        child: Row(
          children: [
            MyText(
              title: title,
              color: MyColors.primary,
              size: 13,
              fontWeight: FontWeight.bold,
            ),
            SizedBox(width: 10,),
            Icon(icon, color: MyColors.grey,)
          ],
        ),
      ),
    );
  }

  Widget filterBar(){
    return Container(
      height: 35, width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
          color: MyColors.white,
          border: Border.all(color: MyColors.grey, width: 1)
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          InkWell( onTap: () {_onPressed();},
              child: filter("ترتيب حسب", Icons.compare_arrows_sharp)),
          VerticalDivider(color: MyColors.primary),
          InkWell( onTap: () {_onPressed();},
              child: filter("الفئة العمرية", Icons.filter_alt_outlined)),

        ],
      ),
    );
  }

  Widget tabDetails(){
    return SingleChildScrollView(
      child: Wrap(
          spacing: 15,
          alignment: WrapAlignment.center,
          children: List.generate(20, (index) => itemDress(context: context, page: Pro_DressDetails()))
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 6,
      child: Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(140),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              AppBar(
                backgroundColor: MyColors.white,
                centerTitle: true,
                title: Text(
                  "فساتين زفاف",
                  style: TextStyle(color: MyColors.primary, fontSize: 20),
                ),
                leading: IconButton(
                    onPressed: (){ Navigator.pushReplacement(context,
                        MaterialPageRoute(builder: (context) => ProHome(0)));},
                    icon: Icon(
                      Icons.arrow_back_ios,
                      color: MyColors.primary,
                      size: 20,
                    )),
              ),

              TabBar(
                isScrollable: true,
                indicatorSize: TabBarIndicatorSize.tab,
                labelColor: MyColors.gold,
                indicatorColor: MyColors.gold,
                unselectedLabelColor: MyColors.blackOpacity,

                tabs: [
                  Tab(text: "فساتين زفاف",),
                  Tab(text: "فساتين خطوبة",),
                  Tab(text: "فساتين مناسبات",),
                  Tab(text: "فساتين مناسبات",),
                  Tab(text: "فساتين مناسبات",),
                  Tab(text: "فساتين مناسبات",),
                ],
              ),
              filterBar(),
            ],
          ),
        ),

        body: TabBarView(
          physics: NeverScrollableScrollPhysics(),

          children: [

            tabDetails(),
            tabDetails(),
            tabDetails(),
            tabDetails(),
            tabDetails(),
            tabDetails(),

          ],
        ),
      ),
    );
  }
}
