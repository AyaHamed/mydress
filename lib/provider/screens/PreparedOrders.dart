import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_dress/general/constants/DefaultAppBar.dart';
import 'package:my_dress/general/constants/MyColors.dart';
import 'package:my_dress/provider/widgets/orderItem.dart';

import 'PreparingDetails.dart';

class PreparedOrders extends StatefulWidget {
  @override
  _PreparedOrdersState createState() => _PreparedOrdersState();
}

class _PreparedOrdersState extends State<PreparedOrders> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar(con: context, title: "طلبات قيد التنفيذ"),

      body: Padding(
        padding: const EdgeInsets.all(20),
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
              border: Border.all(color: MyColors.greyWhite),
              borderRadius: BorderRadius.circular(5)
          ),

          child: ListView(
            children: [
              orderItem(context: context, onTap: (){ Navigator.push(context,
                  CupertinoPageRoute(builder: (context) => PreparingDetails()));},),

              orderItem(context: context, onTap: (){ Navigator.push(context,
                  CupertinoPageRoute(builder: (context) => PreparingDetails()));},),

              orderItem(context: context, onTap: (){ Navigator.push(context,
                  CupertinoPageRoute(builder: (context) => PreparingDetails()));},),

              orderItem(context: context, onTap: (){ Navigator.push(context,
                  CupertinoPageRoute(builder: (context) => PreparingDetails()));},),

              orderItem(context: context, onTap: (){ Navigator.push(context,
                  CupertinoPageRoute(builder: (context) => PreparingDetails()));},),

            ],
          ),
        ),
      ),
    );
  }
}
