import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_dress/general/constants/MyColors.dart';
import 'package:my_dress/provider/screens/AddNew.dart';
import 'package:my_dress/provider/screens/Pro_Engagement.dart';

import 'ProHomePage.dart';
import 'Pro_MyOrders.dart';
import 'Pro_Profile.dart';
import 'Pro_Search.dart';

class ProHome extends StatefulWidget {

  final int newIndex;
  ProHome(this.newIndex);

  @override
  _ProHomeState createState() => _ProHomeState();
}

class _ProHomeState extends State<ProHome> {
  int _selectIndex;
  @override
  void initState() {
    _selectIndex= widget.newIndex;
    super.initState();
  }

  var pages= [
    ProHomePage(),
    Pro_MyOrders(),
    ProEngagement(),
    ProSearch(),
    Pro_Profile(),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(

        floatingActionButton: InkWell(

          onTap: () {Navigator.of(context)
              .push(CupertinoPageRoute(builder: (_) => AddNew()));},

          child: Container(
            height: 80,
            width: 80,
            decoration: BoxDecoration(borderRadius: BorderRadius.circular(50)),

            child: Image.asset(
              "images/add_product@3x.png",
            ),
          ),
        ),

        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,

        bottomNavigationBar: BottomNavigationBar(
          showSelectedLabels: false,
          showUnselectedLabels: false,
          type: BottomNavigationBarType.fixed,
          selectedItemColor: MyColors.gold,
          currentIndex: _selectIndex,
          onTap: (index){
            print(index);
            setState(() {
              _selectIndex= index;
            });
          },
          items: [
            BottomNavigationBarItem(
                icon: Icon(Icons.home_outlined, size: 27,),
                title: Text('Home')
            ),
            BottomNavigationBarItem(
                icon: Icon(Icons.library_books_outlined, size: 24,),
                title: Text('My Orders')
            ),
            BottomNavigationBarItem(
              icon: Container(),
                title: Text('')
            ),
            BottomNavigationBarItem(
                icon: Icon(Icons.search, size: 25,),
                title: Text('Search')
            ),
            BottomNavigationBarItem(
                icon: Icon(Icons.person_outline, size: 25,),
                title: Text('Profile')
            ),
          ],
        ),
        body: pages[_selectIndex]
    );
  }
}
