import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_dress/customer/widgets/Stepper.dart';
import 'package:my_dress/general/constants/DefaultAppBar.dart';
import 'package:my_dress/general/constants/DefaultButton.dart';
import 'package:my_dress/general/constants/MyColors.dart';
import 'package:my_dress/general/constants/MyText.dart';
import 'package:my_dress/provider/screens/FinishedOrders.dart';
import 'package:my_dress/provider/screens/ProOrders.dart';
import 'package:my_dress/provider/screens/Pro_MyOrders.dart';
import 'package:my_dress/provider/widgets/orderItem.dart';

class PreparingDetails extends StatefulWidget {
  @override
  _PreparingDetailsState createState() => _PreparingDetailsState();
}

class _PreparingDetailsState extends State<PreparingDetails> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar(con: context, title: "طلبات قيد التنفيذ"),

      body: ListView(
        children: [

          Padding(
            padding: const EdgeInsets.all(20),
            child: Container(
              width: MediaQuery.of(context).size.width,

              decoration: BoxDecoration(
                color: MyColors.white,
                  border: Border.all(color: MyColors.greyWhite),
                  borderRadius: BorderRadius.circular(5)
              ),

              child: Column(
                children: [

                  orderItem(context: context,),

                  stepper(
                    context: context,
                    status1: "تم فحصها",
                    icon1: Icons.check_circle,
                    date1: "27/7/2019",
                    status2: "تم قبولها",
                    icon2: Icons.check_circle,
                    date2: "27/7/2019",
                    status3: "تم استلامها",
                    icon3: Icons.check_circle_outlined,
                    date3: "27/7/2019",
                  ),

                ],
              ),
            ),
          ),

          InkWell(
            onTap: (){ Navigator.push(context,
                CupertinoPageRoute(builder: (context) => ProOrders()));},
            child: MyText(
              title: "رؤية الطلبات",
              color: MyColors.primary,
              align: TextAlign.center,
              size: 13,
            ),
          ),

        ],
      ),

      bottomNavigationBar: Container(
        padding: EdgeInsets.symmetric(vertical: 20),
        child: DefaultButton(
          title: "تم استلامها",
          onTap: (){ Navigator.push(context,
              CupertinoPageRoute(builder: (context) => FinishedOrders()));},
        ),
      ),

    );
  }
}
