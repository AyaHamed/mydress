import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_dress/customer/widgets/title.dart';
import 'package:my_dress/general/constants/DefaultAppBar.dart';
import 'package:my_dress/general/constants/DefaultButton.dart';
import 'package:my_dress/general/constants/IconTextFiled.dart';
import 'package:my_dress/general/constants/MyColors.dart';
import 'package:my_dress/general/constants/MyText.dart';
import 'package:my_dress/provider/screens/ProHome.dart';
import 'package:image_picker/image_picker.dart';
import 'package:my_dress/provider/widgets/newSteps.dart';

class AddNew3 extends StatefulWidget {
  @override
  _AddNew3State createState() => _AddNew3State();
}

class _AddNew3State extends State<AddNew3> {
  int group = 0;
  File _image;
  List<File> images = new List<File>(6);

  Future getImage(int index) async {
    var image = await ImagePicker.pickImage(source: ImageSource.camera);

    setState(() {
      images[index] = image;
    });
  }

  Future getPhoto(int index) async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);

    setState(() {
      images[index] = image;
    });
  }
  void _onPressed(int index){
    showModalBottomSheet(context: context, builder: (context) {
      return Container(
        height: 130,
        child: Column(children: <Widget>[
          SizedBox(height: 10,),
          InkWell( onTap: (){getImage(index);},
            child: Container(height: 40, width: 150, decoration: BoxDecoration(color: MyColors.greyWhite),
              child: Center(child:Text('Take a picture')),
            ),),
          SizedBox(height: 20,),
          InkWell( onTap: (){getPhoto(index);},
            child: Container(height: 40, width: 150, decoration: BoxDecoration(color: MyColors.greyWhite),
              child: Center(child: Text('Choose a picture')),
            ),),
        ],),
      );
    });
  }

  Widget picture(int index){
    return InkWell(
        onTap: () => _onPressed(index),
        child: Container(
          height: MediaQuery.of(context).size.height * 0.1,
          width: MediaQuery.of(context).size.width * 0.23,
          decoration: BoxDecoration(
              color: MyColors.white,
              border: Border.all(color: MyColors.greyWhite)
          ),
          child: Container(
            height: 80,
            width: 80,
            alignment: Alignment.topLeft,
            child: Visibility(
              child: InkWell(
                child: Icon(
                  Icons.remove_circle,
                  size: 22,
                  color: MyColors.blackOpacity,
                ),
                onTap: () {
                  print(index);
                  print(images[index]);
                  setState(() {
                    images[index]=null;
                  });
                },
              ),
              visible: images[index] == null? false : true,
            ),
            decoration: BoxDecoration(
              image: DecorationImage(
                image: images[index] == null?
                AssetImage("images/add_photo@2x.png",)
                    :FileImage(images[index],), fit: BoxFit.fill,),
            ),
          ),
        ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar(
        con: context,
        title: "إضافة منتج",
      ),

      body: ListView(
        physics: NeverScrollableScrollPhysics(),
        children: [

          steps(
              context: context,
              border1: MyColors.primary,
              fill1: MyColors.gold,
              border2: MyColors.primary,
              fill2: MyColors.gold,
              border3: MyColors.primary,
              fill3: MyColors.gold
          ),

          Padding(
            padding: const EdgeInsets.only(top: 30, right: 20, left: 20),
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height - 300,
              padding: EdgeInsets.symmetric(vertical: 20),

              decoration: BoxDecoration(
                  color: MyColors.white,
                  borderRadius: BorderRadius.circular(5),
                  border: Border.all(color: MyColors.greyWhite)
              ),

              child: ListView(
                children: [

                  title(
                    text: "شراء/ايجار",
                    fontColor: MyColors.primary,
                    iconColor: MyColors.primary,
                    fontSize: 15
                  ),

                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        width: MediaQuery.of(context).size.width * 0.4,
                        child: ListTile(
                          title: MyText(
                            title: "شراء",
                            color: MyColors.blackOpacity,
                            size: 10,
                          ),
                          leading: Radio(
                            value: 0,
                            groupValue: group,
                            onChanged: (value) {
                              print(value);
                              setState(() {
                                group = value;
                              });
                            },
                          ),
                        ),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width * 0.4,
                        child: ListTile(
                          title: MyText(
                            title: "إيجار",
                            color: MyColors.blackOpacity,
                            size: 10,
                          ),
                          leading: Radio(
                            value: 1,
                            groupValue: group,
                            onChanged: (value) {
                              group = value;
                              setState(() {});
                              print(group);
                            },
                          ),
                        ),
                      ),
                    ],
                  ),

                  Container(
                    width: MediaQuery.of(context).size.width * 0.4,
                    child: ListTile(
                      title: MyText(
                        title: "الاثنين معا",
                        color: MyColors.blackOpacity,
                        size: 10,
                      ),
                      leading: Radio(
                        value: 2,
                        groupValue: group,
                        onChanged: (value) {
                          group = value;
                          setState(() {});
                          print(group);
                        },
                      ),
                    ),
                  ),

                  title(
                      text: "حالة المنتج",
                      fontColor: MyColors.primary,
                      iconColor: MyColors.primary,
                      fontSize: 15
                  ),

                  Container(
                    width: MediaQuery.of(context).size.width * 0.4,
                    child: ListTile(
                      title: MyText(
                        title: "متوفر حاليا",
                        color: MyColors.grey,
                      ),
                      leading: Radio(
                        value: 0,
                        groupValue: group,
                        onChanged: (value) {
                          print(value);
                          setState(() {
                            group = value;
                          });
                        },
                      ),
                    ),
                  ),

                  title(
                      text: "القسم",
                      fontColor: MyColors.primary,
                      iconColor: MyColors.primary,
                      fontSize: 15
                  ),

                  IconTextFiled(
                    icon: Icon(Icons.arrow_drop_down),
                    borderColor: MyColors.greyWhite,
                  ),


                  title(
                      text: "الفئة العمرية",
                      fontColor: MyColors.primary,
                      iconColor: MyColors.primary,
                      fontSize: 15
                  ),

                  IconTextFiled(
                    icon: Icon(Icons.arrow_drop_down),
                    borderColor: MyColors.greyWhite,
                  ),

                  title(
                      text: "صور المنتج",
                      fontColor: MyColors.primary,
                      iconColor: MyColors.primary,
                      fontSize: 15
                  ),

                  Padding(
                    padding: const EdgeInsets.all(10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        picture(0),
                        picture(1),
                        picture(2),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        picture(3),
                        picture(4),
                        picture(5),
                      ],
                    ),
                  ),

                ],
              ),

            ),
          ),
        ],
      ),bottomNavigationBar: Padding(
      padding: const EdgeInsets.symmetric(vertical: 25),
      child: DefaultButton(
        title: "إضافة",
        onTap: () {Navigator.of(context)
            .pushAndRemoveUntil(CupertinoPageRoute(builder: (_) => ProHome(0)), (route) => false);},
      ),
    ),
    );
  }
}
