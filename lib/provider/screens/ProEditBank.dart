import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_dress/customer/screens/Home.dart';
import 'package:my_dress/general/constants/DefaultAppBar.dart';
import 'package:my_dress/general/constants/DefaultButton.dart';
import 'package:my_dress/general/constants/LabelTextField.dart';
import 'package:my_dress/general/constants/MyColors.dart';
import 'package:my_dress/general/constants/MyText.dart';

class ProEditBank extends StatefulWidget {
  @override
  _ProEditBankState createState() => _ProEditBankState();
}

class _ProEditBankState extends State<ProEditBank> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(

      appBar: DefaultAppBar(con: context, title: "الحساب البنكي"),

      body: ListView(
        children: [

          Stack(
              children: [
                Image.asset(
                  "images/cover@2x.png",
                  width: MediaQuery.of(context).size.width,
                  height: 200,
                ),

                Positioned(
                  top: 20,
                  left: MediaQuery.of(context).size.width * 0.38,
                  child: Container(
                    height: 100, width: 100,
                    decoration: BoxDecoration(
                        color: MyColors.greyWhite,
                        borderRadius: BorderRadius.circular(90)
                    ),
                    child: Image.asset("images/profile@2x.png",),
                  ),
                ),

                Positioned(
                  top: 90,
                  left: MediaQuery.of(context).size.width * 0.38,
                  child: Container(
                    height: 25, width: 25,
                    decoration: BoxDecoration(
                        color: MyColors.grey,
                        borderRadius: BorderRadius.circular(90)
                    ),
                    child: Icon(Icons.edit, color: MyColors.white,),
                  ),
                ),

                Positioned(
                  top: 150,
                  child: Container(
                    height: 30,
                    width: MediaQuery.of(context).size.width,
                    child: MyText(
                      title: "البنك الأهلي",
                      align: TextAlign.center,
                      size: 15,
                      fontWeight: FontWeight.bold,
                      color: MyColors.primary,
                    ),
                  ),
                )

              ]),

          // title("حالة الحساب "),
          // IconTextFiled(borderColor: MyColors.grey,),

          LabelTextField(
            label: "حالة الحساب",
            isPassword: false,
          ),

          LabelTextField(
            label: "اسم صاحب الحساب",
            isPassword: false,
          ),

          LabelTextField(
            label: "رقم الحساب",
            isPassword: false,
          ),

          LabelTextField(
            label: "المدينة",
            isPassword: false,
          ),

          //
          // title("اسم صاحب الحساب  "),
          // IconTextFiled(borderColor: MyColors.grey,),
          //
          // title("رقم الحساب  "),
          // IconTextFiled(borderColor: MyColors.grey,),
          //
          // title("المدينة"),
          // IconTextFiled(borderColor: MyColors.grey,),

          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 10),
            child: DefaultButton(
              title: "حفظ",
              onTap: () {Navigator.of(context).push(CupertinoPageRoute(builder: (_) => Home(0)));},
            ),

          )

        ],
      ),
    );
  }
}
