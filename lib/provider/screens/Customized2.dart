import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_dress/customer/widgets/title.dart';
import 'package:my_dress/general/constants/DefaultAppBar.dart';
import 'package:my_dress/general/constants/DefaultButton.dart';
import 'package:my_dress/general/constants/MyColors.dart';
import 'package:my_dress/provider/widgets/field.dart';
import 'package:my_dress/provider/widgets/newSteps.dart';

import 'Customized3.dart';

class Customized2 extends StatefulWidget {
  @override
  _Customized2State createState() => _Customized2State();
}

class _Customized2State extends State<Customized2> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar(con: context, title: "تفصيل فستان"),
      body: ListView(
        children: [

          steps(
            context: context,
            border1: MyColors.primary,
            fill1: MyColors.gold,
            border2: MyColors.primary,
            fill2: MyColors.gold,
          ),

          Padding(
            padding: const EdgeInsets.symmetric(vertical: 30),
            child: Image.asset("images/body@3x.png", height: 200,),
          ),

          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 25),
            child: Container(
              width: MediaQuery.of(context).size.width,

              decoration: BoxDecoration(
                //color: Colors.green,
                borderRadius: BorderRadius.circular(8),
                border: Border.all(color: MyColors.greyWhite),
              ),

              padding: EdgeInsets.symmetric(horizontal: 15, vertical: 15),

              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [


                  title(text: "قياس محيط الصدر"),
                  field(context: context, title: "CM"),

                  title(text: "قياس محيط الوسط"),
                  field(context: context, title: "000"),

                  title(text: "قياس محيط الورك"),
                  field(context: context, title: "000"),

                  title(text: "قياس محيط الفخد"),
                  field(context: context, title: "000"),

                  title(text: "قياس الكتف (من الكتف إلى الكتف الآخر)"),
                  field(context: context, title: "000"),

                  title(text: "القياس من الكتف إلى الخصر   "),
                  field(context: context, title: "000"),

                ],
              ),

            ),
          ),

          Padding(
            padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 15),
            child: DefaultButton(
              color:MyColors.primary,
              title: "التالي",
              onTap: () {Navigator.of(context)
                  .pushReplacement(CupertinoPageRoute(builder: (_) => Customized3()));},
            ),
          ),

        ],
      ),
    );
  }
}
