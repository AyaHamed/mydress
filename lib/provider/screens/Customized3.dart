import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_dress/customer/widgets/title.dart';
import 'package:my_dress/general/constants/DefaultAppBar.dart';
import 'package:my_dress/general/constants/DefaultButton.dart';
import 'package:my_dress/general/constants/MyColors.dart';
import 'package:my_dress/provider/screens/Pro_OrderDetails.dart';
import 'package:my_dress/provider/widgets/field.dart';
import 'package:my_dress/provider/widgets/newSteps.dart';


class Customized3 extends StatefulWidget {
  @override
  _Customized3State createState() => _Customized3State();
}

class _Customized3State extends State<Customized3> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar(con: context, title: "تفصيل فستان"),
      body: ListView(
        children: [

          steps(
              context: context,
              border1: MyColors.primary,
              fill1: MyColors.gold,
              border2: MyColors.primary,
              fill2: MyColors.gold,
              border3: MyColors.primary,
              fill3: MyColors.gold
          ),

          Padding(
            padding: const EdgeInsets.symmetric(vertical: 30),
            child: Image.asset("images/body@3x.png", height: 200,),
          ),

          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 25),
            child: Container(
              width: MediaQuery.of(context).size.width,

              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                border: Border.all(color: MyColors.greyWhite),
              ),

              padding: EdgeInsets.symmetric(horizontal: 15, vertical: 15),

              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [

                  title(text: "طول الكم"),
                  field(context: context, title: "CM"),

                  title(text: "وسع الكم"),
                  field(context: context, title: "000"),

                  title(text: "محيط الكم"),
                  field(context: context, title: "000"),

                  title(text: "القياس من الكتف إلى الصدر"),
                  field(context: context, title: "000"),

                  title(text: "صورة لشكل الفستان"),

                  Container(
                    height: 150,
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                        color: MyColors.white,
                        border: Border.all(color: MyColors.greyWhite),
                        borderRadius: BorderRadius.circular(10)
                    ),

                    child: Image.asset("images/profile@2x.png", color: MyColors.greyWhite,),

                  ),

                  title(text: "الملاحظات"),
                  field(context: context, title: "0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"),

                ],
              ),

            ),
          ),

          Padding(
            padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 15),
            child: DefaultButton(
              title: "الرجوع للطلبات",
              onTap: () {Navigator.of(context)
                  .pushReplacement(CupertinoPageRoute(builder: (_) => ProOrderDetails()));},
            ),
          ),

        ],
      ),

    );
  }
}
