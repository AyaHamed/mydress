import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:intl/intl.dart';
import 'package:my_dress/general/constants/DefaultAppBar.dart';
import 'package:my_dress/general/constants/DefaultButton.dart';
import 'package:my_dress/general/constants/MyColors.dart';
import 'package:my_dress/general/constants/MyText.dart';
import 'package:my_dress/provider/screens/Edit.dart';


class Pro_DressDetails extends StatefulWidget {
  @override
  _Pro_DressDetailsState createState() => _Pro_DressDetailsState();
}
enum SingingCharacter { buy, rent }

class _Pro_DressDetailsState extends State<Pro_DressDetails> {

  int group = 0;
  String date = "اختر التاريخ";
  String date2 = "اختر التاريخ";
  int _qty = 0;

  void _increase() {
    setState(() {
      _qty++;
    });
  }

  void _decrease() {
    setState(() {
      if (_qty != 0) _qty--;
    });
  }

  _rate() {
    showDialog(
        context: context,
        barrierDismissible: false,
        child: AlertDialog(
          contentPadding: EdgeInsets.symmetric(horizontal: 20, vertical: 30),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
          content: Container(
            height: MediaQuery.of(context).size.height * 0.2,
            width: MediaQuery.of(context).size.width * 0.5,


            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                MyText(title: "قم بقييم الفستان الآن  !", color: MyColors.primary,),

                SizedBox(height: 20,),

                RatingBar(
                  initialRating: 0,
                  minRating: 1,
                  allowHalfRating: true,
                  itemCount: 5,
                  itemSize: 30,
                  unratedColor: MyColors.grey,
                  itemBuilder: (context, _) => Icon(
                    Icons.star,
                    color: MyColors.gold,
                  ),
                  onRatingUpdate: (rating) {
                    print(rating);
                  },
                ),

                SizedBox(height: 20,),

                InkWell(
                  onTap: () => Navigator.pop(context),
                  child: Container(
                    height: 50,
                    width: 100,

                    decoration: BoxDecoration(
                      color: MyColors.gold,
                      borderRadius: BorderRadius.circular(10),
                    ),

                    child: Center(
                      child: MyText(title: "تأكيد", color: MyColors.white,),
                    ),
                  ),
                ),

              ],
            ),
          ),
        ));
  }

  _alret() {
    showDialog(
        context: context,
        barrierDismissible: false,
        child: AlertDialog(
          contentPadding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
          content: Container(
            height: 340,
            width: MediaQuery.of(context).size.width * 0.8,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                InkWell(
                  onTap: () => Navigator.pop(context),
                  child: Image.asset(
                    "images/cancel@3x.png",
                    height: 20,
                    width: 20,
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Icon(
                      Icons.check_circle,
                      size: 30,
                      color: MyColors.gold,
                    ),
                    SizedBox(
                      width: 20,
                    ),

                    MyText(
                      title: "أضـيف إلى عربة تسوقك  !",
                      color: MyColors.primary,
                      size: 16,
                      fontWeight: FontWeight.bold,
                    )

                  ],
                ),
                //SizedBox(height: 20),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 20),
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    height: 120,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                          height: 100,
                          width: 60,
                          child: Image.asset(
                            "images/model.PNG",
                            fit: BoxFit.fill,
                          ),
                        ),

                        SizedBox(width: 10,),

                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            MyText(
                              title: "فستان خطوبة وردي",
                              color: MyColors.primary,
                              align: TextAlign.start,
                              fontWeight: FontWeight.bold,
                            ),

                            Container(
                              width: 180,
                              child: MyText(
                                title: "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
                                color: MyColors.grey,
                                size: 15,
                              ),
                            ),

                            Row(
                              children: [
                                MyText(
                                  title: "متوفر حاليا",
                                  color: MyColors.primary,
                                ),
                                SizedBox(
                                  width: 5,
                                ),
                                Icon(
                                  Icons.check_circle,
                                  size: 20,
                                  color: MyColors.gold,
                                ),
                                SizedBox(
                                  width: 20,
                                ),
                                //defaultText("299 رس", MyColors.primary),
                                MyText(
                                  title: "299 رس",
                                  color: MyColors.primary,
                                ),
                              ],
                            ),
                          ],
                        ),

                      ],
                    ),
                  ),
                ),

                DefaultButton(
                  title: "تفاصيل عربة التسوق",
                  color: MyColors.gold,
                  // onTap: () {Navigator.of(context)
                  //     .push(CupertinoPageRoute(builder: (_) => ProHome(2)));},
                ),
                DefaultButton(
                  title: "شراء",
                  color: MyColors.gold,
                  // onTap: () {Navigator.of(context)
                  //     .push(CupertinoPageRoute(builder: (_) => ReviewOrder()));},
                ),
              ],
            ),
          ),
        ));
  }

  Widget title({@required String text, double size, Color fontColor, Color iconColor}){
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      child: Row(
        //mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Icon(Icons.circle, size: 10, color: iconColor?? MyColors.primary,),
          SizedBox(width: 10,),
          //defaultText(text, fontColor?? Colors.blue[900]),
          MyText(
            title: text,
            color: fontColor?? MyColors.primary,
          )
        ],
      ),
    );
  }

  Widget button(String sign, onTap){
    return InkWell(
      onTap: onTap,
      child:  Container(
        height: 30, width: 30,
        decoration: BoxDecoration(
            border: Border.all(color: Colors.grey)
        ),
        child: Center(
          child: Text(sign, style: TextStyle(color: Colors.grey[700], fontSize: 25),),
        ),
      ),
    );
  }

  Widget countButton(){
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
      child: Row(
        children: [
          button("+", _increase),
          Container(
            height: 30, width: 30,
            decoration: BoxDecoration(
                border: Border.all(color: MyColors.grey),
                color: MyColors.greyWhite
            ),
            child: Center(
              child: MyText(
                title: '$_qty',
                color: MyColors.primary,
                size: 15,
              ),
              //Text('$_qty', style: TextStyle(color: MyColors.primary, fontSize: 20),),
            ),
          ),
          button("-", _decrease),
        ],
      ),
    );
  }

  bool _visibility = true;

  void _changeVisibility(bool visibility) {
    setState(() {
      _visibility = !_visibility;
    });
  }

  Widget description(String title, String detail){
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,

      children: [
        Icon(Icons.circle, size: 10, color: MyColors.gold,),
        SizedBox(width: 10,),
        MyText(title: title, color: MyColors.grey,),
        SizedBox(width: 5,),
        MyText(title: detail, color: MyColors.grey,),
      ],
    );
  }

  Widget offstageButton(String text){
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        FlatButton(
            onPressed: () {
              _changeVisibility(false);
            },
            child: MyText(title: text, color: MyColors.primary,)
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Icon(Icons.arrow_drop_down_sharp, color: MyColors.primary, size: 30,),
        ),
      ],
    );
  }

  Widget item(String text){
    return Container(
      height: 30,
      width: MediaQuery.of(context).size.width * 0.4,
      color: MyColors.greyWhite,
      child: Center(
        child: Text(
          text, style: TextStyle(
            color: MyColors.grey, fontSize: 15
        ),
        ),
      ),
    );
  }

  Widget itemDetails(String text, String text2){
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          item(text),
          item(text2),
        ],),
    );
  }

  Widget size(String title) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 5, vertical: 10),
      child: Container(
        height: 30,
        width: 30,
        decoration: BoxDecoration(
          color: MyColors.white,
          border: Border.all(color: MyColors.grey),
        ),
        child: Center(
          child: MyText(
            title: title,
            color: MyColors.primary,
            fontWeight: FontWeight.bold,
            size: 15,
          ),
        ),
      ),
    );
  }

  Widget dressColor(Color color) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 5, vertical: 10),
      child: Container(
        height: 30,
        width: 30,
        decoration: BoxDecoration(
          color: MyColors.white,
          border: Border.all(color: MyColors.grey),
        ),
        child: Center(
          child: Container(
            height: 20,
            width: 20,
            decoration: BoxDecoration(
              color: color,
              borderRadius: BorderRadius.circular(10),
            ),
          ),
        ),
      ),
    );
  }

  _selectDate(String index){
    DatePicker.showDatePicker(context,
        showTitleActions: true,
        minTime: DateTime(2018, 3, 5),
        maxTime: DateTime(2021, 12, 31), onChanged: (date) {
          setState(() {
            if(index=="1"){
              this.date = DateFormat("dd/MM/yyyy", "en").format(date);

            }else{
              this.date2 = DateFormat("dd/MM/yyyy", "en").format(date);

            }
          });
          print('change ${this.date}');
        }, onConfirm: (date) {
          print('confirm $date');
        }, currentTime: DateTime.now(), locale: LocaleType.ar);
  }

  Widget dateButton(String select) {
    return FlatButton(
        onPressed:()=> _selectDate(select),
        child: Container(
          height: 30,
          width: 170,
          decoration: BoxDecoration(
              color: MyColors.white, borderRadius: BorderRadius.circular(20)),
          child: Center(
            child: MyText(
              title: select == "1" ? '${this.date} ' : '${this.date2} ',
              color: MyColors.primary,
              size: 12,
              fontWeight: FontWeight.bold,
            ),
          ),
        ));
  }

  Widget recommended(BuildContext context,){
    return InkWell(

      onTap: (){ Navigator.push(context,
          MaterialPageRoute(builder: (context) => Pro_DressDetails()));},

      child: Container(
        width: MediaQuery.of(context).size.width*.28,

        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,

          children: [
            Container(
              height: 160,
              width: MediaQuery.of(context).size.width*.28,
              child: Image.asset("images/model.PNG", fit: BoxFit.fill,),
            ),

            MyText(title: "فستان خطوبة ", color: MyColors.primary,),
            MyText(title: "detaaaaaaaaaaails", color: MyColors.grey,),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                MyText(title: "ر س", color: MyColors.primary,),
                MyText(title: "299", color: MyColors.primary,),
              ],),

          ],
        ),

      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: InkWell(
        onTap: () {Navigator.of(context)
            .pushReplacement(CupertinoPageRoute(builder: (_) => Edit()));},

        child: Container(
          height: 70,
          width: 70,
          decoration: BoxDecoration(
            color: MyColors.gold,
            borderRadius: BorderRadius.circular(50),
            boxShadow: [
              BoxShadow(
                color: MyColors.grey,
                blurRadius: 4,
                offset: Offset(0, 3)
              )
            ]
          ),

          child: Icon(Icons.edit, size: 40, color: MyColors.white,)
        ),
      ),

      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,

      appBar: DefaultAppBar(title: "فستان خطوبة وردي", con: context,),

      body: ListView(

        children: [


          Container(
            height: 300,
            width: MediaQuery.of(context).size.width,
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemBuilder: (context, index) {
                return Padding(
                  padding:
                  const EdgeInsets.symmetric(horizontal: 5, vertical: 11),
                  child: Image.asset(
                    "images/model.PNG",
                  ),
                );
              },
              itemCount: 4,
            ),
          ),


          Container(
              width: MediaQuery.of(context).size.width,
              color: MyColors.white,
              padding: EdgeInsets.symmetric(vertical: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 20,
                    ),
                    child: Row(
                      children: [
                        MyText(
                          title: "فستان خطوبة وردي",
                          color: MyColors.primary,
                        ),
                        Expanded(child: SizedBox()),
                        Icon(
                          Icons.remove_red_eye,
                          color: MyColors.primary,
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Text("50"),
                        SizedBox(
                          width: 5,
                        ),
                        MyText(
                          title: 'مشاهدة ',
                          color: MyColors.grey,
                        ),
                      ],
                    ),
                  ),


                  Padding(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 20,
                    ),
                    child: Row(
                      children: [
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 10),
                          child: MyText(
                            title: "299",
                            color: MyColors.primary,
                          ),
                        ),
                        MyText(
                          title: "ر س",
                          color: MyColors.primary,
                        ),
                        Expanded(child: SizedBox()),
                        RatingBar(
                          initialRating: 3,
                          minRating: 1,
                          allowHalfRating: true,
                          itemCount: 5,
                          itemSize: 20,
                          unratedColor: MyColors.grey,
                          itemBuilder: (context, _) => Icon(
                            Icons.star,
                            color: MyColors.gold,
                          ),
                        ),

                      ],
                    ),
                  ),


                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: Container(
                      width: 130,
                      height: 30,
                      decoration: BoxDecoration(
                        color: MyColors.greyWhite,
                        borderRadius: BorderRadius.circular(20),
                      ),
                      child: Padding(
                        padding: EdgeInsets.symmetric(horizontal: 3),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            MyText(
                              title: "متوفر حاليا",
                              color: MyColors.grey,
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Icon(
                              Icons.check_circle_outline,
                              color: MyColors.gold,
                              size: 15,
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              )),

          Divider(color: MyColors.greyWhite, thickness: 3,),

          Container(
            width: MediaQuery.of(context).size.width,
            color: MyColors.white,
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 20,vertical: 15),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  MyText(
                    title: "المقاس  : ",
                    color: MyColors.primary,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      size("XL"),
                      size("L"),
                      size("M"),
                      size("S"),
                    ],
                  ),
                  MyText(
                    title: "الألوان  : ",
                    color: MyColors.primary,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      dressColor(MyColors.black),
                      dressColor(MyColors.grey),
                      dressColor(MyColors.brown),
                      dressColor(MyColors.pink),
                    ],
                  ),
                ],
              ),
            ),
          ),


          Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 20,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                MyText(
                  title: "شراء/إيجار  :",
                  color: MyColors.primary,
                )
              ],
            ),
          ),


          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                width: MediaQuery.of(context).size.width * 0.4,
                child: ListTile(
                  title: MyText(
                    title: "إيجار",
                    color: MyColors.primary,
                  ),
                  leading: Radio(
                    value: 1,
                    groupValue: group,
                    onChanged: (value) {
                      print(value);
                      setState(() {
                        group = value;
                      });
                    },
                  ),
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width * 0.4,
                child: ListTile(
                  title: MyText(
                    title: "شراء",
                    color: MyColors.primary,
                  ),
                  leading: Radio(
                    value: 0,
                    groupValue: group,
                    onChanged: (value) {
                      group = value;
                      setState(() {});
                      print(group);
                    },
                  ),
                ),
              ),
            ],
          ),


          Visibility(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
              child: Divider(
                thickness: 2,
              ),
            ),
            visible: group == 0 ? true : false,
            replacement: Container(
              color: MyColors.greyWhite,
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 25, vertical: 15),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      title(text : "مدة الإيجار  :"),
                      Spacer(),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          MyText(title: "من", color: MyColors.primary,),
                          MyText(title: "إلى", color: MyColors.primary,),
                        ],
                      ),
                      Column(
                        children: [
                          dateButton("1"),
                          dateButton("2"),
                        ],
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      title(text: "تأمين الإيجار  : "),
                      Spacer(),
                      MyText(title: "50 رس", color: MyColors.grey,),
                      Expanded(child: SizedBox()),

                      title(text: "لليوم الواحد", fontColor: MyColors.grey, iconColor: MyColors.gold)

                    ],
                  ),
                ],
              ),
            ),
          ),


          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                child: MyText(title: "الكمية : ", color: MyColors.primary,),
              ),
              SizedBox(
                width: 15,
              ),
              countButton(),

            ],
          ),

          Divider(color: MyColors.greyWhite, thickness: 3,),

          Container(
              width: MediaQuery.of(context).size.width,
              color: MyColors.white,

              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),

              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  MyText(title: "الوصف :  ", color: MyColors.primary,),
                  description("كود المنتج :  ", "aaaaaaa"),
                  description("الكمية :  ", "200"),
                  description("ضريبة الشحن :  ", "مجاني"),

                  Container(
                    padding: EdgeInsets.only(top: 10),
                    width: MediaQuery.of(context).size.width,
                    height: 70,
                    child: MyText(
                      title: "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
                      align: TextAlign.start,
                      color: MyColors.grey,
                    ),)
                ],
              )
          ),

          offstageButton("تفاصيل المنتج   : "),

          Offstage(
            offstage: _visibility,

            child: Container(
              width: MediaQuery.of(context).size.width,
              color: MyColors.white,

              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),

              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,

                children: [
                  Image.asset("images/visalogo.png", height: 80,),

                  MyText(title: "الوصف : ", color: MyColors.primary,),
                  description("كود المنتج : ", "aaaaaaa"),
                  description("الكمية : ", "200"),
                  description("ضريبة الشحن : ", "مجاني"),
                  SizedBox(height: 10,),
                  itemDetails("نوع القماش", "قطن 100%"),
                  itemDetails("نوع القماش", "قطن 100%"),
                  itemDetails("نوع القماش", "قطن 100%"),

                ],
              ),

            ),
          ),

          offstageButton("قياسات المنتج   : "),

          Offstage(
            offstage: _visibility,

            child: Container(
              width: MediaQuery.of(context).size.width,
              color: MyColors.white,

              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),

              child: Image.asset("images/measurement.PNG"),

            ),
          ),

          Divider(color: MyColors.greyWhite, thickness: 1,),

          Container(
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.symmetric(vertical: 10, horizontal: 15),

            child: Wrap(
              spacing: 10,
              runSpacing: 15,
              children: [
                recommended(context),
                recommended(context),
                recommended(context),
                recommended(context),
                recommended(context),
                recommended(context),
              ],
            ),
          ),

          SizedBox(height: 80,)
        ],
      ),
    );
  }
}
