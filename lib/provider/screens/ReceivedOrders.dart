import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_dress/general/constants/DefaultAppBar.dart';
import 'package:my_dress/general/constants/MyColors.dart';
import 'package:my_dress/provider/screens/Pro_OrderDetails.dart';
import 'package:my_dress/provider/widgets/orderItem.dart';

class ReceivedOrders extends StatefulWidget {
  @override
  _ReceivedOrdersState createState() => _ReceivedOrdersState();
}

class _ReceivedOrdersState extends State<ReceivedOrders> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar(con: context, title: "الطلبات الواردة"),

      body: Padding(
        padding: const EdgeInsets.all(20),
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            border: Border.all(color: MyColors.greyWhite),
            borderRadius: BorderRadius.circular(5)
          ),

          child: ListView.builder(
            itemCount: 4,
              itemBuilder: (context, index){
                return orderItem(context: context, onTap: (){ Navigator.push(context,
                    CupertinoPageRoute(builder: (context) => ProOrderDetails()));},);
              })
        ),
      ),

    );
  }
}
