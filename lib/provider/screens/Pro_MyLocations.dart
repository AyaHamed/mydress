import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_dress/general/constants/DefaultAppBar.dart';
import 'package:my_dress/general/constants/DefaultButton.dart';
import 'package:my_dress/general/constants/MyColors.dart';
import 'package:my_dress/general/constants/MyText.dart';

import 'ProNewLocation.dart';

class Pro_MyLocations extends StatefulWidget {
  @override
  _Pro_MyLocationsState createState() => _Pro_MyLocationsState();
}

class _Pro_MyLocationsState extends State<Pro_MyLocations> {

  Widget customItem(BuildContext context, String text) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 15),
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: 80,

        decoration: BoxDecoration(
            color: MyColors.white,
            border: Border.all(color: MyColors.greyWhite)
        ),

        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [

              Icon(Icons.location_on, size: 30, color: MyColors.grey,),
              MyText(title: text, color: MyColors.grey),
              Icon(Icons.delete_outline,
                color: MyColors.primary,  size: 30,),
            ],),
        ),
      ),
    );
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar(con: context, title: "مواقع الشحن"),
      body: Padding(
          padding: const EdgeInsets.only(top: 20),
          child: ListView(
            children: [
              customItem(context, "ش الملك عبدالعزيز - الرياض، السعودية 25"),
              customItem(context, "ش الملك عبدالعزيز - الرياض، السعودية 25"),
              customItem(context, "ش الملك عبدالعزيز - الرياض، السعودية 25"),
              customItem(context, "ش الملك عبدالعزيز - الرياض، السعودية 25"),

              Padding(
                padding: const EdgeInsets.symmetric(vertical: 25, horizontal: 15),
                child: DefaultButton(
                  title: "إضف عنوان جديد",
                  onTap: () {Navigator.of(context).push(CupertinoPageRoute(builder: (_) => ProNewLocation()));},
                ),
              )

            ],
          )

      ),

    );
  }
}
