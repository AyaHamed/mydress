import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_dress/customer/screens/Home.dart';
import 'package:my_dress/general/constants/DefaultAppBar.dart';
import 'package:my_dress/general/constants/DefaultButton.dart';
import 'package:my_dress/general/constants/IconTextFiled.dart';
import 'package:my_dress/general/constants/MyColors.dart';

class ProNewLocation extends StatefulWidget {
  @override
  _ProNewLocationState createState() => _ProNewLocationState();
}

class _ProNewLocationState extends State<ProNewLocation> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar(con: context, title: "موقع الشحن"),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
        child: Container(
          width: MediaQuery.of(context).size.width,

          decoration: BoxDecoration(
              color: MyColors.white,
              border: Border.all(color: MyColors.greyWhite),
              borderRadius: BorderRadius.circular(10)
          ),

          padding: EdgeInsets.symmetric(vertical: 35,),

          child: ListView(
            children: [
              IconTextFiled(
                label: "الاسم الأول",
                borderColor: MyColors.greyWhite,
              ),

              IconTextFiled(
                label: "اسم العائلة",
                borderColor: MyColors.greyWhite,
              ),

              IconTextFiled(
                label: "البلد/المنطقة",
                borderColor: MyColors.greyWhite,
              ),

              IconTextFiled(
                label: "المحافظة",
                borderColor: MyColors.greyWhite,
              ),

              IconTextFiled(
                label: "المدينة",
                borderColor: MyColors.greyWhite,
              ),

              IconTextFiled(
                label: "المدينة",
                borderColor: MyColors.greyWhite,
              ),

              IconTextFiled(
                label: "عنوان المنزل",
                borderColor: MyColors.greyWhite,
              ),

              IconTextFiled(
                label: "رقم الجوال",
                borderColor: MyColors.greyWhite,
              ),

              IconTextFiled(
                label: "البريد الالكتروني",
                borderColor: MyColors.greyWhite,
              ),

              SizedBox(height: 30,),

              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: DefaultButton(
                  title: "إضافة العنوان",
                  onTap: () {Navigator.of(context).push(CupertinoPageRoute(builder: (_) => Home(0)));},
                ),

              )

            ],
          ),
        ),
      ),
    );
  }
}
