import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_dress/customer/widgets/title.dart';
import 'package:my_dress/general/constants/DefaultAppBar.dart';
import 'package:my_dress/general/constants/DefaultButton.dart';
import 'package:my_dress/general/constants/MyColors.dart';
import 'package:my_dress/provider/widgets/field.dart';
import 'package:my_dress/provider/widgets/newSteps.dart';

import 'Customized2.dart';

class Customized1 extends StatefulWidget {
  @override
  _Customized1State createState() => _Customized1State();
}

class _Customized1State extends State<Customized1> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar(con: context, title: "تفصيل فستان"),
      body: ListView(
        children: [

          steps(
            context: context,
            border1: MyColors.primary,
            fill1: MyColors.gold,
          ),

          Padding(
            padding: const EdgeInsets.symmetric(vertical: 30),
            child: Image.asset("images/body@3x.png", height: 200,),
          ),

          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 25),
            child: Container(
              width: MediaQuery.of(context).size.width,

              decoration: BoxDecoration(
                //color: Colors.green,
                borderRadius: BorderRadius.circular(8),
                border: Border.all(color: MyColors.greyWhite),
              ),

              padding: EdgeInsets.symmetric(horizontal: 15, vertical: 15),

              child: Column(
                //crossAxisAlignment: CrossAxisAlignment.start,
                children: [

                  title(text: "وحدة القياس"),
                  field(context: context, title: "CM"),

                  title(text: "الوزن"),
                  field(context: context, title: "50"),

                  title(text: "الطول"),
                  field(context: context, title: "164"),

                  title(text: "نوع القماش"),
                  field(context: context, title: "شيفون"),

                  title(text: "لون القماش"),
                  field(context: context, title: "وردي"),

                  title(text: "طول الفستان من الكتف للارض"),
                  field(context: context, title: "100"),

                ],
              ),

            ),
          ),

          Padding(
            padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 15),
            child: DefaultButton(
              color: MyColors.primary,
              title: "التالي",
              onTap: () {Navigator.of(context)
                  .pushReplacement(CupertinoPageRoute(builder: (_) => Customized2()));},
            ),
          ),

        ],
      ),
    );
  }
}
