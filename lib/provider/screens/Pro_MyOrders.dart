import 'package:flutter/material.dart';
import 'package:my_dress/general/constants/DefaultAppBar.dart';
import 'package:my_dress/general/constants/MyColors.dart';
import 'package:my_dress/general/constants/MyText.dart';
import 'package:my_dress/provider/screens/FinishedOrders.dart';
import 'package:my_dress/provider/screens/PreparedOrders.dart';
import 'package:my_dress/provider/screens/ReceivedOrders.dart';

class Pro_MyOrders extends StatefulWidget {
  @override
  _Pro_MyOrdersState createState() => _Pro_MyOrdersState();
}

class _Pro_MyOrdersState extends State<Pro_MyOrders> {

  Widget item(String title, Widget page,){
    return InkWell(
      onTap: (){ Navigator.push(context,
          MaterialPageRoute(builder: (context) => page));},

      child: Container(

        height: 70,
        width: MediaQuery.of(context).size.width,

        decoration: BoxDecoration(
            color: MyColors.white,
            border: Border.all(color: MyColors.greyWhite)
        ),

        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,

            children: [
              MyText(title: title,
                size: 15,
                color: MyColors.primary,
              ),
              Expanded(child: SizedBox()),
              Icon(Icons.arrow_left_sharp, color: MyColors.primary, size: 22,),
            ],
          ),
        ),

      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(

      appBar: DefaultAppBar(
        con: context,
        title: "طلباتي",
        leading: Container(),
      ),

      body: Container(
        padding: EdgeInsets.all(10),
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
            border: Border.all(color: MyColors.greyWhite),
            borderRadius: BorderRadius.circular(5)
        ),

        child: Column(
          children: [
            item("الطلبات الواردة", ReceivedOrders(),),
            item("طلبات قيد التنفيذ", PreparedOrders()),
            item("الطلبات المنتهية", FinishedOrders()),
          ],
        ),
      ),

    );
  }
}
