import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_dress/customer/widgets/dressColor.dart';
import 'package:my_dress/customer/widgets/title.dart';
import 'package:my_dress/general/constants/DefaultAppBar.dart';
import 'package:my_dress/general/constants/DefaultButton.dart';
import 'package:my_dress/general/constants/MyColors.dart';
import 'package:my_dress/general/constants/MyText.dart';
import 'package:my_dress/provider/screens/PreparedOrders.dart';
import 'package:my_dress/provider/screens/ReceivedOrders.dart';

class ItemDetails extends StatefulWidget {
  @override
  _ItemDetailsState createState() => _ItemDetailsState();
}

class _ItemDetailsState extends State<ItemDetails> {

  Widget itemRow({String text, Widget detail, String textDetail}){
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        title(
          text: text,
          fontColor: MyColors.primary,
          iconColor: MyColors.primary,
        ),

        Container(
          width: MediaQuery.of(context).size.width *0.5,
          padding: EdgeInsets.symmetric(vertical: 15, horizontal: 10),
          child: detail == null ? MyText(title: textDetail, color: MyColors.grey,)
              : Container(child: detail,),
        )

      ],
    );
  }

  Widget _itemDetail({String text, String detail, bool extra = false, String more}){
    return  Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        title(
            text: text,
            icon: false,
            fontColor: MyColors.primary,
            padding: false
        ),
        MyText(
          title: detail,
          color: MyColors.grey,
        ),

        extra == true ? title(
            text: more,
            fontColor: MyColors.primary,
            iconColor: MyColors.gold,
            padding: false
        ) : Container(),

      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar(con: context, title: "تفاصيل المنتج"),

      body: Padding(
        padding: const EdgeInsets.all(20),
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,

          decoration: BoxDecoration(
            color: MyColors.white,
              border: Border.all(color: MyColors.greyWhite),
              borderRadius: BorderRadius.circular(5)
          ),

          child: ListView(
            children: [

              itemRow(
                text: "صورة المنتج :",
                detail: Image.asset("images/model.PNG", height: 200,),
              ),

              itemRow(
                text: "اسم المنتج :",
                textDetail: "فستان خطوبة وردي"
              ),
              itemRow(
                text: "كود المنتج :",
                textDetail: "abcd1234"
              ),
              itemRow(
                text: "الكمية :",
                textDetail: "1"
              ),
              itemRow(
                text: "سعر المنتج :",
                textDetail: "299 ر س"
              ),
              itemRow(
                text: "وصف المنتج :",
                textDetail: "فستان خطوبة وردي فستان خطوبة وردي فستان خطوبة وردي فستان خطوبة وردي فستان خطوبة وردي فستان خطوبة وردي فستان خطوبة وردي فستان خطوبة وردي "
              ),
              itemRow(
                text: "اسم المنتج :",
                detail: Column(
                  children: [
                    _itemDetail(
                        text: "العلامة التجارية",
                        detail: "aaaaaaaa"
                    ),
                    _itemDetail(
                        text: "نوع القماش",
                        detail: "شيفون"
                    ),
                    _itemDetail(
                        text: "نمط الفستان",
                        detail: "لون محدد"
                    ),
                    _itemDetail(
                        text: "شكل الفستان",
                        detail: "قصة مستقيمة"
                    ),
                  ],
                )
              ),

              itemRow(
                text: "الوان المنتج :",
                detail: Row(
                  children: [
                    dressColor(MyColors.brown, padding: false),
                    dressColor(MyColors.pink, padding: false),
                    dressColor(MyColors.blackOpacity, padding: false),
                    dressColor(MyColors.grey, padding: false),
                  ],
                ),
              ),

              itemRow(
                text: "مقاسات المنتج :",
                textDetail: "Small"
              ),

              itemRow(
                  text: "شراء/إيجار :",
                  textDetail: "إيجار"
              ),

              Container(
                width: MediaQuery.of(context).size.width,
                color: Colors.grey[100],
                padding: EdgeInsets.all(15),
                child: Column(
                  children: [
                    _itemDetail(
                      text: "تأمين الايجار",
                      detail: "50 رس",
                      extra: true,
                      more: "لليوم الواحد"
                    ),
                    _itemDetail(
                        text: "من :",
                        detail: "25/10/2020"
                    ),
                    _itemDetail(
                        text: "إلى :",
                        detail: "26/10/2020"
                    ),
                  ],
                ),
              ),

              itemRow(
                text: "القسم :",
                textDetail: "فساتين خطوبة"
              ),

              itemRow(
                text: "الفئة العمرية :",
                textDetail: "سيدات"
              ),

              itemRow(
                text: "المبلغ الإجمالي :",
                textDetail: "299 رس"
              ),

              SizedBox(height: 20,),

              DefaultButton(
                title: "قبول",
                onTap: (){ Navigator.pushReplacement(context,
                    CupertinoPageRoute(builder: (context) => PreparedOrders()));},
              ),
              DefaultButton(
                title: "رفض",
                onTap: (){ Navigator.pushReplacement(context,
                    CupertinoPageRoute(builder: (context) => ReceivedOrders()));},
              ),

            ],
          ),

        ),
      ),

    );
  }
}
