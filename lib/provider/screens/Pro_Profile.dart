import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_dress/customer/screens/Home.dart';
import 'package:my_dress/general/constants/DefaultAppBar.dart';
import 'package:my_dress/general/constants/DefaultButton.dart';
import 'package:my_dress/general/constants/IconTextFiled.dart';
import 'package:my_dress/general/constants/MyColors.dart';

class Pro_Profile extends StatefulWidget {
  @override
  _Pro_ProfileState createState() => _Pro_ProfileState();
}

class _Pro_ProfileState extends State<Pro_Profile> {

  Widget info(double top, String text, Color color){
    return Positioned(
      top: MediaQuery.of(context).size.height * top,
      child: Container(
        height: 30,
        width: MediaQuery.of(context).size.width,
        child: Text(
          text,
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 18,
            fontWeight: FontWeight.bold,
            color: color,
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[100],
      appBar: DefaultAppBar(con: context, title: "الملف الشخصي",
        leading: Container(),
      ),

      body: ListView(
        children: [
          Stack(
            children: [
              Image.asset(
                "images/cover@2x.png",
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height * 0.26,
              ),

              Positioned(
                top: MediaQuery.of(context).size.height * 0.018,
                left: MediaQuery.of(context).size.width * 0.36,
                child: Container(
                  height: 130, width: 130,
                  decoration: BoxDecoration(
                      color: Colors.grey[400],
                      borderRadius: BorderRadius.circular(90)
                  ),
                  child: Image.asset("images/profile@2x.png",),
                ),
              ),

              Positioned(
                top: MediaQuery.of(context).size.height * 0.145,
                left: MediaQuery.of(context).size.width * 0.38,
                child: Container(
                  height: 30, width: 30,
                  decoration: BoxDecoration(
                      color: Colors.grey[700],
                      borderRadius: BorderRadius.circular(90)
                  ),
                  child: Icon(Icons.edit, color: MyColors.white,),
                ),
              ),

              info(0.18, "إسراء العجمي", MyColors.primary),
              info(0.21, "aaaa@gmail.com", MyColors.grey),
            ],
          ),

          IconTextFiled(label: "الاسم التجاري",),

          IconTextFiled(label: "تاريخ الميلاد",
            prefix: Icon(Icons.calendar_today_outlined),),

          IconTextFiled(label: "البلد/المنطقة",
            prefix: Icon(Icons.location_on),),

          IconTextFiled(label: "عنوان المنزل",
            prefix: Icon(Icons.location_on),),

          IconTextFiled(label: "رقم الجوال"),

          IconTextFiled(label: "البريد الالكتروني",),

          IconTextFiled(label: "كلمة المرور",
            isPassword: true,),

          Padding(
            padding: EdgeInsets.symmetric(horizontal: 30, vertical: 5),
            child: DefaultButton(
              title: "حفظ",
              //onTap: null
            ),
          ),

          SizedBox(height: 40,),

        ],
      ),
    );
  }
}
