import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_dress/customer/screens/OrderDetails.dart';
import 'package:my_dress/general/constants/DefaultAppBar.dart';
import 'package:my_dress/general/constants/MyColors.dart';
import 'package:my_dress/provider/screens/ProOrders.dart';
import 'package:my_dress/provider/screens/Pro_MyOrders.dart';
import 'package:my_dress/provider/screens/Pro_OrderDetails.dart';
import 'package:my_dress/provider/widgets/orderItem.dart';

class FinishedOrders extends StatefulWidget {
  @override
  _FinishedOrdersState createState() => _FinishedOrdersState();
}

class _FinishedOrdersState extends State<FinishedOrders> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar(con: context, title: "الطلبات المنتهية",),

      body: Padding(
        padding: const EdgeInsets.all(20),
        child: Container(
          width: MediaQuery.of(context).size.width,

          child: ListView.builder(
            itemCount: 4,
              itemBuilder: (context, index){
            return orderItem(
              context: context,
              icon: true,
              onTap: (){ Navigator.pushReplacement(context,
                  CupertinoPageRoute(builder: (context) => ProOrderDetails()));},
            );
          })

        ),
      ),

    );
  }
}
