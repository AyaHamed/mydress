import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_dress/general/constants/DefaultAppBar.dart';
import 'package:my_dress/general/constants/MyColors.dart';
import 'package:my_dress/general/constants/MyText.dart';
import 'package:my_dress/provider/screens/Customized1.dart';
import 'package:my_dress/provider/screens/ItemDetails.dart';

class ProOrderDetails extends StatefulWidget {
  @override
  _ProOrderDetailsState createState() => _ProOrderDetailsState();
}

class _ProOrderDetailsState extends State<ProOrderDetails> {

  Widget detailItem(String title, String detail){
    return Row(
      children: [
        MyText(title: title, color: MyColors.primary),
        Text("  :  "),
        MyText(title: detail, color: MyColors.grey),
      ],
    );
  }

  Widget details(){
    return InkWell(
      onTap: (){ Navigator.push(context,
          CupertinoPageRoute(builder: (context) => ItemDetails()));},
      child: Container(
        //height: 85,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
            border: Border.all(color: MyColors.greyWhite),
            color: MyColors.white
        ),
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
        child:  Row(
          children: [
            Container(
              height: 120,
              width: 100,
              child: Image.asset("images/model.PNG", fit: BoxFit.fill,),
            ),

            SizedBox(width: 10,),

            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                MyText(title: "فستان خطوبة رمادي", color: MyColors.primary),
                SizedBox(height: 5,),
                detailItem("تاريخ الطلب", "25/10/2020"),
                detailItem("سعر الطلب", "299 رس"),
                detailItem("الكمية", "1"),

              ],
            ),

          ],
        ),
      ),
    );
  }

  Widget tailoringOrder(){
    return InkWell(
      onTap: (){ Navigator.pushReplacement(context,
          CupertinoPageRoute(builder: (context) => Customized1()));},
      child: Container(
        //height: 85,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
            border: Border.all(color: MyColors.greyWhite),
            color: MyColors.white
        ),
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
        child:  Row(
          children: [
            Container(
              height: 120,
              width: 100,
              child: Image.asset("images/tailoring.PNG", fit: BoxFit.fill,),
            ),

            SizedBox(width: 10,),

            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                MyText(title: "طلب تفصيل فستان", color: MyColors.primary),
                SizedBox(height: 5,),
                detailItem("تاريخ الطلب", "25/10/2020"),
                detailItem("سعر الطلب", "299 رس"),
                detailItem("الكمية", "1"),

              ],
            ),

          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar(con: context, title: "تفاصيل الطلب"),

      body: Padding(
        padding: const EdgeInsets.all(20),
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
              color: MyColors.white,
              border: Border.all(color: MyColors.greyWhite),
              borderRadius: BorderRadius.circular(5)
          ),

          child: ListView(
            children: [
              details(),
              details(),
              tailoringOrder(),
              tailoringOrder(),
            ],
          ),
        ),
      ),

    );
  }
}
