import 'package:flutter/material.dart';
import 'package:my_dress/customer/screens/Tailoring.dart';
import 'package:my_dress/customer/widgets/DressCategory.dart';
import 'package:my_dress/general/constants/DefaultAppBar.dart';
import 'package:my_dress/general/constants/MyColors.dart';
import 'package:my_dress/provider/screens/ProHome.dart';
import 'package:my_dress/provider/widgets/ProDrawer.dart';

class ProHomePage extends StatefulWidget {

  @override
  _ProHomePageState createState() => _ProHomePageState();
}

class _ProHomePageState extends State<ProHomePage> {
  GlobalKey<ScaffoldState> scaffold=GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffold,

      appBar: DefaultAppBar(
        title: "الرئيسية",
        con: context,
        leading: IconButton(
          onPressed: () => scaffold.currentState.openDrawer(),
          icon: Icon(
            Icons.segment,
            color: MyColors.primary,
            size: 30,),),),

      drawer: proDrawer(context),

      // body: ListView(
      //   children: [
      //     SizedBox(height: 10,),
      //
      //     dressCategory(context: context, image: "images/wedding.PNG", page: ProHome(2)),
      //     dressCategory(context: context, image: "images/engagement.PNG", page: ProHome(2)),
      //     dressCategory(context: context, image: "images/monasbat.PNG", page: ProHome(2)),
      //     dressCategory(context: context, image: "images/sahra.PNG", page: ProHome(2)),
      //     dressCategory(context: context, image: "images/special.PNG", page: ProHome(2)),
      //     dressCategory(context: context, image: "images/tafseel.PNG", page: Tailoring()),
      //   ],
      // ),
    );
  }
}
