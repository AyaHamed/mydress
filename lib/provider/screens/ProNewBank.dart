import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_dress/customer/screens/Home.dart';
import 'package:my_dress/general/constants/DefaultAppBar.dart';
import 'package:my_dress/general/constants/DefaultButton.dart';
import 'package:my_dress/general/constants/IconTextFiled.dart';
import 'package:my_dress/general/constants/MyColors.dart';

import 'ProHome.dart';

class ProNewBank extends StatefulWidget {
  @override
  _ProNewBankState createState() => _ProNewBankState();
}

class _ProNewBankState extends State<ProNewBank> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(

      appBar: DefaultAppBar(con: context, title: "حساب جديد"),

      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
        child: Container(

          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,

          decoration: BoxDecoration(
              color: MyColors.white,
              border: Border.all(color: MyColors.greyWhite),
              borderRadius: BorderRadius.circular(10)
          ),

          padding: EdgeInsets.symmetric(vertical: 35,),

          child: ListView(
            children: [

              IconTextFiled(
                label: "نوع الحساب",
                borderColor: MyColors.greyWhite,
              ),

              IconTextFiled(
                label: "كود الحساب",
                borderColor: MyColors.greyWhite,
              ),

              IconTextFiled(
                label: "اسم الحساب",
                borderColor: MyColors.greyWhite,
              ),

              IconTextFiled(
                label: "رقم الحساب",
                borderColor: MyColors.greyWhite,
              ),

              IconTextFiled(
                label: "اسم البنك",
                borderColor: MyColors.greyWhite,
              ),



              SizedBox(height: 30,),

              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: DefaultButton(
                  title: "أضف الحساب",
                  onTap: () {Navigator.of(context).push(CupertinoPageRoute(builder: (_) => ProHome(0)));},
                ),

              )

            ],
          ),
        ),
      ),

    );
  }
}
