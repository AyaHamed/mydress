import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_dress/general/constants/DefaultAppBar.dart';
import 'package:my_dress/general/constants/DefaultButton.dart';
import 'package:my_dress/general/constants/MyColors.dart';
import 'package:my_dress/general/constants/MyText.dart';
import 'ProEditBank.dart';
import 'ProNewBank.dart';


class Pro_BankAccounts extends StatefulWidget {
  @override
  _Pro_BankAccountsState createState() => _Pro_BankAccountsState();
}

class _Pro_BankAccountsState extends State<Pro_BankAccounts> {

  Widget row(String title, String detail, IconData icon, Function onTap){
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Icon(Icons.circle, size: 8, color: MyColors.grey),
        MyText(title: title, color: MyColors.grey),
        SizedBox(width: 15,),
        MyText(title: detail, color: MyColors.grey),
        Spacer(),
        InkWell(
          onTap: onTap,
          child: Icon(icon,
            color: MyColors.primary,  size: 20,),
        ),
      ],);
  }

  Widget item(BuildContext context, String text, String accnum) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 15),
      child: Container(
        width: MediaQuery.of(context).size.width,

        decoration: BoxDecoration(
            color: MyColors.white,
            border: Border.all(color: MyColors.greyWhite)
        ),

        child: Padding(
            padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
            child: Column(
              children: [

                row("  اسم البنك     :  ", text, Icons.edit, (){ Navigator.push(context,
                    CupertinoPageRoute(builder: (context) => ProEditBank()));},),

                row("  رقم الحساب    :  ", accnum, Icons.delete_outline, null),

              ],
            )

        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar(con: context, title: "الحساب البنكي"),
      body: Padding(
          padding: const EdgeInsets.only(top: 20),
          child: ListView(
            children: [

              item(context, "البنك الأهلي", "123456789000000000"),
              item(context, "البنك الأهلي", "123456789000000000"),
              item(context, "البنك الأهلي", "123456789000000000"),

              Padding(
                padding: const EdgeInsets.symmetric(vertical: 25, horizontal: 15),
                child: DefaultButton(
                  title: "أضف حساب جديد",
                  onTap: (){ Navigator.push(context,
                      CupertinoPageRoute(builder: (context) => ProNewBank()));},
                ),
                //defaultButton(context: context, title: "أضف حساب جديد", page: NewBankAcc()),
              )

            ],
          )

      ),

    );
  }
}
