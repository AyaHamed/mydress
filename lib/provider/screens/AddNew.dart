import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_dress/general/constants/DefaultAppBar.dart';
import 'package:my_dress/general/constants/DefaultButton.dart';
import 'package:my_dress/general/constants/LabelTextField.dart';
import 'package:my_dress/general/constants/MyColors.dart';
import 'package:my_dress/general/constants/MyText.dart';
import 'package:my_dress/provider/screens/AddNew2.dart';
import 'package:my_dress/provider/widgets/newSteps.dart';

class AddNew extends StatefulWidget {
  @override
  _AddNewState createState() => _AddNewState();
}

class _AddNewState extends State<AddNew> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar(
        con: context,
        title: "إضافة منتج",
      ),

      body: ListView(
        physics: NeverScrollableScrollPhysics(),

        children: [

          steps(
            context: context,
            border1: MyColors.primary,
            fill1: MyColors.gold,
          ),

          Padding(
            padding: const EdgeInsets.only(right: 20, left: 20),
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height - 300,
              padding: EdgeInsets.symmetric(vertical: 20),
              decoration: BoxDecoration(
                color: MyColors.white,
                borderRadius: BorderRadius.circular(5),
                border: Border.all(color: MyColors.greyWhite)
              ),

              child: ListView(
                children: [

                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                    child: MyText(
                      title: "المعلومات الأساسية",
                      color: MyColors.primary,
                      size: 17,
                      fontWeight: FontWeight.bold,
                      align: TextAlign.start,
                    ),
                  ),

                  //SizedBox(height: 30,),

                  LabelTextField(label: "اسم المنتج",),
                  LabelTextField(label: "كود المنتج",),
                  LabelTextField(label: "سعر المنتج",),
                  LabelTextField(label: "الكمية",),
                  LabelTextField(label: "وصف المنتج",),

                ],
              ),

            ),
          ),
        ],
      ),bottomNavigationBar: Padding(
        padding: const EdgeInsets.symmetric(vertical: 25),
        child: DefaultButton(
          title: "التالي",
          onTap: () {Navigator.of(context)
              .push(CupertinoPageRoute(builder: (_) => AddNew2()));},
    ),
      ),
    );
  }
}
