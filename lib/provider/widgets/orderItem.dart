import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_dress/general/constants/MyColors.dart';
import 'package:my_dress/general/constants/MyText.dart';

Widget orderItem({
  BuildContext context,
  Function onTap,
  String name,
  String date,
  String image,
  bool icon,
})
{
  return InkWell(
    onTap: onTap?? null,
    child: Container(
      height: 85,
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
          border: Border.all(color: MyColors.greyWhite),
          color: MyColors.white
      ),
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: Row(
        children: [
          CircleAvatar(
            backgroundColor: MyColors.grey,
            child: Image.asset(image?? "images/profile@2x.png", fit: BoxFit.fill,),
            radius: 25,
          ),
          SizedBox(width: 15,),
          MyText(
            title: name?? "اسراء العجمي",
            color: MyColors.primary,
            size: 13,
          ),
          Spacer(),
          MyText(
            title: date?? "25/10/2020",
            color: MyColors.primary,
          ),
          icon == true? SizedBox(width: 20,): Container(),
          icon == true?
          Icon(
            Icons.check_circle_outline_outlined,
            color: MyColors.gold,
            size: 20,) : Container()
        ],
      ),
    ),
  );
}