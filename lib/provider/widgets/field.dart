import 'package:flutter/material.dart';
import 'package:my_dress/general/constants/MyColors.dart';
import 'package:my_dress/general/constants/MyText.dart';

Widget field({BuildContext context,String title}){
  return Container(
    width: MediaQuery.of(context).size.width,
    //height: 50,
    decoration: BoxDecoration(
      border: Border.all(color: MyColors.greyWhite),
      borderRadius: BorderRadius.circular(10),
    ),
    padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
    child: MyText(
      title: title,
      color: MyColors.grey,
    ),
  );
}