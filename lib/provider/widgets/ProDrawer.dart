import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_dress/customer/screens/ContactUs.dart';
import 'package:my_dress/customer/screens/Settings.dart';
import 'package:my_dress/general/constants/MyColors.dart';
import 'package:my_dress/general/constants/MyText.dart';
import 'package:my_dress/general/screens/Ploicies.dart';
import 'package:my_dress/general/screens/Register.dart';
import 'package:my_dress/provider/screens/ProHome.dart';
import 'package:my_dress/provider/screens/Pro_BankAccounts.dart';
import 'package:my_dress/provider/screens/Pro_MyLocations.dart';

Widget proDrawer(context) {

  Widget contactInfo(String text){
    return MyText(
      title: text,
      align: TextAlign.center,
      size: 12,
      fontWeight: FontWeight.bold,
      color: MyColors.white,
    );
  }

  Widget drawerItem(context, String title, IconData icon, Widget page){
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 25, vertical: 10),
      child: InkWell(
        onTap: (){ Navigator.push(context,
            CupertinoPageRoute(builder: (context) => page));},
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Icon(icon, color: MyColors.white, size: 25,),
            SizedBox(width: 10,),
            MyText(
              title: title,
              color: MyColors.white,
              size: 15,
            ),
          ],
        ),
      ),
    );
  }

  return Drawer(
    child: Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,

        decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("images/bg_menu@2x.png"),
              fit: BoxFit.fill,
            )
        ),

        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(height: 30,),

              Container(
                height: 100, width: 100,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(50),
                    color: MyColors.greyWhite
                ),
                child: Image.asset("images/profile@2x.png",),
              ),

              SizedBox(height: 10,),

              contactInfo("إسراء العجمي"),
              contactInfo("aaaaaaaaaaa@gmail.com"),

              Padding(
                padding: EdgeInsets.symmetric(horizontal: 35, vertical: 10),
                child: Divider( color: MyColors.white, thickness: 1,),
              ),

              drawerItem(context, "الرئيسية ", Icons.home_rounded, ProHome(0)),
              drawerItem(context, "مواقع الشحن", Icons.location_on, Pro_MyLocations()),
              drawerItem(context, "الحساب البنكي  ", Icons.credit_card, Pro_BankAccounts()),
              drawerItem(context, "السياسة العامة", Icons.help_rounded, Policies()),
              drawerItem(context, "الاعدادات   ", Icons.settings, Settings()),
              drawerItem(context, "تواصل معنا", Icons.chat, ContactUs()),
              drawerItem(context, "تسجيل خروج", Icons.logout, Register()),

            ],
          ),
        ),

      ),

    ),
  );
}