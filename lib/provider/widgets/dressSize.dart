import 'package:flutter/material.dart';
import 'package:my_dress/general/constants/MyColors.dart';
import 'package:my_dress/general/constants/MyText.dart';

Widget dressSize({String size1, String size2, String size3}){
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      size(size: size1),
      size(size: size2),
      size(size: size3),
    ],
  );
}

Widget size({String size,}){
  bool value = false;
  return Row(
    children: [
      Checkbox(value: value,
        activeColor: MyColors.primary,
        // onChanged: (bool newValue){
        //   setState(() {
        //     value=newValue;
        //   });
        // },
      ),

      MyText(
        title: size,
        size: 12,
        color: MyColors.grey,
      )
    ],
  );
}
