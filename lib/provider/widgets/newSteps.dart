import 'package:flutter/material.dart';
import 'package:my_dress/general/constants/MyColors.dart';

Widget steps({
  BuildContext context,
  Color border1,
  Color border2,
  Color border3,
  Color fill1,
  Color fill2,
  Color fill3,
}){
  return Padding(
    padding: const EdgeInsets.symmetric(vertical: 25, horizontal: 30),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [

        circle(
          fill: fill1,
          border: border1,
        ),
        Container(
          height: 2,
          width: MediaQuery.of(context).size.width * 0.2,
          color: MyColors.primary,
        ),
        circle(
          fill: fill2,
          border: border2,
        ),
        Container(
          height: 2,
          width: MediaQuery.of(context).size.width * 0.2,
          color: MyColors.primary,
        ),
        circle(
          fill: fill3,
          border: border3,
        )

      ],
    ),
  );
}

Widget circle({Color border, Color fill}){
  return Container(
    height: 30,
    width: 30,

    decoration: BoxDecoration(
        color: MyColors.white,
        borderRadius: BorderRadius.circular(50),
        border: Border.all(color: border?? MyColors.white, width: 2)
    ),

    child: Center(
      child: Container(
        height: 20,
        width: 20,

        decoration: BoxDecoration(
          color: fill?? MyColors.primary,
          borderRadius: BorderRadius.circular(20),
        ),
      ),
    ),
  );
}