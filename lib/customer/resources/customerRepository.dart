import 'dart:io';

import 'package:flutter/material.dart';
import 'package:my_dress/customer/models/AuthModel.dart';
import 'package:my_dress/customer/models/CartModel.dart';
import 'package:my_dress/customer/models/CategoryModel.dart';
import 'package:my_dress/customer/models/DeptModel.dart';
import 'package:my_dress/customer/models/FavoriteModel.dart';
import 'package:my_dress/customer/models/LocationsModel.dart';
import 'package:my_dress/customer/models/ProductModel.dart';
import 'package:my_dress/customer/resources/customerMethods.dart';

class CustomerRepository {

  GlobalKey<ScaffoldState> _scaffold;
  CustomerMethods _customerMethods;

  CustomerRepository({@required GlobalKey<ScaffoldState> scaffold}){
    _scaffold = scaffold;
    _customerMethods = CustomerMethods(scaffold: _scaffold);
  }

  Future<bool> login({String phone, String password}) async =>
      _customerMethods.login(phone: phone, password: password);

  Future<bool> setUserLogin(String phone, String password) async =>
      _customerMethods.login(phone: phone, password: password);

  Future <AuthModel> editCustomer({
    String firstName, String lastName, String phone,
    String mail, String password, String birthday,
    String city, String name, String address, File image
  }) async => _customerMethods.editCustomer(
    phone: phone, password: password, image: image,
    name: name, address: address, birthday: birthday,
    city: city, firstName: firstName, lastName: lastName, mail: mail);

  Future<DeptModel> getDept() async => _customerMethods.getDept();

  Future<CategoryModel> getCategory(int deptId) async => _customerMethods.getCategory(deptId);

  Future<ProductModel> getDetails(int id) async => _customerMethods.getDetails(id);

  Future<FavoriteModel> getFavs() async => _customerMethods.getFavs();

  Future<CartModel> getCart() async => _customerMethods.getCart();

  Future<LocationsModel> myLocations() async => _customerMethods.myLocations();

  Future<bool> addLocation(
      String firstName, String lastName, String region, String governate, String city, String code,
      String address, String phone, String mail
      ) async => _customerMethods.addLocation(firstName, lastName, region, governate, city,
      code, address, phone, mail);

  Future<bool> addToFav(int id) async => _customerMethods.addToFav(id);

  Future<bool> addToCart(
      String color, int sizeId, int productId, int amount, int sellType,
      String from, String to ) async => _customerMethods.addToCart(color, sizeId, productId,
      amount, sellType, from, to);

  Future<bool> addComment(int pro_id, String text) async => _customerMethods.addComment(pro_id, text);

}

