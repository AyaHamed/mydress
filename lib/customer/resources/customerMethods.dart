import 'dart:convert';
import 'dart:io';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_dress/customer/customerProvider/customerProvider.dart';
import 'package:my_dress/customer/models/AuthModel.dart';
import 'package:my_dress/customer/models/CartModel.dart';
import 'package:my_dress/customer/models/CategoryModel.dart';
import 'package:my_dress/customer/models/DeptModel.dart';
import 'package:my_dress/customer/models/FavoriteModel.dart';
import 'package:my_dress/customer/models/LocationsModel.dart';
import 'package:my_dress/customer/models/ProductModel.dart';
import 'package:my_dress/customer/screens/Home.dart';
import 'package:my_dress/general/constants/GlobalState.dart';
import 'package:my_dress/general/constants/Http.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CustomerMethods {

  GlobalKey<ScaffoldState> scaffold;
  CustomerMethods({@required this.scaffold});

  FirebaseMessaging _firebaseMessaging = new FirebaseMessaging();
  String _lang = "1";
  String userId = GlobalState.instance.get("user") == null ? "" : GlobalState.instance.get("user")["customer_id"];

  Future<bool> login({String phone, String password}) async {
    String url = "Customer/Login";
    String token = "await _firebaseMessaging.getToken()";
    var body = {
      "phone" : "$phone",
      "password": "$password",
      "token": "$token",
      "lang": "$_lang",
    };
    print(body);
    var data = await Http(scaffold).post(url, body);
    if (data != null){
      SharedPreferences _prefs = await SharedPreferences.getInstance();
      _prefs.setString("user", json.encode(data["data"]));
      _prefs.setString("lang", json.encode(data["data"]["lang"]));
      _prefs.setString("token", data["data"]["token"]);
      GlobalState.instance.set("user", data["data"]);
      GlobalState.instance.set("lang", data["data"]["lang"]);
      GlobalState.instance.set("token", data["data"]["token"]);
      final customer = Provider.of<CustomerProvider>(scaffold.currentContext, listen: false);
      customer.refreshCustomer(AuthModel.fromJson(data["data"]));
      Navigator.of(scaffold.currentContext)
          .pushAndRemoveUntil(CupertinoPageRoute(builder: (_) => Home(0)), (route) => false);
      return true;
    }
    else {
      return false;
    }
  }

  Future <AuthModel> editCustomer({
    String firstName, String lastName, String phone,
    String mail, String password, String birthday,
    String city, String name, String address, File image
  }) async{
    final customer = Provider.of<CustomerProvider>(scaffold.currentContext, listen: false);
    String url = "Customer/EditCustomer";
    var body = {
      "user_id":"${customer.user.customerId}",
      "first" : "$firstName",
      "last" : "$lastName",
      "phone" : "$phone",
      "mail" : "$mail",
      "password" : "$password",
      "birthday" : "$birthday",
      "city" : "$city",
      "name" : "$name" ,
      "address" : "$address" ,
      "image" : image??"" ,
    };
    var data = await Http(scaffold).uploadFile(url, body);
    print("ffff $data");
    if (data != null)
      {
        SharedPreferences _prefs = await SharedPreferences.getInstance();
        _prefs.setString("user", json.encode(data["data"]));
        _prefs.setString("lang", json.encode(data["data"]["lang"]));
        GlobalState.instance.set("user", data["data"]);
        GlobalState.instance.set("lang", data["data"]["lang"]);
        customer.refreshCustomer(AuthModel.fromJson(data["data"]));
        return AuthModel.fromJson(data);
      }
    else {
      return AuthModel();
    }
  }

  Future<DeptModel> getDept() async {
    String url = "Customer/GetDepts";
    var body = {
      "lang" : "$_lang"
    };
    var data = await Http(scaffold).get(url, body);
    if (data != null){
      return DeptModel.fromJson(data);
    }
    else {
      return DeptModel();
    }
  }

  Future<CategoryModel> getCategory(int deptId) async {
    String url = "Customer/GetCats";
    var body = {
      "user_id" : "$userId",
      "dept_id" : "$deptId",
    };
    print (body);
    var data = await Http(scaffold).get(url, body);
    print ("data $data");
    if (data != null) {
      return CategoryModel.fromJson(data);
    }
    else {
      return CategoryModel();
    }
  }

  Future<ProductModel> getDetails(int id) async {
    String url = "Customer/GetProductDetails";
    var body = {
      "pro_id" : "$id"
    };
    print(body);
    var data = await Http(scaffold).get(url, body);
    if (data != null) {
      return ProductModel.fromJson(data);
    }
    else {
      return ProductModel();
    }
  }

  Future<FavoriteModel> getFavs() async {
    String url = "Customer/GetFavPrducts";
    var body = {
      "user_id" : "$userId"
    };
    print(body);
    var data = await Http(scaffold).get(url, body);
    if (data != null) {
      return FavoriteModel.fromJson(data);
    }
    else {
      return FavoriteModel();
    }
  }

  Future<CartModel> getCart() async {
    String url = "Customer/CustomerCard";
    var body = {
      "user_id" : "$userId",
      "lang" : "$_lang"
    };
    var data = await Http(scaffold).get(url, body);
    if (data!=null){
      return CartModel.fromJson(data);
    }
    else {
      return CartModel();
    }
  }

  Future<LocationsModel> myLocations() async {
    String url = "Customer/MyLocations";
    var body = {
      "user_id" : "$userId"
    };
    var data = await Http(scaffold).get(url, body);
    if (data!=null) {
      return LocationsModel.fromJson(data);
    }
    else {
      return LocationsModel();
    }
  }

  Future<bool> addLocation(
      String firstName, String lastName, String region,
      String governate, String city, String code,
      String address, String phone, String mail
      ) async {
    String url = "Customer/AddLocation";
    var body = {
      "user_id" : "$userId",
      "first_name" : "$firstName",
      "last_name" : "$lastName",
      "region" : "$region",
      "city" : "$city",
      "government" : "$governate",
      "postal_code" : "$code",
      "address" : "$address",
      "mail" : "$mail",
      "phone" : "$phone",
    };
    var data = await Http(scaffold).post(url, body);
    if (data!=null) {
      Navigator.of(scaffold.currentContext).pop();
      return true;
    }
    else {
      return false;
    }
  }

  Future<bool> addToFav(int id) async {
    String url = "Customer/AddToFavourites";
    var body = {
      "user_id" : "$userId",
      "id" : "$id"
    };
    print(body);
    var data = await Http(scaffold).post(url, body);
    if (data!=null) {
      return true;
    }
    else {
      return false;
    }
  }

  Future<bool> addToCart(
      String color, int sizeId, int productId, int amount, int sellType,
      String from, String to ) async {
    String url = "Customer/AddToCard";
    var body = {
      "user_id" : "$userId",
      "color" : "$color",
      "size_id" : "$sizeId",
      "product_id" : "$productId",
      "amount" : "$amount",
      "sellType" : "$sellType",
      "from" : "$from",
      "to" : "$to"
    };
    var data = await Http(scaffold).post(url, body);
    if (data != null) {
      Navigator.of(scaffold.currentContext).pop();
      return true;
    }
    else {
      return false;
    }
  }

  Future<bool> addComment(int pro_id, String text) async {
    String url = "Customer/AddComment";
    var body = {
      "user_id" : "$userId",
      "pro_id" : "$pro_id",
      "text" : "$text"
    };
    var data = await Http(scaffold).post(url, body);
    if (data != null) {
      return true;
    }
    else {
      return false;
    }
  }

}