import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_dress/customer/models/CartModel.dart';
import 'package:my_dress/customer/resources/customerRepository.dart';
import 'package:my_dress/customer/screens/PaymentMethods.dart';
import 'package:my_dress/customer/widgets/bill.dart';
import 'package:my_dress/customer/widgets/promoCode.dart';
import 'package:my_dress/general/constants/CachedImage.dart';
import 'package:my_dress/general/constants/DefaultAppBar.dart';
import 'package:my_dress/general/constants/DefaultButton.dart';
import 'package:my_dress/general/constants/LoadingDialog.dart';
import 'package:my_dress/general/constants/MyColors.dart';
import 'package:my_dress/general/constants/MyText.dart';

class ReviewOrder extends StatefulWidget {
  @override
  _ReviewOrderState createState() => _ReviewOrderState();
}

class _ReviewOrderState extends State<ReviewOrder> {

  GlobalKey<ScaffoldState> scaffold = GlobalKey<ScaffoldState>();
  CustomerRepository _repository;
  CartModel _cartModel = CartModel();

  @override
  void initState() {
    setState(() {
      _repository = CustomerRepository(scaffold: scaffold);
    });
    super.initState();
  }

  Widget orderItem(String title, String detail){
    return Row(
      children: [
        MyText(title: title, color: MyColors.primary),
        Text("  :  "),
        MyText(title: detail, color: MyColors.grey),
      ],
    );
  }

  Widget order({String title,String image, String amount, int price}){
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 130,

      decoration: BoxDecoration(
        color: MyColors.white,
        border: Border.all(color: MyColors.greyWhite),
      ),

      padding: EdgeInsets.symmetric(horizontal: 15),

      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,

        children: [

          Row(
            children: [
              Container(
                height: 100,
                width: 80,
                child: CachedImage(url: image, fit: BoxFit.fill,)
                //Image.asset("images/model.PNG", fit: BoxFit.fill,),
              ),

              SizedBox(width: 15,),

              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  MyText(title: title,color: MyColors.primary),
                  SizedBox(height: 15,),
                  orderItem("السعر", "$price رس "),
                  orderItem("الكمية", amount),

                ],
              ),

            ],
          ),


        ],
      ),

    );
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffold,
      appBar: DefaultAppBar(con: context, title: "إتمام الطلب"),

      body: FutureBuilder<CartModel>(
        future: _repository.getCart(),
        builder: (context, snapshot){
          if (snapshot.hasData){
            return Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
              child: ListView(
                shrinkWrap: true,
                children: [
                  ListView.builder(
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: snapshot.data.products.length,
                    itemBuilder: (context, index){
                      return order(
                        title: snapshot.data.products[index].name,
                        image: snapshot.data.products[index].image,
                        amount: snapshot.data.products[index].amount.toString(),
                        price: snapshot.data.products[index].price
                      );
                    },
                  ),

                  SizedBox(height: 20,),

                  promoCode(context),

                  SizedBox(height: 10,),

                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: 70,

                    decoration: BoxDecoration(
                      color: MyColors.white,
                      border: Border.all(color: MyColors.greyWhite),
                      borderRadius: BorderRadius.circular(2),
                    ),

                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,

                      children: [

                        MyText(title: "الشحن إلى", color: MyColors.primary,),

                        SizedBox(height: 20,),

                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 15),
                          child: Container(
                            height: 30,
                            width: MediaQuery.of(context).size.width * 0.6,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(20),
                                border: Border.all(color: MyColors.grey)
                            ),
                            child: TextFormField(
                                decoration: InputDecoration(
                                    enabledBorder: InputBorder.none,
                                    focusedBorder: InputBorder.none,
                                    suffixIcon: Icon(Icons.location_on_outlined, color: MyColors.grey, size: 20,)
                                )
                            ),
                          ),
                        ),

                      ],
                    ),

                  ),

                  SizedBox(height: 20,),

                  bill(
                    quantity: snapshot.data.amount.toString(),
                    delivery: snapshot.data.delivery.toString(),
                    sale: snapshot.data.sale.toString(),
                    total: snapshot.data.total.toString()
                  ),

                  SizedBox(height: 20,),

                  DefaultButton(
                    title: "تأكيد ودفع",
                    color: MyColors.primary,
                    onTap: () {Navigator.of(context)
                        .push(CupertinoPageRoute(builder: (_) => PaymentMethods()));},)

                ],
              ),
            );
          }
          else {
            return Center(
              child: LoadingDialog(scaffold).showLoadingView(),
            );
          }
        },
      )

    );
  }
}
