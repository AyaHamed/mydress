import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_dress/customer/models/DeptModel.dart';
import 'package:my_dress/customer/resources/customerRepository.dart';
import 'package:my_dress/customer/widgets/DefaultDrawer.dart';
import 'package:my_dress/customer/widgets/DressCategory.dart';
import 'package:my_dress/general/constants/DefaultAppBar.dart';
import 'package:my_dress/general/constants/LoadingDialog.dart';
import 'package:my_dress/general/constants/MyColors.dart';
import 'package:my_dress/general/utilities/Delay.dart';

import 'Engagement.dart';
import 'Tailoring.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  GlobalKey<ScaffoldState> scaffold=GlobalKey<ScaffoldState>();
  CustomerRepository _repository;

  @override
  void initState() {
    setState(() {
      _repository = CustomerRepository(scaffold: scaffold);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffold,

      appBar: DefaultAppBar(
        title: "الرئيسية",
        con: context,
        leading: IconButton(
        onPressed: () => scaffold.currentState.openDrawer(),
          icon: Icon(
          Icons.segment,
          color: MyColors.primary,
          size: 30,),),),

      drawer: defaultDrawer(context),

      body: FutureBuilder<DeptModel>(
        future: Delay<DeptModel>().invoke(function: _repository.getDept),
        builder: (context, snapshot){
          if (snapshot.hasData){
            return Visibility(
              child: ListView.builder(
                itemCount: snapshot.data.data.length,
                itemBuilder: (context, index){
                  return dressCategory(
                    context: context,
                    title: "${snapshot.data.data[index].name}",
                    image: "${snapshot.data.data[index].image}",
                    id: snapshot.data.data[index].id,
                    page: Engagement(id: snapshot.data.data[index].id,),
                  );
                },
              ),
            );
          }
          else {
            return Center(
              child: LoadingDialog(scaffold).showLoadingView(),
            );
          }
        },
      ),

      // body: ListView(
      //   children: [
      //     SizedBox(height: 10,),
      //
      //     dressCategory(context: context, image: "images/wedding.PNG", page: Engagement(),),
      //     dressCategory(context: context, image: "images/engagement.PNG", page: Engagement()),
      //     dressCategory(context: context, image: "images/monasbat.PNG", page: Engagement()),
      //     dressCategory(context: context, image: "images/sahra.PNG", page: Engagement()),
      //     dressCategory(context: context, image: "images/special.PNG", page: Engagement()),
      //     dressCategory(context: context, image: "images/tafseel.PNG", page: Tailoring()),
      //   ],
      // ),
    );
  }
}
