import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_dress/general/constants/DefaultAppBar.dart';
import 'package:my_dress/general/constants/MyColors.dart';
import 'package:my_dress/general/constants/MyText.dart';
import 'Country.dart';
import 'Help.dart';
import 'Language.dart';

class Settings extends StatefulWidget {
  @override
  _SettingsState createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {

  Widget item(String title, Widget page, IconData icon){
    return InkWell(
      onTap: (){ Navigator.push(context,
          CupertinoPageRoute(builder: (context) => page));},

      child: Container(

        height: 70,
        width: MediaQuery.of(context).size.width,

        decoration: BoxDecoration(
            color: MyColors.white,
            border: Border.all(color: MyColors.greyWhite)
        ),

        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,

            children: [

              Icon(icon, color: MyColors.primary, size: 22,),
              SizedBox(width: 10,),
              MyText(title: title,
                size: 15,
                color: MyColors.primary,
              ),
              Expanded(child: SizedBox()),
              Icon(Icons.arrow_left_sharp, color: MyColors.primary, size: 22,),
            ],
          ),
        ),

      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(

      appBar: DefaultAppBar(con: context, title: "الاعدادات"),

      body: Container(
        padding: EdgeInsets.all(10),
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
            border: Border.all(color: MyColors.greyWhite),
            borderRadius: BorderRadius.circular(5)
        ),

        child: Column(
          children: [
            item("اللغة", Language(), Icons.language),
            item("الدولة", Country(), Icons.language),
            item("المساعدة", Help(), Icons.help_rounded),
          ],
        ),
      ),

    );
  }
}
