import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_dress/customer/models/CartModel.dart';
import 'package:my_dress/customer/resources/customerRepository.dart';
import 'package:my_dress/customer/screens/Home.dart';
import 'package:my_dress/customer/widgets/bill.dart';
import 'package:my_dress/customer/widgets/dressColor.dart';
import 'package:my_dress/customer/widgets/promoCode.dart';
import 'package:my_dress/general/constants/CachedImage.dart';
import 'package:my_dress/general/constants/DefaultAppBar.dart';
import 'package:my_dress/general/constants/DefaultButton.dart';
import 'package:my_dress/general/constants/LoadingDialog.dart';
import 'package:my_dress/general/constants/MyColors.dart';
import 'package:my_dress/general/constants/MyText.dart';

import 'ReviewOrder.dart';

class Cart extends StatefulWidget {
  final id;

  const Cart({Key key, this.id}) : super(key: key);

  @override
  _CartState createState() => _CartState();
}

class _CartState extends State<Cart> {

  GlobalKey<ScaffoldState> scaffold=GlobalKey<ScaffoldState>();
  CustomerRepository _repository;
  CartModel _cartModel = CartModel();

  @override
  void initState() {
    setState(() {
      _repository = CustomerRepository(scaffold: scaffold);
    });
    super.initState();
  }

  _validateColor(String color) {
    if (color.startsWith("#")) {
      return int.parse(color.replaceAll("#", "0xff"));
    } else {
      return int.parse("0xffffffff");
    }
  }

  // int _qty = 1 ;

  void _increase(index) {
    setState(() {
      _cartModel.products[index].amount ++;
    });
  }

  void _decrease(index) {
    setState(() {
      if (_cartModel.products[index].amount != 0) _cartModel.products[index].amount --;
    });
  }

  Widget button(String sign, onTap){
    return InkWell(
      onTap:onTap,
      child:  Container(
        height: 30, width: 30,
        decoration: BoxDecoration(
            border: Border.all(color: MyColors.grey)
        ),
        child: Center(
          child: Text(sign, style: TextStyle(color: MyColors.grey, fontSize: 25),),
        ),
      ),
    );
  }

  Widget countButton(index){
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
      child: Row(
        children: [
          button("+", (){
            _increase(index);
          }),
          Container(
            height: 30, width: 30,
            decoration: BoxDecoration(
                border: Border.all(color: MyColors.grey),
                color: MyColors.greyWhite
            ),
            child: Center(
              child: MyText(title: '${_cartModel.products[index].amount}', color: MyColors.primary, size: 15),
            ),
          ),
          button("-", (){
            _decrease(index);
          }),
        ],
      ),
    );
  }
  
  Widget orderItem({String title, String detail, bool color = false , Color colorName}){
    return Row(
      children: [
        MyText(title: title, color: MyColors.primary),
        Text("  :  "),
        color==false? MyText(title: detail, color: MyColors.grey) : dressColor(colorName)
      ],
    );
  }

  Widget order({String image, String title, String price, String size, Color color, int index}){
    return Directionality(
      textDirection: TextDirection.rtl,

      child: Container(
        width: MediaQuery.of(context).size.width,

        decoration: BoxDecoration(
          color: MyColors.white,
          border: Border.all(color: MyColors.greyWhite),
          borderRadius: BorderRadius.circular(5),
        ),

        padding: EdgeInsets.symmetric(horizontal: 15, vertical: 5),

        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,

          children: [

            Row(
              children: [
                Container(
                  height: 125,
                  width: 95,
                  //child: Image.asset("images/model.PNG", fit: BoxFit.fill,),
                  child: CachedImage(
                    url: image,
                    fit: BoxFit.fill,
                  ),
                ),

                SizedBox(width: 10,),

                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    MyText(title: title, color: MyColors.primary),
                    SizedBox(height: 5,),
                    orderItem(title: "السعر", detail: "$price رس "),
                    orderItem(title: "اللون", color: true, colorName: color),
                    Row(
                      children: [
                        orderItem(title: "المقاس", detail: size),
                        SizedBox(width: 10,),
                        countButton(index),
                        InkWell(
                          // onTap: ,
                            child: Icon(
                              Icons.delete_outline,
                              color: MyColors.primary, size: 25,))
                      ],
                    ),

                  ],
                ),

              ],
            ),


          ],
        ),

      ),
    );
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffold,

      appBar: DefaultAppBar(con: context, title: "عربة التسوق",
        leading: Container(),
        // leading: IconButton(
        //     onPressed: () {Navigator.of(context)
        //         .pushReplacement(CupertinoPageRoute(builder: (_) => Home(0)));},
        //     icon: Icon(
        //       Icons.arrow_back_ios,
        //       color: MyColors.primary,
        //       size: 20,
        //     )),
      ),

      body: FutureBuilder<CartModel>(
        future:_cartModel.products==null? _repository.getCart():null,
        builder: (context, snapshot){
          if (snapshot.hasData){
            _cartModel=snapshot.data;
            if (snapshot.data.products.length != 0){
              return Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 20),
                child: ListView(
                  shrinkWrap: true,
                  children: [

                    ListView.builder(
                      physics: NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: snapshot.data.products.length,
                      itemBuilder: (context, index){
                        return order(
                          title: snapshot.data.products[index].name,
                          price: "${snapshot.data.products[index].price}",
                          image: snapshot.data.products[index].image,
                          color: Color(_validateColor(snapshot.data.products[index].color)),
                          size: snapshot.data.products[index].size,
                          index: index
                        );
                      },
                    ),

                    SizedBox(height: 30,),

                    promoCode(context),

                    SizedBox(height: 20,),

                    bill(
                      quantity: "${snapshot.data.amount}",
                      delivery: "${snapshot.data.delivery}",
                      sale: "${snapshot.data.sale}",
                      total: "${snapshot.data.total}",
                    ),

                    DefaultButton(
                      title: "إتمام الشراء",
                      onTap: (){ Navigator.push(context,
                          CupertinoPageRoute(builder: (_) => ReviewOrder()));},
                    ),

                  ],
                ),
              );
            }
            else {
              return Center(
                child: MyText(
                  title: "لا يوجد منتجات",
                  color: MyColors.primary,
                  fontWeight: FontWeight.bold,
                  size: 17,
                ),
              );
            }
          }
          else {
            return Center(
              child: LoadingDialog(scaffold).showLoadingView(),
            );
          }
        },
      )
    );
  }
}

