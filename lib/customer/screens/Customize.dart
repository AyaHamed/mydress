import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_dress/customer/screens/Customize_2.dart';
import 'package:my_dress/general/constants/DefaultAppBar.dart';
import 'package:my_dress/general/constants/DefaultButton.dart';
import 'package:my_dress/general/constants/LabelTextField.dart';
import 'package:my_dress/general/constants/MyColors.dart';
import 'package:my_dress/provider/widgets/newSteps.dart';

class Customize extends StatefulWidget {
  @override
  _CustomizeState createState() => _CustomizeState();
}

class _CustomizeState extends State<Customize> {
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar(con: context, title: "تفصيل فستان"),
      body: ListView(
        children: [

          steps(
            context: context,
            border1: MyColors.primary,
            fill1: MyColors.gold,
          ),

          Padding(
            padding: const EdgeInsets.symmetric(vertical: 30),
            child: Image.asset("images/body@3x.png", height: 200,),
          ),

          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 25),
            child: Container(
              width: MediaQuery.of(context).size.width,

              decoration: BoxDecoration(
                //color: Colors.green,
                borderRadius: BorderRadius.circular(8),
                border: Border.all(color: MyColors.greyWhite),
              ),

              padding: EdgeInsets.symmetric(horizontal: 15, vertical: 15),

              child: Column(
                //crossAxisAlignment: CrossAxisAlignment.start,
                children: [

                  LabelTextField(
                    label: "وحدة القياس",
                    isPassword: false,
                  ),
                  LabelTextField(
                    label: "الوزن",
                    isPassword: false,
                  ),
                  LabelTextField(
                    label: "الطول",
                    isPassword: false,
                  ),
                  LabelTextField(
                    label: "نوع القماش",
                    isPassword: false,
                  ),
                  LabelTextField(
                    label: "لون القماش",
                    isPassword: false,
                  ),
                  LabelTextField(
                    label: "    طول الفستان من الكتف للارض",
                    isPassword: false,
                  ),

                  // title("وحدة القياس"),
                  // IconTextFiled(borderColor: MyColors.grey, label: "سنتيمتر cm ",),
                  //
                  // title("الوزن"),
                  // IconTextFiled(borderColor: MyColors.grey,),
                  //
                  // title("الطول"),
                  // IconTextFiled(borderColor: MyColors.grey,),
                  //
                  // title("نوع القماش"),
                  // IconTextFiled(borderColor: MyColors.grey,),
                  //
                  // title("لون القماش"),
                  // IconTextFiled(borderColor: MyColors.grey,),
                  //
                  // title("طول الفستان من الكتف للارض    "),
                  // IconTextFiled(borderColor: MyColors.grey,),

                ],
              ),

            ),
          ),

          Padding(
            padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 15),
            child: DefaultButton(
              color: MyColors.primary,
              title: "التالي",
              onTap: () {Navigator.of(context).push(CupertinoPageRoute(builder: (_) => Customize2()));},
            ),
          ),

        ],
      ),
    );
  }
}
