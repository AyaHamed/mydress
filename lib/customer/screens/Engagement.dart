import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:my_dress/customer/models/CategoryModel.dart';
import 'package:my_dress/customer/resources/customerRepository.dart';
import 'package:my_dress/customer/screens/Home.dart';
import 'package:my_dress/customer/widgets/ItemDress.dart';
import 'package:my_dress/general/constants/LoadingDialog.dart';
import 'package:my_dress/general/constants/MyColors.dart';
import 'package:my_dress/general/constants/MyText.dart';
import 'package:my_dress/general/utilities/Delay.dart';

import 'DressDetails.dart';

class Engagement extends StatefulWidget {
  final int id;

  const Engagement({Key key, this.id,}) : super(key: key);

  @override
  _EngagementState createState() => _EngagementState();
}

class _EngagementState extends State<Engagement> {
  GlobalKey<ScaffoldState> scaffold=GlobalKey<ScaffoldState>();
  CustomerRepository _repository;
  List<Product> _products;
  CategoryModel _categoryModel=CategoryModel();

  @override
  void initState() {
    setState(() {
      _repository = CustomerRepository(scaffold: scaffold);
    });
    super.initState();
  }

  _selectProducts(List<Product> pros){
    _products=pros;
    setState(() {

    });
  }

  _addToFav(id, int index) async {
    print("object");
    bool loved = await _repository.addToFav(id);
    if (loved == true) {
      setState(() {
        _products[index].loved=!_products[index].loved;
      });
    }
  }

  void _onPressed(){
    showModalBottomSheet(context: context, builder: (context) {

      return Container(
        height: MediaQuery.of(context).size.height * 0.3,
        child: Column(children: [
          bottomSheetItem("ترتيب حسب", color: MyColors.primary),
          bottomSheetItem("ترتيب حسب"),
          bottomSheetItem("ترتيب حسب"),
          bottomSheetItem("ترتيب حسب"),
          bottomSheetItem("ترتيب حسب"),
          bottomSheetItem("ترتيب حسب"),
        ],),
      );
    });
  }

  Widget bottomSheetItem(String title, {Color color}){
    return Container(
      height: 40,
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
          color: MyColors.white,
          border: Border.all(color: MyColors.greyWhite)
      ),
      child: Center(
        child: MyText(
          title: title,
          color: color?? MyColors.black,
          size: 15,
          align: TextAlign.center,
        ),
      ),
    );
  }

  Widget filter(String title, IconData icon){
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 30),
      child: Container(
        child: Row(
          children: [
            MyText(
              title: title,
              color: MyColors.primary,
              size: 13,
              fontWeight: FontWeight.bold,
            ),
            SizedBox(width: 10,),
            Icon(icon, color: MyColors.grey,)
          ],
        ),
      ),
    );
  }

  Widget filterBar(){
    return Container(
      height: 35, width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
          color: MyColors.white,
          border: Border.all(color: MyColors.grey, width: 1)
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          InkWell( onTap: () {_onPressed();},
              child: filter("ترتيب حسب", Icons.compare_arrows_sharp)),
          VerticalDivider(color: MyColors.primary),
          InkWell( onTap: () {_onPressed();},
              child: filter("الفئة العمرية", Icons.filter_alt_outlined)),

        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    print("hhhhhhhhhhhhhh ${_categoryModel==null}");
    return Scaffold(
      key: scaffold,
      body: FutureBuilder<CategoryModel>(
        future:_categoryModel.data==null? _repository.getCategory(widget.id):null ,
        builder: (context, snapshot){
          if (snapshot.hasData) {
            _products??=snapshot.data.data[0].products;
            _categoryModel=snapshot.data;
            return Column(
              children: [
                PreferredSize(
                  preferredSize: Size.fromHeight(140),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      AppBar(
                        backgroundColor: MyColors.white,
                        centerTitle: true,
                        title: MyText(
                          title: "فساتين زفاف",
                          color: MyColors.primary,
                          size: 17,
                          fontWeight: FontWeight.bold,
                        ),
                        // title: Text(
                        //   "فساتين زفاف",
                        //   style: TextStyle(color: MyColors.primary, fontSize: 20),
                        // ),
                        leading: IconButton(
                            onPressed: (){ Navigator.pop(context);},
                            icon: Icon(
                              Icons.arrow_back_ios,
                              color: MyColors.primary,
                              size: 20,
                            )),
                      ),

                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 5),
                        child: Container(
                          height: 35,
                          color: MyColors.greyWhite,
                          child: ListView(
                            scrollDirection: Axis.horizontal,
                            children: List.generate(snapshot.data.data.length, (index){
                              return InkWell(
                                onTap: ()=>_selectProducts(snapshot.data.data[index].products),
                                child: Padding(
                                  padding: const EdgeInsets.all(2),
                                  child: Row(
                                    children: [
                                      Container(
                                        width: 100,
                                          alignment: Alignment.center,
                                          child: MyText(
                                            align: TextAlign.center,
                                            title: "${snapshot.data.data[index].name}",
                                            color: MyColors.primary,
                                            fontWeight: FontWeight.bold,
                                            )),
                                      VerticalDivider(color: MyColors.white, width: 4,),
                                    ],
                                  ),
                                ),
                              );
                            }),
                          ),
                        ),
                      ),
                      // filterBar(),
                    ],
                  ),
                ),
                Flexible(
                    child: _products.length != 0 ? SingleChildScrollView(
                      child: Wrap(
                          spacing: 15,
                          alignment: WrapAlignment.start,
                          children: List.generate(_products.length, (position) => itemDress(
                            context: context,
                            image: "${_products[position].image}",
                            title: "${_products[position].name}",
                            price: "${_products[position].price}",
                            loved: _products[position].loved,
                            onTap: ()=> _addToFav(_products[position].id,position),
                            //details: "${_products[position].desc}",
                            page: DressDetails(id: _products[position].id, title: "${_products[position].name}",),)
                          )
                      )
                      ) : Center(
                      child: MyText(
                        title: "لا يوجد منتجات",
                        color: MyColors.primary,
                        size: 17, fontWeight: FontWeight.bold,
                      ),
                    )
                ),
              ],
            );
          }
          else {
            return Center(
              child: LoadingDialog(scaffold).showLoadingView(),
            );
          }
        },
      ),
    );
  }
}
