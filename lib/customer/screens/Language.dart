import 'package:flutter/material.dart';
import 'package:my_dress/general/constants/DefaultAppBar.dart';
import 'package:my_dress/general/constants/MyColors.dart';
import 'package:my_dress/general/constants/MyText.dart';

class Language extends StatefulWidget {
  @override
  _LanguageState createState() => _LanguageState();
}

class _LanguageState extends State<Language> {

  int group = 0;

  Widget lang(String title, int value){
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: 60,
        padding: EdgeInsets.only(right: 10),
        decoration: BoxDecoration(
          color: MyColors.white,
          border: Border.all(color: MyColors.greyWhite),
        ),

        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [

            MyText(title: title, color: MyColors.primary),

            Container(
              width: MediaQuery.of(context).size.width * 0.4,
              child: ListTile(
                leading: Radio(
                  value: value,
                  groupValue: group,
                  onChanged: (value){
                    group = value;
                    setState(() {
                    });
                    print(group);
                  },),
              ),
            ),


          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar(con: context, title: "اللغة"),

      body: Column(
        children: [

          SizedBox(height: 30,),

          lang("اللغة العربية", 0,),
          lang("اللغة الانجليزية", 1,)

        ],
      ),

    );
  }
}
