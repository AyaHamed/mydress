import 'package:flutter/cupertino.dart';
import 'package:my_dress/general/constants/DefaultButton.dart';
import 'package:my_dress/general/constants/MyText.dart';
import 'Customize.dart';
import 'package:flutter/material.dart';
import 'package:my_dress/general/constants/DefaultAppBar.dart';
import 'package:my_dress/general/constants/MyColors.dart';

class Tailoring extends StatefulWidget {
  @override
  _TailoringState createState() => _TailoringState();
}

class _TailoringState extends State<Tailoring> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar(con: context, title: "تفصيل فساتين"),

      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height * 0.7,

          decoration: BoxDecoration(
              color: MyColors.white,
              border: Border.all(color:  MyColors.greyWhite),
              borderRadius: BorderRadius.circular(5)
          ),

          padding: EdgeInsets.symmetric(horizontal: 10, vertical: 30),

          child: Column(
            children: [

              MyText(title: "طلب تفصيل فستان خاص", color: MyColors.primary),
              SizedBox(height: 30,),
              Image.asset("images/img_two@3x.png", height: 270,),
              SizedBox(height: 30,),
              Container(
                height: 80, width: 270,
                child: MyText(title: "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", color: MyColors.grey,)
              ),
              SizedBox(height: 10,),

              DefaultButton(
                title: "إضافة مقاس جديد",
                onTap: () {Navigator.of(context).push(CupertinoPageRoute(builder: (_) => Customize()));},
              ),

            ],),

        ),
      ),

    );
  }
}
