import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_dress/customer/resources/customerRepository.dart';
import 'package:my_dress/customer/screens/Home.dart';
import 'package:my_dress/general/constants/DefaultAppBar.dart';
import 'package:my_dress/general/constants/DefaultButton.dart';
import 'package:my_dress/general/constants/IconTextFiled.dart';
import 'package:my_dress/general/constants/MyColors.dart';
import 'package:my_dress/general/utilities/Validator.dart';

class NewLocation extends StatefulWidget {
  @override
  _NewLocationState createState() => _NewLocationState();
}

class _NewLocationState extends State<NewLocation> {
  GlobalKey<ScaffoldState> _scaffold = new GlobalKey<ScaffoldState>();
  GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  CustomerRepository _repository;

  TextEditingController _firstName = new TextEditingController();
  TextEditingController _lastName = new TextEditingController();
  TextEditingController _region = new TextEditingController();
  TextEditingController _city = new TextEditingController();
  TextEditingController _address = new TextEditingController();
  TextEditingController _phone = new TextEditingController();
  TextEditingController _mail = new TextEditingController();
  TextEditingController _code = new TextEditingController();
  TextEditingController _governate = new TextEditingController();

  @override
  void initState() {
    setState(() {
      _repository = CustomerRepository(scaffold: _scaffold);
    });
    super.initState();
  }

  _addNewLocation() async {
    if (_formKey.currentState.validate()) {
      await _repository.addLocation(
          _firstName.text,
          _lastName.text,
          _region.text,
          _governate.text,
          _city.text,
          _code.text,
          _address.text,
          _phone.text,
          _mail.text
      );}
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffold,
      appBar: DefaultAppBar(con: context, title: "موقع الشحن"),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
        child: Container(
          width: MediaQuery.of(context).size.width,

          decoration: BoxDecoration(
            color: MyColors.white,
            border: Border.all(color: MyColors.greyWhite),
            borderRadius: BorderRadius.circular(10)
          ),

          padding: EdgeInsets.symmetric(vertical: 35,),

          child: Form(
            key: _formKey,

            child: ListView(
              children: [
                IconTextFiled(
                  label: "الاسم الأول",
                  borderColor: MyColors.greyWhite,
                  controller: _firstName,
                  validate: (value)=>Validator().validateEmpty(value: value),
                ),

                IconTextFiled(
                  label: "اسم العائلة",
                  borderColor: MyColors.greyWhite,
                  controller: _lastName,
                  validate: (value)=>Validator().validateEmpty(value: value),
                ),

                IconTextFiled(
                  label: "البلد/المنطقة",
                  borderColor: MyColors.greyWhite,
                  controller: _region,
                  validate: (value)=>Validator().validateEmpty(value: value),
                ),

                IconTextFiled(
                  label: "المحافظة",
                  borderColor: MyColors.greyWhite,
                  controller: _governate,
                  validate: (value)=>Validator().validateEmpty(value: value),
                ),

                IconTextFiled(
                  label: "المدينة",
                  borderColor: MyColors.greyWhite,
                  controller: _city,
                  validate: (value)=>Validator().validateEmpty(value: value),
                ),

                IconTextFiled(
                  label: "كود المنطقة",
                  borderColor: MyColors.greyWhite,
                  controller: _code,
                  validate: (value)=>Validator().validateEmpty(value: value),
                ),

                IconTextFiled(
                  label: "عنوان المنزل",
                  borderColor: MyColors.greyWhite,
                  controller: _address,
                  validate: (value)=>Validator().validateEmpty(value: value),
                ),

                IconTextFiled(
                  label: "رقم الجوال",
                  borderColor: MyColors.greyWhite,
                  controller: _phone,
                  validate: (value)=>Validator().validateEmpty(value: value),
                ),

                IconTextFiled(
                  label: "البريد الالكتروني",
                  borderColor: MyColors.greyWhite,
                  controller: _mail,
                  validate: (value)=>Validator().validateEmpty(value: value),
                ),

                SizedBox(height: 30,),

                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15),
                  child: DefaultButton(
                    title: "إضافة العنوان",
                    onTap: () => _addNewLocation(),
                  ),

                )

              ],
            ),
          ),
        ),
      ),
    );
  }
}
