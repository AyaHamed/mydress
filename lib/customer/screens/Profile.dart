import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:my_dress/customer/customerProvider/customerProvider.dart';
import 'package:my_dress/customer/resources/customerRepository.dart';
import 'package:my_dress/general/constants/CachedImage.dart';
import 'package:my_dress/general/constants/DefaultAppBar.dart';
import 'package:my_dress/general/constants/DefaultButton.dart';
import 'package:my_dress/general/constants/GlobalState.dart';
import 'package:my_dress/general/constants/IconTextFiled.dart';
import 'package:my_dress/general/constants/MyColors.dart';
import 'package:my_dress/general/utilities/Validator.dart';
import 'package:provider/provider.dart';

class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  GlobalKey<ScaffoldState> _scaffold = new GlobalKey<ScaffoldState>();
  GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  CustomerRepository _repository;
  File image;
  TextEditingController _firstName = new TextEditingController();
  TextEditingController _lastName = new TextEditingController();
  TextEditingController _birth = new TextEditingController();
  TextEditingController _city = new TextEditingController();
  TextEditingController _address = new TextEditingController();
  TextEditingController _phone = new TextEditingController();
  TextEditingController _mail = new TextEditingController();
  TextEditingController _password = new TextEditingController();

  Widget info(double top, String text, Color color){
    return Positioned(
      top: MediaQuery.of(context).size.height * top,
      child: Container(
        height: 30,
        width: MediaQuery.of(context).size.width,
        child: Text(
          text?? "",
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 18,
            fontWeight: FontWeight.bold,
            color: color,
          ),
        ),
      ),
    );
  }

  _getData(){
    final customer = Provider.of<CustomerProvider>(context, listen: false);
    setState(() {
      _phone.text = customer.user.phone;
      _mail.text = customer.user.email;
      _address.text = customer.user.address;
      _city.text = customer.user.city;
      _birth.text = customer.user.birthday;
      _firstName.text = customer.user.first;
      _lastName.text = customer.user.last;
    });
  }

  _editCustomer() async {
    if (_formKey.currentState.validate()) {
      await _repository.editCustomer(
      mail: _mail.text,
      lastName: _lastName.text,
      firstName: _firstName.text,
      city: _city.text,
      birthday: _birth.text,
      address: _address.text,
      name: "${_firstName.text?? ""} ${_lastName.text?? ""}" ,
      image: image,
      password: _password.text,
      phone: _phone.text
    );}
  }

  @override
  void initState() {
    _getData();
    super.initState();
    setState(() {
      _repository = CustomerRepository(scaffold: _scaffold);
    });
  }
  

  Future getImage() async {
    var image = await ImagePicker.pickImage(source: ImageSource.camera);

    setState(() {
      image = image;
    });
  }

  Future getPhoto() async {
    var image = await ImagePicker().getImage(source: ImageSource.gallery);
    
    setState(() {
      this.image = File(image.path);
    });
    Navigator.pop(context);
  }

  void _onPressed(){
    showModalBottomSheet(context: context, builder: (context) {
      return Container(
        height: 130,
        child: Column(children: <Widget>[
          SizedBox(height: 10,),
          InkWell( onTap: (){getImage();},
            child: Container(height: 40, width: 150, decoration: BoxDecoration(color: MyColors.greyWhite),
              child: Center(child:Text('Take a picture')),
            ),),
          SizedBox(height: 20,),
          InkWell( onTap: (){getPhoto();},
            child: Container(height: 40, width: 150, decoration: BoxDecoration(color: MyColors.greyWhite),
              child: Center(child: Text('Choose a picture')),
            ),),
        ],),
      );
    });
  }

  Widget picture(String photo){
    return Container(
      height: 130,
      width: 130,
      alignment: Alignment.bottomLeft,
      child: InkWell(
        child: Container(
          height: 30,
          width: 30,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            color: MyColors.grey,
          ),
          child: Icon(
            Icons.edit,
            size: 22,
            color: MyColors.white,
          ),
        ),
        onTap: ()=>_onPressed()
        // onTap: () {
        //   setState(() {
        //     _onPressed();
        //   });
        // },
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(70),
        color: MyColors.grey,
        image: DecorationImage(
          image: image == null?
           NetworkImage(photo,)
              :FileImage(image,), fit: BoxFit.fill,),
      ),
    );
  }


  @override
  Widget build(BuildContext context) {
    final customer = Provider.of<CustomerProvider>(context, listen: false);
    return Scaffold(
      key: _scaffold,
      backgroundColor: Colors.grey[100],
      appBar: DefaultAppBar(con: context, title: "الملف الشخصي",
        leading: Container(),
        // leading: IconButton(
        //     onPressed: () {Navigator.of(context).push(CupertinoPageRoute(builder: (_) => Home(0)));},
        //     icon: Icon(
        //       Icons.arrow_back_ios,
        //       color: MyColors.primary,
        //       size: 20,
        //     )),
      ),

      body: ListView(
        children: [
          Stack(
            children: [
              Image.asset(
                "images/cover@2x.png",
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height * 0.26,
              ),

              Positioned(
                top: MediaQuery.of(context).size.height * 0.018,
                left: MediaQuery.of(context).size.width * 0.36,
                // child: Container(
                //   height: 130, width: 130,
                //   decoration: BoxDecoration(
                //     color: MyColors.greyWhite,
                //     borderRadius: BorderRadius.circular(90)
                //   ),
                  child: picture(customer.user.avatar),
                  //CachedImage(url: customer.user.avatar, fit: BoxFit.fill,),
                // ),
              ),

              // Positioned(
              //   top: MediaQuery.of(context).size.height * 0.145,
              //   left: MediaQuery.of(context).size.width * 0.38,
              //   child: Container(
              //     height: 30, width: 30,
              //     decoration: BoxDecoration(
              //         color: Colors.grey[700],
              //         borderRadius: BorderRadius.circular(90)
              //     ),
              //     child: Icon(Icons.edit, color: MyColors.white,),
              //   ),
              // ),

              info(0.18, customer.user.name, MyColors.primary),
              info(0.21, customer.user.email, MyColors.grey),
            ],
          ),

          Form(
            key: _formKey,
            child: Column(
              children: [
                IconTextFiled(
                  label: "الاسم الاول",
                  controller: _firstName,
                  validate: (value)=>Validator().validateEmpty(value: value),
                ),

                IconTextFiled(
                  label: "اسم العائلة",
                  controller: _lastName,
                  validate: (value)=>Validator().validateEmpty(value: value),
                ),

                IconTextFiled(label: "تاريخ الميلاد",
                  controller: _birth,
                  validate: (value)=>Validator().validateEmpty(value: value),
                  prefix: Icon(Icons.calendar_today_outlined),),

                IconTextFiled(label: "البلد/المحافظة",
                  controller: _city,
                  validate: (value)=>Validator().validateEmpty(value: value),
                  prefix: Icon(Icons.location_on),),

                IconTextFiled(label: "عنوان المنزل",
                  controller: _address,
                  validate: (value)=>Validator().validateEmpty(value: value),
                  prefix: Icon(Icons.location_on),),

                IconTextFiled(
                  label: "رقم الجوال",
                  controller: _phone,
                  validate: (value)=>Validator().validateEmpty(value: value),
                ),

                IconTextFiled(
                  label: "البريد الالكتروني",
                  controller: _mail,
                  validate: (value)=>Validator().validateEmpty(value: value),
                ),

                IconTextFiled(label: "كلمة المرور",
                  controller: _password,
                  validate: (value)=>Validator().validateEmpty(value: value),
                  isPassword: true,),
              ],
            ),
          ),

          Padding(
            padding: EdgeInsets.symmetric(horizontal: 30, vertical: 5),
            child: DefaultButton(
              title: "حفظ",
              onTap: () => _editCustomer(),
            ),

          ),
        ],
      ),
    );
  }
}
