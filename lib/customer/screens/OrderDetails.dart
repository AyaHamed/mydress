import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_dress/customer/screens/Home.dart';
import 'package:my_dress/customer/widgets/Stepper.dart';
import 'package:my_dress/general/constants/DefaultAppBar.dart';
import 'package:my_dress/general/constants/DefaultButton.dart';
import 'package:my_dress/general/constants/MyColors.dart';
import 'package:my_dress/general/constants/MyText.dart';

class OrderDetails extends StatefulWidget {
  @override
  _OrderDetailsState createState() => _OrderDetailsState();
}

class _OrderDetailsState extends State<OrderDetails> {

  Widget orderItem(String title, String detail){
    return Row(
      children: [
        MyText(title: title, color: MyColors.primary),
        Text("  :  "),
        MyText(title: detail, color: MyColors.grey),
      ],
    );
  }

  Widget order(){
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 150,

      decoration: BoxDecoration(
        color: MyColors.white,
        border: Border.all(color: MyColors.greyWhite),
        borderRadius: BorderRadius.circular(5),
      ),

      padding: EdgeInsets.symmetric(horizontal: 15),

      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,

        children: [

          Row(
            children: [
              Container(
                height: 125,
                width: 100,
                child: Image.asset("images/model.PNG", fit: BoxFit.fill,),
              ),

              SizedBox(width: 10,),

              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  MyText(title: "فستان خطوبة رمادي", color: MyColors.primary),
                  SizedBox(height: 5,),
                  orderItem("السعر", "299 رس"),
                  orderItem("الكمية", "1"),

                ],
              ),

            ],
          ),


        ],
      ),

    );
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar(con: context, title: "تفاصيل الطلب"),

      body: ListView(children: [

        Padding(
          padding: const EdgeInsets.symmetric(vertical: 30, horizontal: 25),
          child: Container(
            height: MediaQuery.of(context).size.height * 0.7,
            width: MediaQuery.of(context).size.width,

            decoration: BoxDecoration(
              color: MyColors.white,
              borderRadius: BorderRadius.circular(5),
              border: Border.all(color: MyColors.greyWhite),
            ),

            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,

              children: [

                order(),

                SizedBox(height: 30,),

                stepper(
                  context: context,
                  status1: "جاهزة للشحن",
                  icon1: Icons.check_circle,
                  status2: "تم شحنها",
                  icon2: Icons.check_circle,
                  status3: "تم استلامها",
                  icon3: Icons.check_circle_outlined,
                  date1: "27/7/2019",
                  date2: "27/7/2019",
                  date3: "27/7/2019",
                ),
                
              ],
            ),

          ),
        ),

        Padding(
          padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
          child: DefaultButton(
            title: "رجوع",
            onTap: () {Navigator.of(context).push(CupertinoPageRoute(builder: (_) => Home(0)));},
          ), 
          
          // defaultButton(
          //     context: context,
          //     title: "رجوع",
          //     page: BottomNavBar(4)
          // ),
        )

      ],),

    );
  }
}
