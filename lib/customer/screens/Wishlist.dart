import 'package:my_dress/customer/models/FavoriteModel.dart';
import 'package:my_dress/customer/resources/customerRepository.dart';
import 'package:my_dress/customer/widgets/orderItem.dart';
import 'package:my_dress/general/constants/DefaultAppBar.dart';
import 'package:my_dress/general/constants/LoadingDialog.dart';
import 'package:my_dress/general/constants/MyColors.dart';
import 'package:my_dress/general/constants/MyText.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Wishlist extends StatefulWidget {
  @override
  _WishlistState createState() => _WishlistState();
}

class _WishlistState extends State<Wishlist> {
  GlobalKey<ScaffoldState> scaffold=GlobalKey<ScaffoldState>();
  CustomerRepository _repository;

  @override
  void initState() {
    setState(() {
      _repository = CustomerRepository(scaffold: scaffold);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffold,
      backgroundColor: MyColors.greyWhite,
      appBar: DefaultAppBar(
        title: "المفضلة",
        con: context,
        leading: Container(),
        // leading: IconButton(
        //   icon: Icon(
        //     Icons.arrow_back_ios,
        //     color: MyColors.primary,
        //     size: 20,
        //   ),
        //   onPressed: () {Navigator.of(context)
        //       .pushReplacement(CupertinoPageRoute(builder: (_) => Home(0)));},),
        //
      ),

      body: FutureBuilder<FavoriteModel>(
        future: _repository.getFavs(),
        builder: (context, snapshot){
          if (snapshot.hasData){
            if(snapshot.data.data.length!=0){
              return Padding(
                padding: const EdgeInsets.only(top: 10),
                child: ListView.builder(
                  itemBuilder: (context, index){
                  return item(
                    context: context,
                    price: "${snapshot.data.data[index].price}",
                    image: snapshot.data.data[index].image,
                    status: snapshot.data.data[index].avalable,
                  );},
                  itemCount: snapshot.data.data.length,
                ),
              );
            }
            else {
              return Center(
                child: MyText(
                  title: "لا يوجد منتجات مفضلة",
                  color: MyColors.primary,
                  fontWeight: FontWeight.bold,
                  size: 17,
                ),
              );
            }
          }
          else {
            return Center(
              child: LoadingDialog(scaffold).showLoadingView(),
            );
          }
        },
      )
    );
  }
}

