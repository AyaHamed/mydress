import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_dress/customer/screens/Home.dart';
import 'package:my_dress/general/constants/DefaultAppBar.dart';
import 'package:my_dress/general/constants/DefaultButton.dart';
import 'package:my_dress/general/constants/IconTextFiled.dart';
import 'package:my_dress/general/constants/MyColors.dart';
import 'package:my_dress/general/constants/MyText.dart';

class ContactUs extends StatefulWidget {
  @override
  _ContactUsState createState() => _ContactUsState();
}

class _ContactUsState extends State<ContactUs> {

  Widget item(IconData icon, String hint){
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 40),
      child: Container(
        height: 50,
        width: MediaQuery.of(context).size.width,

        decoration: BoxDecoration(
            color: MyColors.white,
            border: Border.all(color: MyColors.greyWhite),
            borderRadius: BorderRadius.circular(30)
        ),

        padding: EdgeInsets.symmetric(horizontal: 20),

        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [

            Container(
              height: 30, width: 30,

              decoration: BoxDecoration(
                color: MyColors.primary,
                border: Border.all(color: MyColors.grey),
                borderRadius: BorderRadius.circular(30),
              ),

              child: Icon(icon, color: MyColors.white, size: 20,),

            ),

            SizedBox(width: 15,),
            MyText(
              title: hint,
              align: TextAlign.right,
                color: MyColors.grey
            ),


          ],
        ),

      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar(con: context, title: "تواصل معنا"),

      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: ListView(
          children: [

            SizedBox(height: 30,),

            item(Icons.location_on, "aaaaaaaaaaaa"),
            item(Icons.phone, "aaaaaaaaaaaa"),
            item(Icons.alternate_email, "aaaaaaaaaaaa"),

            SizedBox(height: 70,),

            IconTextFiled(
              label: "الاسم",
              prefix: Icon(Icons.person_outline),
              borderColor: MyColors.greyWhite,),

            IconTextFiled(
              label: "رقم الهاتف",
              prefix: Icon(Icons.phone_android),
              borderColor: MyColors.greyWhite,),

            IconTextFiled(
              label: "البريد الاكتروني",
              prefix: Icon(Icons.mail_outline_sharp),
              borderColor: MyColors.greyWhite,),


            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
              child: Container(
                height: 150,
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                    color: MyColors.white,
                    border: Border.all(color: MyColors.greyWhite),
                    borderRadius: BorderRadius.circular(30)
                ),

                padding: EdgeInsets.symmetric(horizontal: 25),

                child: TextFormField(
                  keyboardType: TextInputType.multiline,
                  minLines: 1,
                  maxLines: 5,
                  textAlign: TextAlign.right,
                  decoration: InputDecoration(
                    suffixIcon: Icon(Icons.message, color: MyColors.grey, size: 20,),
                    hintText: "الرسالة",
                      hintStyle: TextStyle(
                        color: MyColors.grey, fontSize: 20
                      ),

                      focusedBorder: InputBorder.none,
                      enabledBorder: InputBorder.none
                  ),

                ),

              ),
            ),

            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 25, vertical: 10),
              child: DefaultButton(
                title: "إرسال",
                onTap: () {Navigator.of(context)
                    .pushReplacement(CupertinoPageRoute(builder: (_) => Home(0)));},
              ),

            )

          ],
        ),
      ),

    );
  }
}
