import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:my_dress/general/constants/DefaultAppBar.dart';
import 'package:my_dress/general/constants/DefaultButton.dart';
import 'package:my_dress/general/constants/LabelTextField.dart';
import 'package:my_dress/general/constants/MyColors.dart';
import 'package:my_dress/general/constants/MyText.dart';
import 'package:my_dress/provider/widgets/newSteps.dart';

import 'Home.dart';


class Customize3 extends StatefulWidget {
  @override
  _Customize3State createState() => _Customize3State();
}

class _Customize3State extends State<Customize3> {
  File _image;

  Future getImage() async {
    var image = await ImagePicker.pickImage(source: ImageSource.camera);

    setState(() {
      _image = image;
    });
  }

  Future getPhoto() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);

    setState(() {
      _image = image;
    });
  }
  void _onPressed(){
    showModalBottomSheet(context: context, builder: (context) {
      return Container(
        height: 130,
        child: Column(children: <Widget>[
          SizedBox(height: 10,),
          InkWell( onTap: (){getImage();},
            child: Container(height: 40, width: 150, decoration: BoxDecoration(color: MyColors.greyWhite),
              child: Center(child:Text('Take a picture')),
            ),),
          SizedBox(height: 20,),
          InkWell( onTap: (){getPhoto();},
            child: Container(height: 40, width: 150, decoration: BoxDecoration(color: MyColors.greyWhite),
              child: Center(child: Text('Choose a picture')),
            ),),
        ],),
      );
    });
  }

  Widget picture(){
    return InkWell(
        onTap: _onPressed,
        child: Container(
          height: MediaQuery.of(context).size.height * 0.1,
          width: MediaQuery.of(context).size.width * 0.23,
          decoration: BoxDecoration(
              color: MyColors.white,
              border: Border.all(color: MyColors.greyWhite)
          ),
          child: Container(
            height: 80,
            width: 80,
            alignment: Alignment.topLeft,
            child: Visibility(
              child: InkWell(
                child: Icon(
                  Icons.remove_circle,
                  size: 22,
                  color: MyColors.blackOpacity,
                ),
                onTap: () {
                  setState(() {
                    _image = null;
                  });
                },
              ),
              visible: _image == null? false : true,
            ),
            decoration: BoxDecoration(
              //color: MyColors.primary,
              image: DecorationImage(
                image: _image == null?
                AssetImage("images/add_photo@2x.png",)
                    :FileImage(_image,)),
            ),
          ),
        ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar(con: context, title: "تفصيل فستان"),
      body: ListView(
        children: [

          steps(
              context: context,
              border1: MyColors.primary,
              fill1: MyColors.gold,
              border2: MyColors.primary,
              fill2: MyColors.gold,
              border3: MyColors.primary,
              fill3: MyColors.gold
          ),

          Padding(
            padding: const EdgeInsets.symmetric(vertical: 30),
            child: Image.asset("images/body@3x.png", height: 200,),
          ),

          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 25),
            child: Container(
              width: MediaQuery.of(context).size.width,

              decoration: BoxDecoration(
                //color: Colors.green,
                borderRadius: BorderRadius.circular(8),
                border: Border.all(color: MyColors.greyWhite),
              ),

              padding: EdgeInsets.symmetric(horizontal: 15, vertical: 15),

              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [

                  LabelTextField(
                    label: "طول الكم",
                    isPassword: false,
                  ),
                  LabelTextField(
                    label: "وسع الكم",
                    isPassword: false,
                  ),
                  LabelTextField(
                    label: "محيط الكم",
                    isPassword: false,
                  ),
                  LabelTextField(
                    label: "   القياس من الكتف إلى الصدر",
                    isPassword: false,
                  ),

                  MyText(title: "إضافة صورة لشكل الفستان", color: MyColors.grey,),
                  
                  Container(
                    height: 150,
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      color: MyColors.white,
                      border: Border.all(color: MyColors.greyWhite),
                      borderRadius: BorderRadius.circular(10)
                    ),
                    
                    child: picture(),
                    //Image.asset("images/profile@2x.png", color: MyColors.greyWhite,),
                    
                  ),
                  
                  MyText(title: "الملاحظات", color: MyColors.grey,),

                  Container(
                    height: 70,
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                        color: MyColors.white,
                        border: Border.all(color: MyColors.greyWhite),
                        borderRadius: BorderRadius.circular(10)
                    ),

                    child: TextFormField(
                      keyboardType: TextInputType.multiline,
                      minLines: 1,
                      maxLines: 3,
                      decoration: InputDecoration(
                        focusedBorder: InputBorder.none,
                        enabledBorder: InputBorder.none
                      ),
                    ),

                  ),
                ],
              ),

            ),
          ),

          Padding(
            padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 15),
            child: DefaultButton(
              title: "إرسال البيانات",
              onTap: () {Navigator.of(context).push(CupertinoPageRoute(builder: (_) => Home(0)));},
            ),
          ),

        ],
      ),

    );
  }
}
