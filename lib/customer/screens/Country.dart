import 'package:flutter/material.dart';
import 'package:my_dress/general/constants/DefaultAppBar.dart';
import 'package:my_dress/general/constants/MyColors.dart';
import 'package:my_dress/general/constants/MyText.dart';

class Country extends StatefulWidget {
  @override
  _CountryState createState() => _CountryState();
}

class _CountryState extends State<Country> {

  int group = 0;

  Widget country(String title, int value, String image){
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: 60,
        padding: EdgeInsets.only(right: 10),
        decoration: BoxDecoration(
          color: MyColors.white,
          border: Border.all(color: MyColors.grey),
        ),

        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            CircleAvatar(
              backgroundColor: Colors.transparent,
              backgroundImage: AssetImage(image),
            ),

            MyText(title: title, color: MyColors.primary),

            Container(
              width: MediaQuery.of(context).size.width * 0.4,
              child: ListTile(
                leading: Radio(
                  value: value,
                  groupValue: group,
                  onChanged: (value){
                    group = value;
                    setState(() {
                    });
                    print(group);
                  },),
              ),
            ),



          ],
        ),
      ),
    );
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      
      appBar: DefaultAppBar(con: context, title: "الدولة"),
      
      body: Column(
        children: [

          Padding(
            padding: const EdgeInsets.only(top: 25, right: 20, left: 20),
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: 60,
              decoration: BoxDecoration(
                color: MyColors.white,
                border: Border.all(color: MyColors.grey),
              ),
              child: Center(
                  child: MyText(title: "اختر دولتك", color: MyColors.primary)
              ),

            ),
          ),

          country("المملكة العربية السعودية", 0, "images/flag2.jpg")

        ],
      ),
    );
  }
}
