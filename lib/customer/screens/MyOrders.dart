import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_dress/customer/screens/OrderDetails.dart';
import 'package:my_dress/customer/widgets/orderItem.dart';
import 'package:my_dress/general/constants/DefaultAppBar.dart';
import 'package:my_dress/general/constants/MyColors.dart';

class MyOrders extends StatefulWidget {
  @override
  _MyOrdersState createState() => _MyOrdersState();
}

class _MyOrdersState extends State<MyOrders> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(

      appBar: DefaultAppBar(con: context, title: "طلباتي",),

        body: InkWell(

          onTap: () {Navigator.of(context).pushReplacement(CupertinoPageRoute(builder: (_) => OrderDetails()));},

          child: Padding(
            padding: const EdgeInsets.only(top: 20, right: 15, left: 15),
            child: Container(

              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,

              decoration: BoxDecoration(
                border: Border.all(color: MyColors.greyWhite),
                borderRadius: BorderRadius.circular(5)
              ),

              child: ListView.builder(itemBuilder: (context, index){
                return item(context: context);},
                itemCount: 10,
              ),

            ),
          ),
        )

    );
  }
}
