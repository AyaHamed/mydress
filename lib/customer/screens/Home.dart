import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:my_dress/customer/screens/HomePage.dart';
import 'package:my_dress/general/constants/MyColors.dart';
import 'Cart.dart';
import 'Profile.dart';
import 'Search.dart';
import 'Wishlist.dart';

class Home extends StatefulWidget {

  final int newIndex;
  Home(this.newIndex);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {

  int _selectIndex;

  @override
  void initState() {
    _selectIndex= widget.newIndex;
    super.initState();
  }

  var pages= [
    HomePage(),
    Wishlist(),
    Cart(),
    Search(),
    Profile(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        bottomNavigationBar: BottomNavigationBar(
          showSelectedLabels: false,
          showUnselectedLabels: false,
          type: BottomNavigationBarType.fixed,
          selectedItemColor: MyColors.gold,
          currentIndex: _selectIndex,
          onTap: (index){
            print(index);
            setState(() {
              _selectIndex= index;
            });
          },
          items: [
            BottomNavigationBarItem(
                icon: Icon(Icons.home_outlined, size: 27,),
                title: Text('Home')
            ),
            BottomNavigationBarItem(
                icon: Icon(Icons.favorite_border, size: 24,),
                title: Text('Wishlist')
            ),
            BottomNavigationBarItem(
                icon: Icon(Icons.shopping_cart, size: 23,),
                title: Text('Cart')
            ),
            BottomNavigationBarItem(
                icon: Icon(Icons.search, size: 25,),
                title: Text('Search')
            ),
            BottomNavigationBarItem(
                icon: Icon(Icons.person_outline, size: 25,),
                title: Text('Profile')
            ),
          ],
        ),
        body: pages[_selectIndex]
    );
  }
}
