import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_dress/customer/screens/Customize_3.dart';
import 'package:my_dress/general/constants/DefaultAppBar.dart';
import 'package:my_dress/general/constants/DefaultButton.dart';
import 'package:my_dress/general/constants/LabelTextField.dart';
import 'package:my_dress/general/constants/MyColors.dart';
import 'package:my_dress/provider/widgets/newSteps.dart';

class Customize2 extends StatefulWidget {
  @override
  _Customize2State createState() => _Customize2State();
}

class _Customize2State extends State<Customize2> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar(con: context, title: "تفصيل فستان"),
      body: ListView(
        children: [

          steps(
              context: context,
              border1: MyColors.primary,
              fill1: MyColors.gold,
              border2: MyColors.primary,
              fill2: MyColors.gold,
          ),

          Padding(
            padding: const EdgeInsets.symmetric(vertical: 30),
            child: Image.asset("images/body@3x.png", height: 200,),
          ),

          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 25),
            child: Container(
              width: MediaQuery.of(context).size.width,

              decoration: BoxDecoration(
                //color: Colors.green,
                borderRadius: BorderRadius.circular(8),
                border: Border.all(color: MyColors.greyWhite),
              ),

              padding: EdgeInsets.symmetric(horizontal: 15, vertical: 15),

              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [

                  LabelTextField(
                    label: "قياس محيط الصدر",
                    isPassword: false,
                  ),
                  LabelTextField(
                    label: "قياس محيط الوسط",
                    isPassword: false,
                  ),
                  LabelTextField(
                    label: "قياس محيط الورك",
                    isPassword: false,
                  ),
                  LabelTextField(
                    label: "قياس محيط الفخد",
                    isPassword: false,
                  ),
                  LabelTextField(
                    label: "قياس الكتف (من الكتف إلى الكتف الآخر)",
                    isPassword: false,
                  ),
                  LabelTextField(
                    label: "القياس من الكتف إلى الخصر   ",
                    isPassword: false,
                  ),

                  // title("قياس محيط الصدر"),
                  // IconTextFiled(borderColor: MyColors.grey,),
                  // //iconTextField(context, 0, 10, null, null, false, color: Colors.grey[300]),
                  //
                  // title("قياس محيط الوسط"),
                  // IconTextFiled(borderColor: MyColors.grey,),
                  // //iconTextField(context, 0, 10, null, null, false, color: Colors.grey[300]),
                  //
                  // title("قياس محيط الورك"),
                  // IconTextFiled(borderColor: MyColors.grey,),
                  // //iconTextField(context, 0, 10, null, null, false, color: Colors.grey[300]),
                  //
                  // title("قياس محيط الفخد"),
                  // IconTextFiled(borderColor: MyColors.grey,),
                  // //iconTextField(context, 0, 10, null, null, false, color: Colors.grey[300]),
                  //
                  // title("قياس الكتف (من الكتف إلى الكتف الآخر)"),
                  // IconTextFiled(borderColor: MyColors.grey,),
                  // //iconTextField(context, 0, 10, null, null, false, color: Colors.grey[300]),
                  //
                  // title("القياس من الكتف إلى الخصر   "),
                  // IconTextFiled(borderColor: MyColors.grey,),
                  // //iconTextField(context, 0, 10, null, null, false, color: Colors.grey[300]),

                ],
              ),

            ),
          ),

          Padding(
            padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 15),
            child: DefaultButton(
              color:MyColors.primary,
              title: "التالي",
              onTap: () {Navigator.of(context).push(CupertinoPageRoute(builder: (_) => Customize3()));},
            ),
          ),

        ],
      ),
    );
  }
}
