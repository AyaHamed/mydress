import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:intl/intl.dart';
import 'package:my_dress/customer/models/ProductModel.dart';
import 'package:my_dress/customer/resources/customerRepository.dart';
import 'package:my_dress/customer/widgets/dressColor.dart';
import 'package:my_dress/customer/widgets/title.dart';
import 'package:my_dress/general/constants/CachedImage.dart';
import 'package:my_dress/general/constants/DefaultAppBar.dart';
import 'package:my_dress/general/constants/DefaultButton.dart';
import 'package:my_dress/general/constants/LoadingDialog.dart';
import 'package:my_dress/general/constants/MyColors.dart';
import 'package:my_dress/general/constants/MyText.dart';
import 'package:my_dress/general/utilities/Validator.dart';
import 'Home.dart';
import 'ReviewOrder.dart';

class DressDetails extends StatefulWidget {
  final int id;
  final String title;

  const DressDetails({Key key, this.id, this.title}) : super(key: key);

  @override
  _DressDetailsState createState() => _DressDetailsState();
}

enum SingingCharacter { buy, rent }

class _DressDetailsState extends State<DressDetails> {
  ProductModel _productModel;
  int group = 0;
  String date = "اختر التاريخ";
  String date2 = "اختر التاريخ";
  int _qty = 1;
  String color;
  int size_id;
  DateTime initialDate = DateTime.now();
  TextEditingController _date1 = TextEditingController();
  TextEditingController _date2 = TextEditingController();
  TextEditingController _comment = TextEditingController();

  GlobalKey<ScaffoldState> scaffold = GlobalKey<ScaffoldState>();
  CustomerRepository _repository;

  @override
  void initState() {
    setState(() {
      _repository = CustomerRepository(scaffold: scaffold);
    });
    super.initState();
  }

  _validateColor(String color) {
    if (color.startsWith("#")) {
      return int.parse(color.replaceAll("#", "0xff"));
    } else {
      return int.parse("0xffffffff");
    }
  }

  void _increase() {
    setState(() {
      _qty++;
    });
  }

  void _decrease() {
    setState(() {
      if (_qty != 0) _qty--;
    });
  }

  _addComment() async {
    await _repository.addComment(
        widget.id,
        _comment.text);
    _productModel = await _repository.getDetails(widget.id);
    setState(() {
      _comment.clear();
    });
  }

_addToCart() async {
    await _repository.addToCart(
        color,
        size_id,
        widget.id,
        _qty,
        group,
        _date1.text,
        _date2.text
    );
}

  _rate() {
    showDialog(
        context: context,
        barrierDismissible: false,
        child: AlertDialog(
          contentPadding: EdgeInsets.symmetric(horizontal: 20, vertical: 30),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
          content: Container(
            height: MediaQuery.of(context).size.height * 0.2,
            width: MediaQuery.of(context).size.width * 0.5,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                MyText(
                  title: "قم بقييم الفستان الآن  !",
                  color: MyColors.primary,
                ),
                SizedBox(
                  height: 20,
                ),
                RatingBar(
                  initialRating: 0,
                  minRating: 1,
                  allowHalfRating: true,
                  itemCount: 5,
                  itemSize: 30,
                  unratedColor: MyColors.grey,
                  itemBuilder: (context, _) => Icon(
                    Icons.star,
                    color: MyColors.gold,
                  ),
                  onRatingUpdate: (rating) {
                    print(rating);
                  },
                ),
                SizedBox(
                  height: 20,
                ),
                InkWell(
                  onTap: () => Navigator.pop(context),
                  child: Container(
                    height: 50,
                    width: 100,
                    decoration: BoxDecoration(
                      color: MyColors.gold,
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Center(
                      child: MyText(
                        title: "تأكيد",
                        color: MyColors.white,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ));
  }

  _alret({String title, String detail, String image, int price, bool available}) {
    showDialog(
        context: context,
        barrierDismissible: false,
        child: AlertDialog(
          contentPadding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
          content: Container(
            height: 340,
            width: MediaQuery.of(context).size.width * 0.8,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                InkWell(
                  onTap: () => Navigator.pop(context),
                  child: Image.asset(
                    "images/cancel@3x.png",
                    height: 20,
                    width: 20,
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Icon(
                      Icons.check_circle,
                      size: 30,
                      color: MyColors.gold,
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    MyText(
                      title: "أضـيف إلى عربة تسوقك  !",
                      color: MyColors.primary,
                      size: 16,
                      fontWeight: FontWeight.bold,
                    )
                  ],
                ),
                //SizedBox(height: 20),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 20),
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    height: 120,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                            height: 100,
                            width: 60,
                            // child: Image.asset(
                            //   "images/model.PNG",
                            //   fit: BoxFit.fill,
                            // ),
                            child: CachedImage(
                              url: image,
                              fit: BoxFit.fill,
                            )),
                        SizedBox(
                          width: 10,
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            MyText(
                              title: title,
                              //title: "فستان خطوبة وردي",
                              color: MyColors.primary,
                              align: TextAlign.start,
                              fontWeight: FontWeight.bold,
                            ),
                            Container(
                              width: 180,
                              height: 70,
                              child: MyText(
                                //title: "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
                                title: detail,
                                color: MyColors.grey,
                                size: 10,
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                            Row(
                              children: [
                                MyText(
                                  title: available == true
                                      ? "متوفر حاليا"
                                      : "غير متوفر حاليا",
                                  color: MyColors.primary,
                                ),
                                SizedBox(
                                  width: 5,
                                ),
                                Icon(
                                  available == true
                                      ? Icons.check_circle
                                      : Icons.remove_circle,
                                  size: 20,
                                  color: MyColors.gold,
                                ),
                                SizedBox(
                                  width: 20,
                                ),
                                //defaultText("299 رس", MyColors.primary),
                                MyText(
                                  //title: "299 رس",
                                  title: "$price  رس ",
                                  color: MyColors.primary,
                                ),
                              ],
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),

                DefaultButton(
                  title: "تفاصيل عربة التسوق",
                  color: MyColors.gold,
                  onTap: () {
                    Navigator.of(context)
                        .push(CupertinoPageRoute(builder: (_) => Home(2)));
                  },
                ),
                DefaultButton(
                  title: "شراء",
                  color: MyColors.gold,
                  onTap: () => _addToCart(),
                ),
              ],
            ),
          ),
        ));
  }

  Widget button(String sign, onTap) {
    return InkWell(
      onTap: onTap,
      child: Container(
        height: 30,
        width: 30,
        decoration: BoxDecoration(border: Border.all(color: MyColors.grey)),
        child: Center(
          child: Text(
            sign,
            style: TextStyle(color: MyColors.blackOpacity, fontSize: 25),
          ),
        ),
      ),
    );
  }

  Widget countButton() {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
      child: Row(
        children: [
          button("+", _increase),
          Container(
            height: 30,
            width: 30,
            decoration: BoxDecoration(
                border: Border.all(color: MyColors.grey),
                color: MyColors.greyWhite),
            child: Center(
              child: MyText(
                title: '$_qty',
                color: MyColors.primary,
                size: 15,
              ),
              //Text('$_qty', style: TextStyle(color: MyColors.primary, fontSize: 20),),
            ),
          ),
          button("-", _decrease),
        ],
      ),
    );
  }

  bool _visibility = true;

  void _changeVisibility(bool visibility) {
    setState(() {
      _visibility = !_visibility;
    });
  }

  Widget review({String name, String image, String comment, double rate}) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        vertical: 5,
      ),
      child: Container(
        width: MediaQuery.of(context).size.width,
        color: MyColors.greyWhite,
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Container(
              height: 50,
              width: 50,
              decoration:
                  BoxDecoration(borderRadius: BorderRadius.circular(30)),
              child: CachedImage(
                url: image,
                fit: BoxFit.fill,
              ),
            ),
            SizedBox(
              width: 20,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  width: MediaQuery.of(context).size.width - 150,
                  child: MyText(
                    //title: "اسراء العجمي",
                    title: name,
                    color: MyColors.primary,
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width - 170,
                  child: MyText(
                    //title: "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
                    title: comment,
                    align: TextAlign.start,
                    color: MyColors.grey,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget description(String title, String detail) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Icon(
          Icons.circle,
          size: 10,
          color: MyColors.gold,
        ),
        SizedBox(
          width: 10,
        ),
        MyText(
          title: title,
          color: MyColors.grey,
        ),
        SizedBox(
          width: 5,
        ),
        MyText(
          title: detail,
          color: MyColors.grey,
        ),
      ],
    );
  }

  Widget offstageButton(String text) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        FlatButton(
            onPressed: () {
              _changeVisibility(false);
            },
            child: MyText(
              title: text,
              color: MyColors.primary,
            )),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Icon(
            Icons.arrow_drop_down_sharp,
            color: MyColors.primary,
            size: 30,
          ),
        ),
      ],
    );
  }

  Widget item(String text) {
    return Container(
      height: 30,
      width: MediaQuery.of(context).size.width * 0.4,
      color: MyColors.greyWhite,
      child: Center(
        child: Text(
          text,
          style: TextStyle(color: MyColors.grey, fontSize: 15),
        ),
      ),
    );
  }

  Widget itemDetails(String text, String text2) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          item(text),
          item(text2),
        ],
      ),
    );
  }

  Widget size({String title, Color color}) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 5, vertical: 10),
      child: Container(
        height: 30,
        width: 40,
        decoration: BoxDecoration(
          color: MyColors.white,
          border: Border.all(color: MyColors.grey),
        ),
        child: Center(
          child: MyText(
            title: title,
            //color: MyColors.primary,
            color: color,
            fontWeight: FontWeight.bold,
            size: 15,
          ),
        ),
      ),
    );
  }

  _selectDate(String index) {
    DatePicker.showDatePicker(context,
        showTitleActions: true,
        minTime: initialDate,
        maxTime: DateTime(2025, 12, 31), onChanged: (date) {
      setState(() {
        if (index == "1") {
          this.date = DateFormat("dd/MM/yyyy", "en").format(date);
        } else {
          this.date2 = DateFormat("dd/MM/yyyy", "en").format(date);
        }
      });
      print('change ${this.date}');
    }, onConfirm: (date) {
      print('confirm $date');
    }, currentTime: DateTime.now(), locale: LocaleType.ar);
  }

  Widget dateButton(String select) {
    return FlatButton(
        onPressed: () => _selectDate(select),
        child: Container(
          height: 30,
          width: 170,
          decoration: BoxDecoration(
              color: MyColors.white,
              borderRadius: BorderRadius.circular(20)),
          child: Center(
            child: MyText(
              title: select == "1" ? '${this.date} ' : '${this.date2} ',
              color: MyColors.primary,
              size: 12,
              fontWeight: FontWeight.bold,
            ),
          ),
        ));
  }

  Widget recommended({BuildContext context, String image, String title, String detail, int price}) {
    return InkWell(
      onTap: () {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => DressDetails()));
      },
      child: Container(
        width: MediaQuery.of(context).size.width * .28,
        height: 250,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              height: 160,
              width: MediaQuery.of(context).size.width * .28,
              //child: Image.asset("images/model.PNG", fit: BoxFit.fill,),
              child: CachedImage(
                url: image,
                fit: BoxFit.fill,
              ),
            ),

            MyText(
              title: title, //"فستان خطوبة ",
              color: MyColors.primary,
            ),
            MyText(
              title: "  $price ر س ",
              color: MyColors.primary,
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: scaffold,
        floatingActionButton: InkWell(
          onTap: () {
            _alret(
              title: _productModel.data.name,
              detail: _productModel.data.des,
              price: _productModel.data.price,
              image: _productModel.data.images[0].image,
              available: _productModel.data.avalable,
            );
          },
          child: Container(
            height: 80,
            width: 80,
            decoration: BoxDecoration(borderRadius: BorderRadius.circular(50)),
            child: Image.asset(
              "images/basket@3x.png",
            ),
          ),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        appBar: DefaultAppBar(
          title: widget.title,
          con: context,
        ),
        body: FutureBuilder<ProductModel>(
          future:
              _productModel == null ? _repository.getDetails(widget.id) : null,
          builder: (context, snapshot) {
            print("data ${snapshot.hasData}");
            _productModel == null ? _productModel = snapshot.data: null ;
            if (snapshot.hasData) {
              return ListView(
                shrinkWrap: true,
                children: [
                  Container(
                    height: 300,
                    width: MediaQuery.of(context).size.width,
                    child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemBuilder: (context, index) {
                        return Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 5, vertical: 11),
                          child: CachedImage(
                            url: snapshot.data.data.images[index].image,
                            width: 200,
                            height: 350,
                          ),
                        );
                      },
                      itemCount: snapshot.data.data.images.length,
                    ),
                  ),
                  Container(
                      width: MediaQuery.of(context).size.width,
                      color: MyColors.white,
                      padding: EdgeInsets.symmetric(vertical: 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(
                              horizontal: 20,
                            ),
                            child: Row(
                              children: [
                                MyText(
                                  //title: "فستان خطوبة وردي",
                                  title: snapshot.data.data.name,
                                  color: MyColors.primary,
                                ),
                                Expanded(child: SizedBox()),
                                Icon(
                                  Icons.remove_red_eye,
                                  color: MyColors.primary,
                                ),
                                SizedBox(
                                  width: 5,
                                ),
                                Text("${snapshot.data.data.visited}"),
                                SizedBox(
                                  width: 5,
                                ),
                                MyText(
                                  title: 'مشاهدة ',
                                  color: MyColors.grey,
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(
                              horizontal: 20,
                            ),
                            child: Row(
                              children: [
                                Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 10),
                                  child: MyText(
                                    //title: "299",
                                    title: "${snapshot.data.data.price}",
                                    color: MyColors.primary,
                                  ),
                                ),
                                MyText(
                                  title: "ر س",
                                  color: MyColors.primary,
                                ),
                                Expanded(child: SizedBox()),
                                RatingBar(
                                  initialRating:
                                      snapshot.data.data.rate.toDouble(),
                                  minRating: 1,
                                  allowHalfRating: true,
                                  itemCount: 5,
                                  itemSize: 20,
                                  unratedColor: MyColors.grey,
                                  itemBuilder: (context, _) => Icon(
                                    Icons.star,
                                    color: MyColors.gold,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 20),
                            child: Container(
                              width: 130,
                              height: 30,
                              decoration: BoxDecoration(
                                color: MyColors.greyWhite,
                                borderRadius: BorderRadius.circular(20),
                              ),
                              child: Padding(
                                padding: EdgeInsets.symmetric(horizontal: 3),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    MyText(
                                      title: snapshot.data.data.avalable == true
                                          ? "متوفر حاليا"
                                          : "غير متوفر حاليا",
                                      color: MyColors.grey,
                                    ),
                                    SizedBox(
                                      width: 5,
                                    ),
                                    Icon(
                                      snapshot.data.data.avalable == true
                                          ? Icons.check_circle_outline
                                          : Icons.remove_circle,
                                      color: MyColors.gold,
                                      size: 15,
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ],
                      )),
                  Divider(
                    color: MyColors.greyWhite,
                    thickness: 3,
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    color: MyColors.white,
                    child: Padding(
                      padding:
                          EdgeInsets.symmetric(horizontal: 20, vertical: 15),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          MyText(
                            title: "المقاس  : ",
                            color: MyColors.primary,
                          ),
                          Container(
                              height: 60,
                              child: ListView.builder(
                                scrollDirection: Axis.horizontal,
                                itemCount: snapshot.data.data.sizes.length,
                                itemBuilder: (context, index) {
                                  return InkWell(
                                    onTap: () {
                                      for(int i =0;i<snapshot.data.data.sizes.length;i++){
                                        setState(() {
                                          snapshot.data.data.sizes[i].selected=false;
                                        });
                                      }
                                      setState(() {
                                        snapshot.data.data.sizes[index].selected = !snapshot.data.data.sizes[index].selected;
                                        size_id = snapshot.data.data.sizes[index].id;
                                        print(size_id);
                                      });
                                    },
                                    child: size(
                                        title: "${snapshot.data.data.sizes[index].name}",
                                        color: snapshot.data.data.sizes[index].selected == false ? MyColors.grey
                                            : MyColors.primary),
                                  );
                                },
                              )),
                          MyText(
                            title: "الألوان  : ",
                            color: MyColors.primary,
                          ),
                          Container(
                              height: 50,
                              child: ListView.builder(
                                scrollDirection: Axis.horizontal,
                                itemCount: snapshot.data.data.colors.length,
                                itemBuilder: (context, index) {
                                  return InkWell(
                                      onTap: () {
                                        for(int i =0;i<snapshot.data.data.colors.length;i++){
                                          setState(() {
                                            snapshot.data.data.colors[i].selected=false;
                                          });
                                        }
                                        setState(() {
                                          snapshot.data.data.colors[index].selected=!snapshot.data.data.colors[index].selected;
                                          color = snapshot.data.data.colors[index].color;
                                          print(color);
                                        });
                                      },
                                      child: dressColor(
                                          Color(_validateColor(
                                              "${snapshot.data.data.colors[index].color}")),
                                          selected: snapshot.data.data
                                              .colors[index].selected));
                                },
                              )),
                        ],
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 20,
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        MyText(
                          title: "شراء/إيجار  :",
                          color: MyColors.primary,
                        )
                      ],
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        width: MediaQuery.of(context).size.width * 0.4,
                        child: ListTile(
                          title: MyText(
                            title: "إيجار",
                            color: MyColors.primary,
                          ),
                          leading: Radio(
                            value: 1,
                            groupValue: group,
                            onChanged: (value) {
                              print(value);
                              setState(() {
                                group = value;
                              });
                            },
                          ),
                        ),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width * 0.4,
                        child: ListTile(
                          title: MyText(
                            title: "شراء",
                            color: MyColors.primary,
                          ),
                          leading: Radio(
                            value: 0,
                            groupValue: group,
                            onChanged: (value) {
                              group = value;
                              setState(() {});
                              print(group);
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                  Visibility(
                    child: Padding(
                      padding:
                          EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                      child: Divider(
                        thickness: 2,
                      ),
                    ),
                    visible: group == 0 ? true : false,
                    replacement: Container(
                      color: MyColors.greyWhite,
                      width: MediaQuery.of(context).size.width,
                      padding:
                          EdgeInsets.symmetric(horizontal: 25, vertical: 15),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              title(text: "مدة الإيجار  :"),
                              Spacer(),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  MyText(
                                    title: "من",
                                    color: MyColors.primary,
                                  ),
                                  MyText(
                                    title: "إلى",
                                    color: MyColors.primary,
                                  ),
                                ],
                              ),
                              Column(
                                children: [
                                  dateButton("1"),
                                  dateButton("2"),
                                ],
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              title(text: "تأمين الإيجار  : "),
                              Spacer(),
                              MyText(
                                title: "${snapshot.data.data.rent} رس ",
                                color: MyColors.grey,
                              ),
                              Expanded(child: SizedBox()),
                              title(
                                  text: "لليوم الواحد",
                                  fontColor: MyColors.grey,
                                  iconColor: MyColors.gold)
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Padding(
                        padding:
                            EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                        child: MyText(
                          title: "الكمية : ",
                          color: MyColors.primary,
                        ),
                      ),
                      SizedBox(
                        width: 15,
                      ),
                      countButton(),
                    ],
                  ),
                  Divider(
                    color: MyColors.greyWhite,
                    thickness: 3,
                  ),
                  Container(
                      width: MediaQuery.of(context).size.width,
                      color: MyColors.white,
                      padding:
                          EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          MyText(
                            title: "الوصف :  ",
                            color: MyColors.primary,
                          ),
                          description(
                              "كود المنتج :  ", "${snapshot.data.data.code}"),
                          description(
                              "الكمية :  ", "${snapshot.data.data.amount}"),
                          //description("ضريبة الشحن :  ", "مجاني"),

                          Divider(
                            color: MyColors.grey,
                            height: 2,
                          ),

                          Container(
                            width: MediaQuery.of(context).size.width,
                            child: MyText(
                              title: snapshot.data.data.des,
                              align: TextAlign.start,
                              color: MyColors.grey,
                            ),
                          )
                        ],
                      )),
                  offstageButton("تفاصيل المنتج   : "),
                  Offstage(
                    offstage: _visibility,
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      color: MyColors.white,
                      padding:
                          EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          MyText(
                            title: "الوصف : ",
                            color: MyColors.primary,
                          ),
                          description("كود المنتج : ", snapshot.data.data.code),
                          description(
                              "الكمية : ", "${snapshot.data.data.amount}"),
                          //description("ضريبة الشحن : ", "مجاني"),
                          SizedBox(
                            height: 10,
                          ),
                          itemDetails(
                              "نوع القماش", snapshot.data.data.clothType),
                          itemDetails(
                              "شكل الفستان", snapshot.data.data.dressShap),
                          itemDetails(
                              "قصة الفستان", snapshot.data.data.dressStyle),
                        ],
                      ),
                    ),
                  ),
                  offstageButton("قياسات المنتج   : "),
                  Offstage(
                    offstage: _visibility,
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      color: MyColors.white,
                      padding:
                          EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                      child: Image.asset("images/measurement.PNG"),
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    color: MyColors.white,
                    padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                    child: Column(
                      children: [
                        Container(
                          width: MediaQuery.of(context).size.width,
                          child: ListView.builder(
                            physics: NeverScrollableScrollPhysics(),
                            shrinkWrap: true,
                            itemCount: _productModel.comments.length,
                            itemBuilder: (context, index) {
                              return review(
                                  name: _productModel.comments[index].name,
                                  comment: _productModel.comments[index].comment,
                                  image: _productModel.comments[index].image,
                                  rate: _productModel.comments[index].rate.toDouble());
                            },
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 15),
                          child: Row(
                            children: [
                              Container(
                                width: 300,
                                height: 40,
                                padding: EdgeInsets.symmetric(
                                  horizontal: 10,
                                ),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    border:
                                        Border.all(color: MyColors.greyWhite)),
                                child: TextFormField(
                                  textAlign: TextAlign.right,
                                  controller: _comment,
                                  validator: (value) => Validator().validateEmpty(value: value),
                                  decoration: InputDecoration(
                                      hintText: "أضف تعليق",
                                      hintStyle:
                                          TextStyle(color: MyColors.grey),
                                      enabledBorder: InputBorder.none,
                                      focusedBorder: InputBorder.none),
                                ),
                              ),
                              Spacer(),
                              InkWell(
                                child: Icon(
                                  Icons.send,
                                  color: MyColors.grey,
                                ),
                                onTap: () {
                                  _addComment();
                                  //_rate();
                                },
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                      width: MediaQuery.of(context).size.width,
                      height: 250,
                      padding:
                          EdgeInsets.symmetric(vertical: 10, horizontal: 15),
                      child: Wrap(
                        spacing: 10,
                        runSpacing: 15,
                        children: List.generate(
                            snapshot.data.products.length,
                            (index) => recommended(
                                context: context,
                                image: snapshot.data.products[index].image,
                                price:
                                    snapshot.data.products[index].price.toInt(),
                                detail: snapshot.data.products[index].desc,
                                title: snapshot.data.products[index].name)),
                      )),
                  SizedBox(
                    height: 80,
                  )
                ],
              );
            } else {
              return Center(
                child: LoadingDialog(scaffold).showLoadingView(),
              );
            }
          },
        ));
  }
}
