import 'package:flutter/material.dart';
import 'package:my_dress/general/constants/DefaultAppBar.dart';
import 'package:my_dress/general/constants/MyColors.dart';
import 'package:my_dress/general/constants/MyText.dart';

class Help extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(

      appBar: DefaultAppBar(con: context, title: "المساعدة", ),

      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 25, vertical: 30),
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width ,

          decoration: BoxDecoration(
              color: MyColors.white,
              borderRadius: BorderRadius.circular(5),
              border: Border.all(color: MyColors.greyWhite),
          ),

          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 30),

          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [

              Container(
                width: MediaQuery.of(context).size.width - 80,
                  child: MyText(
                      title: "هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق",
                  align: TextAlign.center,
                  )),

              SizedBox(height: 20,),

              Container(
                width: MediaQuery.of(context).size.width - 80,
                  child: MyText(
                      title: "هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق",
                  align: TextAlign.center,
                  )),
            ],
          )
    ),
      )
    );
  }
}
