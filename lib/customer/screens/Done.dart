import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_dress/customer/screens/Home.dart';
import 'package:my_dress/general/constants/DefaultAppBar.dart';
import 'package:my_dress/general/constants/DefaultButton.dart';
import 'package:my_dress/general/constants/MyColors.dart';
import 'package:my_dress/general/constants/MyText.dart';

class Done extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar(con: context, title: "الدفع"),

      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height * 0.5,
          
          decoration: BoxDecoration(
            color: MyColors.white,
            border: Border.all(color:  MyColors.grey),
            borderRadius: BorderRadius.circular(5)
          ),

          padding: EdgeInsets.symmetric(horizontal: 10, vertical: 30),
          
          child: Column(
            children: [
              Image.asset("images/check_img@3x.png", height: 170,),
              SizedBox(height: 30,),
              //defaultText("تم اتمام الطلب بنجاح", Colors.blue[900]),
              MyText(
                title: "تم اتمام الطلب بنجاح",
                color: MyColors.primary,
              ),
              SizedBox(height: 30,),
              DefaultButton(
                title: "العودة إلى الرئيسية",
                onTap: () {Navigator.of(context).push(CupertinoPageRoute(builder: (_) => Home(0) ));},
              ),

          ],),
          
        ),
      ),

    );
  }
}
