import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_dress/customer/screens/Done.dart';
import 'package:my_dress/customer/screens/OnlinePayment.dart';
import 'package:my_dress/general/constants/DefaultAppBar.dart';
import 'package:my_dress/general/constants/MyColors.dart';
import 'package:my_dress/general/constants/MyText.dart';

class PaymentMethods extends StatefulWidget {
  @override
  _PaymentMethodsState createState() => _PaymentMethodsState();
}


class _PaymentMethodsState extends State<PaymentMethods> {

  int group = 0;

  Widget payment(String title, int value, String image){
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: 60,
        padding: EdgeInsets.only(right: 10),
        decoration: BoxDecoration(
          color: MyColors.white,
          border: Border.all(color: MyColors.greyWhite),
        ),

        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [

            Container(
              width: MediaQuery.of(context).size.width * 0.4,
              child: ListTile(
                title: Image.asset(image),
                leading: Radio(
                  value: value,
                  groupValue: group,
                  onChanged: (value){
                    group = value;
                    setState(() {
                    });
                    print(group);
                  },),
              ),
            ),

            MyText(title: title, color: MyColors.primary),

          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar(con: context, title: "طرق الدفع"),
      body: Column(
        children: [

          Padding(
            padding: const EdgeInsets.only(top: 25, right: 20, left: 20),
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: 60,
              decoration: BoxDecoration(
                color: MyColors.white,
                border: Border.all(color: MyColors.greyWhite),
              ),
              child: Center(
                child: MyText(title: "اختر طريقة الدفع", color: MyColors.primary)
              ),

            ),
          ),

          InkWell(
              onTap: () {Navigator.of(context).push(CupertinoPageRoute(builder: (_) => OnlinePayment()));},
              child: payment("بطاقة فيزا", 1, "images/visalogo.png")),

          InkWell(
              onTap: () {Navigator.of(context).push(CupertinoPageRoute(builder: (_) => Done()));},
              child: payment("الدفع عند الاستلام  ", 0, "images/cash.png"))

        ],
      ),
    );
  }
}
