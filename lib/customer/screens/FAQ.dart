import 'package:flutter/material.dart';
import 'package:my_dress/customer/widgets/term.dart';
import 'package:my_dress/general/constants/DefaultAppBar.dart';
import 'package:my_dress/general/constants/MyColors.dart';

class FAQ extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(

      appBar: DefaultAppBar(con: context, title: "الاسئلة الشائعة"),

        body: Padding(
          padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
          child: Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,

            decoration: BoxDecoration(
                color: MyColors.white,
                borderRadius: BorderRadius.circular(5)
            ),

            child: ListView.builder(
              itemBuilder: (context, index){
                return buildTerm(); }, itemCount: 10,
            ),
          ),

        )

    );
  }
}
