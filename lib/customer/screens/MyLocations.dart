import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_dress/customer/models/LocationsModel.dart';
import 'package:my_dress/customer/resources/customerRepository.dart';
import 'package:my_dress/customer/screens/NewLocation.dart';
import 'package:my_dress/general/constants/DefaultAppBar.dart';
import 'package:my_dress/general/constants/DefaultButton.dart';
import 'package:my_dress/general/constants/LoadingDialog.dart';
import 'package:my_dress/general/constants/MyColors.dart';
import 'package:my_dress/general/constants/MyText.dart';

class MyLocations extends StatefulWidget {
  @override
  _MyLocationsState createState() => _MyLocationsState();
}

class _MyLocationsState extends State<MyLocations> {

  GlobalKey<ScaffoldState> scaffold=GlobalKey<ScaffoldState>();
  CustomerRepository _repository;

  @override
  void initState() {
    setState(() {
      _repository = CustomerRepository(scaffold: scaffold);
    });
    super.initState();
  }

  Widget customItem({@required BuildContext context, String address, String region, String governate}) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 15),
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: 80,

        decoration: BoxDecoration(
            color: MyColors.white,
            border: Border.all(color: MyColors.greyWhite)
        ),

        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
          child: Row(
            //mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [

              Icon(Icons.location_on, size: 30, color: MyColors.grey,),
              SizedBox(width: 20,),
              Column( crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  MyText(title: address, color: MyColors.grey),
                  MyText(title: region, color: MyColors.grey),
                  MyText(title: governate, color: MyColors.grey),
                ],
              ),
              Spacer(),
              Icon(Icons.delete_outline,
                color: MyColors.primary,  size: 30,),
            ],),
        ),
      ),
    );
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffold,
      appBar: DefaultAppBar(con: context, title: "مواقع الشحن"),
      body: FutureBuilder<LocationsModel>(
        future: _repository.myLocations(),
        builder: (context, snapshot){
          if (snapshot.hasData){
            if (snapshot.data.data.length != 0){
              return Padding(
                  padding: const EdgeInsets.only(top: 20),
                  child: ListView(
                    children: [
                      Container(
                        height: MediaQuery.of(context).size.height * 0.75,
                        child: ListView.builder(
                          itemCount: snapshot.data.data.length,
                          itemBuilder: (context, index){
                            return customItem(
                              context: context,
                              address: snapshot.data.data[index].address,
                              governate: snapshot.data.data[index].government,
                              region: snapshot.data.data[index].region
                            );
                          },
                        ),
                      ),

                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 25, horizontal: 15),
                        child: DefaultButton(
                          title: "أضف عنوان جديد",
                          onTap: () {Navigator.of(context).push(CupertinoPageRoute(builder: (_) => NewLocation()));},
                        ),
                      )

                    ],
                  )

              );
            }
            else {
              return Center(
                child: MyText(
                  title: "لا يوجد مواقع",
                  color: MyColors.primary,
                  fontWeight: FontWeight.bold,
                  size: 17,
                ),
              );
            }
          }
          else {
            return Center(
              child: LoadingDialog(scaffold).showLoadingView(),
            );
          }
        },
      )

    );
  }
}


