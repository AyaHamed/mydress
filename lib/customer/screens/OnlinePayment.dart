import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_dress/customer/screens/Home.dart';
import 'package:my_dress/general/constants/DefaultAppBar.dart';
import 'package:my_dress/general/constants/DefaultButton.dart';
import 'package:my_dress/general/constants/LabelTextField.dart';
import 'package:my_dress/general/constants/MyColors.dart';
import 'package:my_dress/general/constants/MyText.dart';

class OnlinePayment extends StatefulWidget {
  @override
  _OnlinePaymentState createState() => _OnlinePaymentState();
}

class _OnlinePaymentState extends State<OnlinePayment> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar(con: context, title: "الدفع"),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 30),
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,

          decoration: BoxDecoration(
            color: MyColors.white,
            border: Border.all(color: MyColors.greyWhite),
            borderRadius: BorderRadius.circular(5)
          ),

          child: ListView(
            children: [
              
              SizedBox(height: 20,),
              
              MyText(title: " :  معلومات بطاقة الائتمان"),
              
              Image.asset("images/visa@3x.png", height: 250,),

              // title("رقم البطاقة"),
              // //iconTextField(context, 30, 5, null, null, false, color: Colors.grey[300]),
              // IconTextFiled(
              //   borderColor: MyColors.grey,
              // ),
              LabelTextField(
                label: "رقم البطاقة",
              ),
              LabelTextField(
                label: "اسم حامل البطاقة",
              ),
              LabelTextField(
                label: "تاريخ انتهاء البطاقة",
              ),
              LabelTextField(
                label: "رمز CCV ",
              ),

              // title("اسم حامل البطاقة "),
              // //iconTextField(context, 30, 5, null, null, false, color: Colors.grey[300]),
              // IconTextFiled(
              //   borderColor: MyColors.grey,
              // ),
              //
              // title("تاريخ انتهاء البطاقة "),
              // IconTextFiled(
              //   borderColor: MyColors.grey,
              // ),
              // //iconTextField(context, 30, 5, null, null, false, color: Colors.grey[300]),
              //
              //
              // title(" CCV رمز"),
              // IconTextFiled(
              //   borderColor: MyColors.grey,
              // ),
              // //iconTextField(context, 30, 5, null, null, false, color: Colors.grey[300]),

              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 25, vertical: 10),
                child: DefaultButton(
                  title: "دفع الطلب",
                  onTap: () {Navigator.of(context).push(CupertinoPageRoute(builder: (_) => Home(0)));},
                ),
                // defaultButton(
                //   context: context,
                //   title: "دفع الطلب",
                //   page: BottomNavBar(4),
                // ),
              )

            ],
          ),

        ),
      ),
    );
  }
}
