class CartModel {
  List<Products> products;
  int delivery;
  int amount;
  int total;
  bool haveSale;
  int sale;

  CartModel(
      {this.products,
        this.delivery,
        this.amount,
        this.total,
        this.haveSale,
        this.sale});

  CartModel.fromJson(Map<String, dynamic> json) {
    if (json['products'] != null) {
      products = new List<Products>();
      json['products'].forEach((v) {
        products.add(new Products.fromJson(v));
      });
    }
    delivery = json['delivery'];
    amount = json['amount'];
    total = json['total'];
    haveSale = json['haveSale'];
    sale = json['sale'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.products != null) {
      data['products'] = this.products.map((v) => v.toJson()).toList();
    }
    data['delivery'] = this.delivery;
    data['amount'] = this.amount;
    data['total'] = this.total;
    data['haveSale'] = this.haveSale;
    data['sale'] = this.sale;
    return data;
  }
}

class Products {
  int id;
  int proId;
  String name;
  int price;
  int amount;
  String image;
  String color;
  String size;

  Products(
      {this.id,
        this.proId,
        this.name,
        this.price,
        this.amount,
        this.image,
        this.color,
        this.size});

  Products.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    proId = json['pro_id'];
    name = json['name'];
    price = json['price'];
    amount = json['amount'];
    image = json['image'];
    color = json['color'];
    size = json['size'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['pro_id'] = this.proId;
    data['name'] = this.name;
    data['price'] = this.price;
    data['amount'] = this.amount;
    data['image'] = this.image;
    data['color'] = this.color;
    data['size'] = this.size;
    return data;
  }
}
