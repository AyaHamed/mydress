class LocationsModel {
  List<Data> data;

  LocationsModel({this.data});

  LocationsModel.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  int id;
  String firstName;
  String lastName;
  String mail;
  String phone;
  String postalCode;
  String region;
  String government;
  String address;

  Data(
      {this.id,
        this.firstName,
        this.lastName,
        this.mail,
        this.phone,
        this.postalCode,
        this.region,
        this.government,
        this.address});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    firstName = json['first_name'];
    lastName = json['last_name'];
    mail = json['mail'];
    phone = json['phone'];
    postalCode = json['postal_code'];
    region = json['region'];
    government = json['government'];
    address = json['address'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['first_name'] = this.firstName;
    data['last_name'] = this.lastName;
    data['mail'] = this.mail;
    data['phone'] = this.phone;
    data['postal_code'] = this.postalCode;
    data['region'] = this.region;
    data['government'] = this.government;
    data['address'] = this.address;
    return data;
  }
}
