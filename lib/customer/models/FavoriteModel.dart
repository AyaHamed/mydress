class FavoriteModel {
  List<Data> data;

  FavoriteModel({this.data});

  FavoriteModel.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  int id;
  int proId;
  String name;
  int price;
  bool avalable;
  String image;

  Data({this.id, this.proId, this.name, this.price, this.avalable, this.image});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    proId = json['pro_id'];
    name = json['name'];
    price = json['price'];
    avalable = json['avalable'];
    image = json['image'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['pro_id'] = this.proId;
    data['name'] = this.name;
    data['price'] = this.price;
    data['avalable'] = this.avalable;
    data['image'] = this.image;
    return data;
  }
}