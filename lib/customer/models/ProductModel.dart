class ProductModel {
  Data data;
  List<Comments> comments;
  List<Products> products;
  bool loved;
  bool rated;
  int secure;

  ProductModel(
      {this.data,
        this.comments,
        this.products,
        this.loved,
        this.rated,
        this.secure});

  ProductModel.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    if (json['comments'] != null) {
      comments = new List<Comments>();
      json['comments'].forEach((v) {
        comments.add(new Comments.fromJson(v));
      });
    }
    if (json['products'] != null) {
      products = new List<Products>();
      json['products'].forEach((v) {
        products.add(new Products.fromJson(v));
      });
    }
    loved = json['loved'];
    rated = json['rated'];
    secure = json['secure'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    if (this.comments != null) {
      data['comments'] = this.comments.map((v) => v.toJson()).toList();
    }
    if (this.products != null) {
      data['products'] = this.products.map((v) => v.toJson()).toList();
    }
    data['loved'] = this.loved;
    data['rated'] = this.rated;
    data['secure'] = this.secure;
    return data;
  }
}

class Data {
  List<Images> images;
  String name;
  String des;
  int price;
  int rent;
  int visited;
  int rate;
  bool avalable;
  List<Sizes> sizes;
  List<Colors> colors;
  int sellType;
  int amount;
  String code;
  int cat;
  String clothType;
  String dressShap;
  String dressStyle;
  String brand;
  bool haveVideo;
  String video;

  Data(
      {this.images,
        this.name,
        this.des,
        this.price,
        this.rent,
        this.visited,
        this.rate,
        this.avalable,
        this.sizes,
        this.colors,
        this.sellType,
        this.amount,
        this.code,
        this.cat,
        this.clothType,
        this.dressShap,
        this.dressStyle,
        this.brand,
        this.haveVideo,
        this.video});

  Data.fromJson(Map<String, dynamic> json) {
    if (json['images'] != null) {
      images = new List<Images>();
      json['images'].forEach((v) {
        images.add(new Images.fromJson(v));
      });
    }
    name = json['name'];
    des = json['des'];
    price = json['price'];
    rent = json['rent'];
    visited = json['visited'];
    rate = json['rate'];
    avalable = json['avalable'];
    if (json['sizes'] != null) {
      sizes = new List<Sizes>();
      json['sizes'].forEach((v) {
        sizes.add(new Sizes.fromJson(v));
      });
    }
    if (json['colors'] != null) {
      colors = new List<Colors>();
      json['colors'].forEach((v) {
        colors.add(new Colors.fromJson(v));
      });
    }
    sellType = json['sell_type'];
    amount = json['amount'];
    code = json['code'];
    cat = json['cat'];
    clothType = json['cloth_type'];
    dressShap = json['dress_shap'];
    dressStyle = json['dress_style'];
    brand = json['brand'];
    haveVideo = json['haveVideo'];
    video = json['video'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.images != null) {
      data['images'] = this.images.map((v) => v.toJson()).toList();
    }
    data['name'] = this.name;
    data['des'] = this.des;
    data['price'] = this.price;
    data['rent'] = this.rent;
    data['visited'] = this.visited;
    data['rate'] = this.rate;
    data['avalable'] = this.avalable;
    if (this.sizes != null) {
      data['sizes'] = this.sizes.map((v) => v.toJson()).toList();
    }
    if (this.colors != null) {
      data['colors'] = this.colors.map((v) => v.toJson()).toList();
    }
    data['sell_type'] = this.sellType;
    data['amount'] = this.amount;
    data['code'] = this.code;
    data['cat'] = this.cat;
    data['cloth_type'] = this.clothType;
    data['dress_shap'] = this.dressShap;
    data['dress_style'] = this.dressStyle;
    data['brand'] = this.brand;
    data['haveVideo'] = this.haveVideo;
    data['video'] = this.video;
    return data;
  }
}

class Images {
  String image;

  Images({this.image});

  Images.fromJson(Map<String, dynamic> json) {
    image = json['image'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['image'] = this.image;
    return data;
  }
}

class Sizes {
  int id;
  String name;
  bool selected;
  String color;

  Sizes({this.id, this.name, this.selected, this.color});

  Sizes.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    selected = json['selected'];
    color = json['color'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['selected'] = this.selected;
    data['color'] = this.color;
    return data;
  }
}

class Colors {
  int id;
  String color;
  String val;
  bool selected;

  Colors({this.id, this.color, this.val, this.selected});

  Colors.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    color = json['color'];
    val = json['val'];
    selected = json['selected'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['color'] = this.color;
    data['val'] = this.val;
    data['selected'] = this.selected;
    return data;
  }
}

class Comments {
  int id;
  String comment;
  int rate;
  String image;
  String name;

  Comments({this.id, this.comment, this.rate, this.image, this.name});

  Comments.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    comment = json['comment'];
    rate = json['rate'];
    image = json['image'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['comment'] = this.comment;
    data['rate'] = this.rate;
    data['image'] = this.image;
    data['name'] = this.name;
    return data;
  }
}

class Products {
  int id;
  String name;
  String desc;
  bool loved;
  String image;
  int price;
  int watch;
  int rate;
  int buyed;
  bool avalable;

  Products(
      {this.id,
        this.name,
        this.desc,
        this.loved,
        this.image,
        this.price,
        this.watch,
        this.rate,
        this.buyed,
        this.avalable});

  Products.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    desc = json['desc'];
    loved = json['loved'];
    image = json['image'];
    price = json['price'];
    watch = json['watch'];
    rate = json['rate'];
    buyed = json['buyed'];
    avalable = json['avalable'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['desc'] = this.desc;
    data['loved'] = this.loved;
    data['image'] = this.image;
    data['price'] = this.price;
    data['watch'] = this.watch;
    data['rate'] = this.rate;
    data['buyed'] = this.buyed;
    data['avalable'] = this.avalable;
    return data;
  }
}