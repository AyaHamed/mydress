// To parse this JSON data, do
//
//     final authModel = authModelFromJson(jsonString);

import 'dart:convert';

AuthModel authModelFromJson(String str) => AuthModel.fromJson(json.decode(str));

String authModelToJson(AuthModel data) => json.encode(data.toJson());

class AuthModel {
  AuthModel({
    this.customerId,
    this.name,
    this.phone,
    this.email,
    this.avatar,
    this.token,
    this.lang,
    this.first,
    this.last,
    this.birthday,
    this.city,
    this.address,
  });

  String customerId;
  String name;
  String phone;
  String email;
  String avatar;
  String token;
  int lang;
  dynamic first;
  dynamic last;
  dynamic birthday;
  dynamic city;
  dynamic address;

  factory AuthModel.fromJson(Map<String, dynamic> json) => AuthModel(
    customerId: json["customer_id"],
    name: json["name"],
    phone: json["phone"],
    email: json["email"],
    avatar: json["avatar"],
    token: json["token"],
    lang: json["lang"],
    first: json["first"],
    last: json["last"],
    birthday: json["birthday"],
    city: json["city"],
    address: json["address"],
  );

  Map<String, dynamic> toJson() => {
    "customer_id": customerId,
    "name": name,
    "phone": phone,
    "email": email,
    "avatar": avatar,
    "token": token,
    "lang": lang,
    "first": first,
    "last": last,
    "birthday": birthday,
    "city": city,
    "address": address,
  };
}
