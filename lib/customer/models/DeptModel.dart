// To parse this JSON data, do
//
//     final deptModel = deptModelFromJson(jsonString);

import 'dart:convert';

DeptModel deptModelFromJson(String str) => DeptModel.fromJson(json.decode(str));

String deptModelToJson(DeptModel data) => json.encode(data.toJson());

class DeptModel {
  DeptModel({
    this.data,
  });

  List<Datum> data;

  factory DeptModel.fromJson(Map<String, dynamic> json) => DeptModel(
    data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class Datum {
  Datum({
    this.id,
    this.name,
    this.image,
  });

  int id;
  String name;
  String image;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    id: json["id"],
    name: json["name"],
    image: json["image"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "image": image,
  };
}
