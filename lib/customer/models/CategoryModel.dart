// To parse this JSON data, do
//
//     final categoryModel = categoryModelFromJson(jsonString);

import 'dart:convert';

CategoryModel categoryModelFromJson(String str) => CategoryModel.fromJson(json.decode(str));


class CategoryModel {
  CategoryModel({
    this.data,
  });

  List<Datum> data;

  factory CategoryModel.fromJson(Map<String, dynamic> json) => CategoryModel(
    data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
  );


}

class Datum {
  Datum({
    this.id,
    this.name,
    this.colorBack,
    this.colorTxt,
    this.products,
  });

  int id;
  String name;
  String colorBack;
  String colorTxt;
  List<Product> products;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    id: json["id"],
    name: json["name"],
    colorBack: json["color_back"],
    colorTxt: json["color_txt"],
    products: List<Product>.from(json["products"].map((x) => Product.fromJson(x))),
  );


}

class Product {
  Product({
    this.id,
    this.name,
    this.desc,
    this.loved,
    this.image,
    this.price,
    this.watch,
    this.rate,
    this.buyed,
    this.avalable,
  });

  int id;
  String name;
  String desc;
  bool loved;
  String image;
  int price;
  int watch;
  int rate;
  int buyed;
  bool avalable;

  factory Product.fromJson(Map<String, dynamic> json) => Product(
    id: json["id"],
    name: json["name"],
    desc: json["desc"],
    loved: json["loved"],
    image: json["image"],
    price: json["price"],
    watch: json["watch"],
    rate: json["rate"],
    buyed: json["buyed"],
    avalable: json["avalable"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "desc": desc,
    "loved": loved,
    "image": image,
    "price": price,
    "watch": watch,
    "rate": rate,
    "buyed": buyed,
    "avalable": avalable,
  };
}
