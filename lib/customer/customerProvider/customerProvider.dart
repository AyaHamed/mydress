import 'package:flutter/material.dart';
import 'package:my_dress/customer/models/AuthModel.dart';

class CustomerProvider with ChangeNotifier{
  AuthModel user ;

  void refreshCustomer(AuthModel newUser){
    user = newUser;
    notifyListeners();
  }

}