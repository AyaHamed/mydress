import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_dress/customer/customerProvider/customerProvider.dart';
import 'package:my_dress/customer/screens/BankAccounts.dart';
import 'package:my_dress/customer/screens/ContactUs.dart';
import 'package:my_dress/customer/screens/Home.dart';
import 'package:my_dress/customer/screens/MyLocations.dart';
import 'package:my_dress/customer/screens/MyOrders.dart';
import 'package:my_dress/customer/screens/Settings.dart';
import 'package:my_dress/general/constants/CachedImage.dart';
import 'package:my_dress/general/constants/MyColors.dart';
import 'package:my_dress/general/constants/MyText.dart';
import 'package:my_dress/general/screens/Ploicies.dart';
import 'package:my_dress/general/screens/Register.dart';
import 'package:provider/provider.dart';

Widget defaultDrawer(context) {
  final customer = Provider.of<CustomerProvider>(context, listen: false);

  Widget contactInfo(String text){
    return MyText(
      title: text,
      align: TextAlign.center,
      size: 12,
      fontWeight: FontWeight.bold,
      color: MyColors.white,
    );
  }

  Widget drawerItem(context, String title, IconData icon, Widget page){
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 25, vertical: 10),
      child: InkWell(
        onTap: (){ Navigator.push(context,
            CupertinoPageRoute(builder: (context) => page));},
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Icon(icon, color: MyColors.white, size: 25,),
            SizedBox(width: 10,),
            MyText(
              title: title,
              color: MyColors.white,
              size: 15,
            ),
          ],
        ),
      ),
    );
  }

  return Drawer(
    child: Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,

        decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("images/bg_menu@2x.png"),
              fit: BoxFit.fill,
            )
        ),

        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(height: 30,),

              Container(
                height: 100, width: 100,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(50),
                    color: MyColors.greyWhite
                ),
                child: CachedImage(url: customer.user.avatar,),
                //Image.asset("images/profile@2x.png",),
              ),

              SizedBox(height: 10,),

              contactInfo(customer.user.name),
              contactInfo(customer.user.email),

              Padding(
                padding: EdgeInsets.symmetric(horizontal: 35, vertical: 10),
                child: Divider( color: MyColors.white, thickness: 1,),
              ),

              drawerItem(context, "الرئيسية ", Icons.home_rounded, Home(0)),
              drawerItem(context, "طلباتي ", Icons.article_outlined, MyOrders()),
              drawerItem(context, "مواقع الشحن", Icons.location_on, MyLocations()),
              drawerItem(context, "الحساب البنكي  ", Icons.credit_card, BankAccounts()),
              drawerItem(context, "السياسة العامة", Icons.help_rounded, Policies()),
              drawerItem(context, "الاعدادات   ", Icons.settings, Settings()),
              drawerItem(context, "تواصل معنا", Icons.chat, ContactUs()),
              drawerItem(context, "تسجيل خروج", Icons.logout, Register()),

            ],
          ),
        ),

      ),

    ),
  );
}