import 'package:flutter/material.dart';
import 'package:my_dress/general/constants/MyColors.dart';
import 'package:my_dress/general/constants/MyText.dart';

Widget title({
  @required String text,
  double iconSize,
  Color fontColor,
  Color iconColor,
  double fontSize,
  bool icon=true,
  bool padding = true,
})
{
  return Padding(
    padding: padding == true ?EdgeInsets.symmetric(horizontal: 20, vertical: 10) : EdgeInsets.zero,
    child: Row(
      //mainAxisAlignment: MainAxisAlignment.start,
      children: [
        icon == true ? Icon(Icons.circle, size: iconSize?? 10, color: iconColor?? MyColors.primary,) : Container(),
        icon == true ? SizedBox(width: 10,) : Container(),
        //defaultText(text, fontColor?? Colors.blue[900]),
        MyText(
          title: text,
          color: fontColor?? MyColors.primary,
          size: fontSize ?? 12,
        )
      ],
    ),
  );
}