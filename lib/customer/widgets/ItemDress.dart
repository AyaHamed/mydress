import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_dress/general/constants/CachedImage.dart';
import 'package:my_dress/general/constants/MyColors.dart';
import 'package:my_dress/general/constants/MyText.dart';

Widget  itemDress({
  BuildContext context, Widget page,
  String title, String details, String price,
  String image, Function onTap, bool loved
}){
  return InkWell(

    onTap: () {Navigator.of(context)
        .push(CupertinoPageRoute(builder: (_) => page));},
    // onTap: onTap,


    child: Container(
      width: MediaQuery.of(context).size.width * 0.45,
      height: 300,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,

        children: [
          Container(
            height: 230,
            width: MediaQuery.of(context).size.width * 0.45,
            child: InkWell(
              onTap: onTap,
              child: Icon(
                loved==false? Icons.favorite_border: Icons.favorite,
                color: MyColors.gold,
            )),
            alignment: Alignment.topLeft,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: image != null ? NetworkImage(image) : AssetImage("images/model.PNG"), fit: BoxFit.fill,
              )
            ),
            //child: Image.asset("images/model.PNG", fit: BoxFit.fill,),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 10),
            child: MyText(
              //title: "فستان خطوبة وردي",
              title: title?? "",
              color: MyColors.primary,
              overflow: TextOverflow.ellipsis,

            ),
          ),

          // Container(
          //   padding: EdgeInsets.symmetric(horizontal: 10),
          //   width: MediaQuery.of(context).size.width * 0.45,
          //   child: MyText(
          //     //title: "detaaaaaaaaaaaaaaaaaaaaaaaaaaaaaails",
          //     title: details.length>20?details.substring(20):details,
          //     color: MyColors.grey,
          //     overflow: TextOverflow.ellipsis,
          //     size: 12,
          //   ),
          // ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 10),
                child: MyText(
                  //title: "299",
                  title: price?? "",
                  color: MyColors.primary,
                ),
              ),
              MyText(
                title: "ر س",
                color: MyColors.primary,
              ),
            ],),

        ],
      ),

    ),
  );
}