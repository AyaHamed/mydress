import 'package:flutter/material.dart';
import 'package:my_dress/general/constants/MyColors.dart';

Widget dressColor(Color color, {bool padding = true,bool selected = false,}) {
  return Padding(
    padding: padding==true? EdgeInsets.symmetric(horizontal: 5, vertical: 10) : EdgeInsets.symmetric(horizontal: 5),
    child: Container(
      height: 30,
      width: 30,
      decoration: BoxDecoration(
        color: selected == true ? MyColors.greyWhite: MyColors.white,
        border: Border.all(color:MyColors.grey),
      ),
      child: Center(
        child: Container(
          height: 20,
          width: 20,
          decoration: BoxDecoration(
            color: color,
            borderRadius: BorderRadius.circular(10),
          ),
        ),
      ),
    ),
  );
}