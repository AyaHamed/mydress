import 'package:flutter/material.dart';
import 'package:my_dress/general/constants/CachedImage.dart';
import 'package:my_dress/general/constants/MyColors.dart';
import 'package:my_dress/general/constants/MyText.dart';

Widget item({BuildContext context, String image, String date, String price, bool status, }) {
  return Padding(
    padding: EdgeInsets.symmetric(vertical: 3, horizontal: 15),
    child: Container(
      width: MediaQuery.of(context).size.width,
      color: MyColors.white,
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [

            Container(
              height: 80, width: 70,
              //child: Image.asset("images/profile@2x.png", fit: BoxFit.fill,),
              child: CachedImage(url: image, fit: BoxFit.fill,),
              decoration: BoxDecoration(
                  color: MyColors.greyWhite,
                  border: Border.all(color: MyColors.grey)
              ),
            ),

            SizedBox(width: 20,),

            Container(
              width: 190,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,

                children: [

                  date!=null? itemText(date, "تاريخ الطلب  : ",): Container(),
                  itemText( "$price رس ", "إجمالي الطلب  : ",),
                  itemText(
                      status==true? "متوفر" : "غير متوفر",
                      "حالة المنتج :"),
                ],),
            ),

            Expanded(child: SizedBox()),

            Icon(Icons.delete_outline,
              color: MyColors.primary,  size: 30,),

          ],),
      ),
    ),
  );
}

Widget itemText(String text, String details,){
  return Row(
    mainAxisAlignment: MainAxisAlignment.start,
    children: [
      MyText(title: details, color: MyColors.primary),
      SizedBox(width: 10,),
      MyText(title: text, color: MyColors.grey),
    ],);
}