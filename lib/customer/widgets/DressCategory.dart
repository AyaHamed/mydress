import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_dress/general/constants/MyColors.dart';
import 'package:my_dress/general/constants/MyText.dart';

Widget dressCategory({BuildContext context, String image, Widget page, String title, int id}){
  return InkWell(
    onTap: () {Navigator.of(context)
        .push(CupertinoPageRoute(builder: (_) => page));},

    child: Padding(
      padding: EdgeInsets.symmetric(vertical: 2.5, horizontal: 5),
      child: Container(

        width: MediaQuery.of(context).size.width,
        height: 180,

        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          image: DecorationImage(
            image:
            //AssetImage(image),fit: BoxFit.fill,
            NetworkImage(image), fit: BoxFit.fill
          )
        ),
        //child: Image.asset(image, fit: BoxFit.fill,),

        alignment: Alignment.center,
        child: MyText(
          title: title?? "",
          color: MyColors.white,
          fontWeight: FontWeight.bold,
          size: 15,
        ),

      ),
    ),
  );
}