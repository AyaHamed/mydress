import 'package:flutter/material.dart';
import 'package:my_dress/general/constants/MyColors.dart';
import 'package:my_dress/general/constants/MyText.dart';

Widget promoCode(BuildContext context){
  return Container(
    width: MediaQuery.of(context).size.width,
    height: 70,

    decoration: BoxDecoration(
      color: MyColors.white,
      border: Border.all(color: MyColors.greyWhite),
      borderRadius: BorderRadius.circular(2),
    ),

    child: Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,

      children: [

        MyText(title: "كود الخصم", color: MyColors.primary,),



        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: Container(
            height: 30,
            width: 180,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                border: Border.all(color: MyColors.greyWhite)
            ),
            child: TextFormField(
                decoration: InputDecoration(
                    enabledBorder: InputBorder.none,
                    focusedBorder: InputBorder.none
                )
            ),
          ),
        ),

        Container(
          width: 70, height: 25,
          decoration: BoxDecoration(
              color: MyColors.primary,
              borderRadius: BorderRadius.circular(20)
          ),
          child: Center(child: MyText(title: "تطبيق", color: MyColors.white)),
        ),

      ],
    ),

  );
}