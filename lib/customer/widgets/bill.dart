import 'package:flutter/material.dart';
import 'package:my_dress/general/constants/MyColors.dart';
import 'package:my_dress/general/constants/MyText.dart';

Widget bill({
  String quantity,
  String total,
  String delivery,
  String sale
})
{
  return Container(
    child: Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,

          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                MyText(title: "عدد القطع :", color: MyColors.grey),
                MyText(title: "الخصم  :", color: MyColors.grey),
                MyText(title:"مصاريف الشحن  :" , color: MyColors.grey),
              ],
            ),

            Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                MyText(title: quantity, color: MyColors.grey),
                MyText(title: sale, color: MyColors.grey),
                MyText(title: delivery, color: MyColors.grey),
              ],
            ),



          ],
        ),

        Divider(
          thickness: 2,
          color: MyColors.gold,
        ),

        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            MyText(title: "إجمالي الطلب  :", color: MyColors.grey),
            MyText(title: total, color: MyColors.grey),

          ],
        ),

        SizedBox(height: 20,),
      ],
    ),
  );
}