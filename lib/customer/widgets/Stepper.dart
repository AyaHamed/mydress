import 'package:flutter/material.dart';
import 'package:my_dress/general/constants/MyColors.dart';
import 'package:my_dress/general/constants/MyText.dart';


Widget stepper({
  BuildContext context,
  String status1,
  IconData icon1,
  String status2,
  IconData icon2,
  String status3,
  IconData icon3,
  String date1,
  String date2,
  String date3,
})

{
  return Container(
    width: MediaQuery.of(context).size.width,

    padding: EdgeInsets.symmetric(vertical: 15),

    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        status(status1, icon1, date2),

        line(),

        status(status2, icon2, date2),

        line(),

        status(status3, icon3, date3),
      ],
    ),
  );
}

Widget status(String text, IconData icon, String date){
  return Padding(
    padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
    child: Row(
      children: [
        Icon(icon, color: MyColors.gold, size: 30,),
        SizedBox(width: 10,),
        MyText(title: text , color: MyColors.primary),
        Spacer(),
        MyText(title: date, color: MyColors.grey),
      ],
    ),
  );
}

Widget line(){
  return Padding(
    padding: const EdgeInsets.symmetric(horizontal: 35),
    child: Container(
      height: 100, width: 1,
      color: MyColors.black,
    ),
  );
}